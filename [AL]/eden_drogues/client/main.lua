ESX = nil
local Keys = {
    ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
    ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
    ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
    ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
    ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
    ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
    ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
    ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
    ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
  }

local GUI                       = {}
GUI.Time                        = 0
local Weed                     = {}


Citizen.CreateThread(function()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(0)
  end
end)

function OpenDrugMenu()

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'police_actions',
      {
        title    = 'Endora Drugs',
        align    = 'top-left',
        elements = {
          {label = 'GPS', value = 'gps'},
        },
      },
      function(data, menu)
  
       
        if data.current.value == 'gps' then
  
          ESX.UI.Menu.Open(
            'default', GetCurrentResourceName(), 'gps',
            {
              title    = 'GPS',
              align    = 'top-left',
              elements = {
                {label = 'Ajouter coord plan', value = 'add_gps'},
                {label = 'Crée plan', value = 'create_plan'},

              },
  
            },
            function(data, menu)
  
                if data.current.value == 'add_gps' then
  
                  local pos = GetEntityCoords(GetPlayerPed(-1))
                  local currentPos = GetEntityCoords(GetPlayerPed(-1))

                    x, y, z = table.unpack(currentPos);
                    TriggerServerEvent('endora_drugs:addpos', x, y, z)
                    ESX.ShowNotification('addpos')
                    ESX.ShowNotification('X : ' .. x)
                    ESX.ShowNotification('Y : ' .. y)
                    ESX.ShowNotification('Z : ' .. z )
        
                end

                if data.current.value == 'create_plan' then
                  CreateWeed()
                end


  
            end,
            function(data, menu)
              menu.close()
            end
          )
  
        end
      end,

      function(data, menu)
        menu.close()
      end
    )
  
end

function CreateWeed()

  ESX.TriggerServerCallback('endora_drugs:addplan', function(data)
    for i=1, #data, 1 do
      local data = data[i]
        Weed[data.name] = CreateObject(GetHashKey("prop_weed_01"), data.x , data.y , data.z -1 ,  true,  true, true)    
        SetEntityAsMissionEntity(Weed[data.name],true,true)
        SetEntityAlwaysPrerender(Weed[data.name],true)
        SetEntityHeading(Weed[data.name], GetEntityHeading(playerPed))
        PlaceObjectOnGroundProperly(Weed[data.name])
        FreezeEntityPosition(Weed[data.name],true)

        --[[
        SetBlipSprite (Blips[data.name], 162)
        SetBlipDisplay(Blips[data.name], 2)
        SetBlipScale(Blips[data.name], 0.8)
        SetBlipColour (Blips[data.name], 1)
        SetBlipAsShortRange(Blips[data.name], true)
        BeginTextCommandSetBlipName("STRING")
        AddTextComponentString(data.name)
        EndTextCommandSetBlipName(Blips[data.name]) ]]
    end
  end)
end

Citizen.CreateThread(function()
  while true do

    Citizen.Wait(0)

    if IsControlPressed(0,  57) and (GetGameTimer() - GUI.Time) > 150 then
        OpenDrugMenu()
        print('ouvert')
        GUI.Time = GetGameTimer()
    end

  end
end)