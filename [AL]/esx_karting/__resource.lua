description 'esx_karting.lua'

server_scripts {

    '@es_extended/locale.lua',
    'locales/fr.lua',
    'server/esx_karting.lua',
    'config.lua'
}

client_scripts {

    '@es_extended/locale.lua',
    'locales/fr.lua',
    'config.lua',
    'client/esx_karting.lua',
}