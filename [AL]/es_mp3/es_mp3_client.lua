--DO-NOT-EDIT-BELLOW-THIS-LINE--
-- Init ESX
ESX = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)
-- Fin init ESX
local soundDistance = 13 -- Distance of MP3 sounds

Key = 38 -- ENTER
local times = 0
Songs = {
	{label = "Inserer nom musique",   music = "music1"},
	{label = "Inserer nom musique1",  music = "KrantenwijkBoef"},
	{label = "Inserer nom musique2",  music = "AlanWalkerSpectreDubstepRemix"},
	{label = "Inserer nom musique3",  music = "DuaLipaNewRules"},
	{label = "Inserer nom musique4",  music = "TheProdigyNoGoodStartTheDance"},
	{label = "Inserer nom musique5",  music = "XillionsSomebodyLikeMe"},
	{label = "Inserer nom musique6",  music = "KygoEllieGouldingFirstTime"},
	{label = "Inserer nom musique7",  music = "KSHMRTigerlilyInvisibleChildren"},
	{label = "Inserer nom musique8",  music = "JaxJonesInstruction"},
	{label = "Inserer nom musique9",  music = "DonDiabloCuttingShapes"},
	{label = "Inserer nom musique10", music = "DieAtzenDiscoPogo"},
}

vehicleMP3Station = {
	{-1381.4698486328,  -616.19482421875,  31.497928619385},
}

function es_mp3_DrawSubtitleTimed(m_text, showtime)
	SetTextEntry_2('STRING')
	AddTextComponentString(m_text)
	DrawSubtitleTimed(showtime, 1)
end

function DisplayHelpText(str)
  SetTextComponentFormat("STRING")
  AddTextComponentString(str)
  DisplayHelpTextFromStringLabel(0, 0, 1, -1)
  --DisplayHelpTextFromStringLabel(0, 0, 0, -1)           ---A 0 Enleve le son de la notif
end

function es_mp3_DrawNotification(m_text)
	SetNotificationTextEntry('STRING')
	AddTextComponentString(m_text)
	DrawNotification(true, false)
end

function startAttitude(lib, anim)
 	Citizen.CreateThread(function()

    local playerPed = GetPlayerPed(-1)

    RequestAnimSet(anim)
    while not HasAnimSetLoaded(anim) do
      Citizen.Wait(0)
    end
    SetPedMovementClipset(playerPed, anim, true)
	end)
end

function startAnim(lib, anim)

	Citizen.CreateThread(function()

	  RequestAnimDict(lib)
	  while not HasAnimDictLoaded( lib) do
	    Citizen.Wait(0)
	  end

	  TaskPlayAnim(GetPlayerPed(-1), lib ,anim ,8.0, -8.0, -1, 0, 0, false, false, false )
	end)
end

function startScenario(anim)
  TaskStartScenarioInPlace(GetPlayerPed(-1), anim, 0, false)
end

function OpenMusic()
  local elements = {}

	for i = 1, #Songs do
			table.insert(elements, {label = Songs[i].label , value = Songs[i].music})
	end


	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'joblisting',
		{
			title    = ('job_center'),
			elements = elements
		},
		function(data, menu)
			TriggerServerEvent("InteractSound_SV:PlayWithinDistance", soundDistance, data.current.value, 0.5, x,y,z)
			TriggerEvent('esx:showNotification', "~p~Vous venez de mettre : ~w~" .. data.current.label )
			menu.close()
		end,
		function(data, menu)
			menu.close()
		end
	)
end

Citizen.CreateThread(function ()
	while true do
		Citizen.Wait(0)
			local coords      = GetEntityCoords(GetPlayerPed(-1))
			local isInMarker  = false
			local currentZone = nil
			for i = 1, #vehicleMP3Station do
				discoCoords = vehicleMP3Station[i]

				DrawMarker(1, discoCoords[1], discoCoords[2], discoCoords[3], 0, 0, 0, 0, 0, 0, 1.0, 1.0, 0.1, 240, 240, 0, 0, 0, 0, 2, 0, 0, 0, 0)
				if GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), discoCoords[1], discoCoords[2], discoCoords[3], true ) < 1 then

				DisplayHelpText("Appuyez sur ~INPUT_PICKUP~ ~p~pour changer de musique")
				DrawSubtitleTimed(2000, 1)
				if IsControlJustPressed(1, Key) then
					local music = Songs[ math.random( #Songs ) ]
					ESX.TriggerServerCallback('es_mp3:checkmoney',function(valid)
						if (valid) then
							times = 1
							x = discoCoords[1]
							y = discoCoords[2]
							z = discoCoords[3]
							OpenMusic()
							--TriggerServerEvent("InteractSound_SV:PlayWithinDistance", soundDistance, music, 0.5, -1381.4698486328,  -616.19482421875,  31.497928619385)
							--TriggerEvent('esx:showNotification', "~p~Vous avez allumé la platine !~w~ ~g~Appuyez sur E pour changer de musique.")
						else
							--TriggerEvent('esx:showNotification', "Vous n'avez pas assez d'argent")
						end
					end)
				end
				 isInMarker  = true
				 currentZone = k
			end
		end

		if (isInMarker and not HasAlreadyEnteredMarker) or (isInMarker and LastZone ~= currentZone) then
			HasAlreadyEnteredMarker = true
			LastZone                = currentZone
		end

		if not isInMarker and HasAlreadyEnteredMarker then
			HasAlreadyEnteredMarker = false
			TriggerServerEvent("InteractSound_SV:PlayWithinDistance", soundDistance, "vide", 0.5)
		end
	end
end)

RegisterNetEvent('es_mp3:free')
AddEventHandler('es_mp3:free', function ()
	es_mp3_DrawNotification("En Avant la ~y~Musique~s~!")
end)

RegisterNetEvent('es_mp3:free')
AddEventHandler('es_mp3:free', function ()
	es_mp3_DrawNotification("~h~~r~Tu n'as pas assez d'argent!")
end)

RegisterNetEvent('es_mp3:free')
AddEventHandler('es_mp3:free', function ()
	es_mp3_DrawNotification("En Avant la ~y~Musique~s~ Gratuite!")
end)
