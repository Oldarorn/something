local blip = {
    -- Police Stations
	{name="Poste de Police", id=60, x=442.444549560547, y=-983.581848144531, z=30.6895866394043, color=29},
	{name="Bureau du Sherif", id=60, x=-450.063201904297, y=6016.5751953125, z=31.7163734436035, color=29},
    --{name="Police Station", id=60, x=1859.234, y= 3678.742, z=33.690},
    --{name="Police Station", id=60, x=818.221, y=-1289.883, z=26.300},
    {name="Prison", id=285, x=1679.049, y=2513.711, z=45.565, color=29},
    -- Hospitals
    {name="Hopital National d'Eden", id=153, x=340.55, y=-1396.63, z=32.50, color=49},
    {name="Urgence de Paleto Bay", id=153, x= -247.76, y= 6331.23, z=32.43, color=49},

	   -- interest
	{name="Bureau de Vote", id=164, x=-429.12744140625, y=1110.62719726563, z=327.682098388672, color=5},
   -- Récoltes
	{name="Pomme d'amour", id=1, x=677.948364257813, y=327.633514404297, z=111.006843566895, color=59},
	{name="Pomme d'amour", id=1, x=-788.879638671875, y=-198.572616577148, z=36.8484344482422, color=59},
	{name="Pomme d'amour", id=1, x=455.956451416016, y=-718.789245605469, z=27.2863178253174, color=59},
	{name="Turner & co - Mining", id=89, x=1567.3, y=-2143.27, z=77.64, color=17}, -- QG Cuivre
	{name="Port d'Eden", id=356, x=-331.9591, y=-2792.6208, z=5.0002, color=67}, -- Port
	{name="Eden Bus Cie", id=106, x=449.1520, y=-650.905, z=28.482, color=11}, --Bus
	{name="Jo's Farm", id=88, x=2306.5944, y=4880.9624, z=41.8082, color=60}, --Farm
	{name="EPS - Livraison", id=430, x=-405.4783, y=6150.4931, z=31.6782, color=56}, -- EPS 
	{name="Scierie Mac Dogall", id=442, x=-565.78955078125, y=5325.8056640625, z=73.5927963256836, color=40}, --Bucheron
	-- Entrerpise Privé
	{name="Eden Repare", id=446, x=847.12, y=-1314.47, z=26.55, color=21},
	{name="Cluckin Bell", id=357, x=-72.68752, y=6253.72656, z=31.08991, color=5}, -- Cluckin Bell
	{name="Sky'Taxi", id=198, x=900.461, y=-181.466, z=73.89, color=3}, -- Taxi
	{name="The Defenders", id=84, x=-565.171, y=276.625, z=83.286, color=40}, -- Tequil-La La OMMC
	{name="Mauken Bank", id=431, x=247.56051, y=223.17640, z=106.286, color=2}, -- mauken
  }

local lieu = {
	{name="Manoir O'Malley", id=40, x=-1536.67529296875, y=105.820404052734, z=56.7797813415527, color=2},
	{name="The Rising Sun", id=40, x=1393.87902832031, y=3619.91650390625, z=38.9210510253906, color=2},
	{name="A VENDRE", id=439,  x=-630.400, y=-236.700, z=40.00, color=4}, --Jewelry 
    {name="A VENDRE", id=374, x=-1047.900, y=-233.000, z=39.000, color=4}, -- Lifeinvader
    {name="A VENDRE", id=374, x=2441.200, y=4968.500, z=51.700, color=4}, -- O'Neil Ranch
    {name="A VENDRE", id=374, x=2476.712, y=3789.645, z=41.226, color=4}, -- Hippy Camp
    {name="A VENDRE", id=374, x=479.056, y=-1316.825, z=28.203, color=4}, -- Chop shop
    {name="A VENDRE", id=374, x=736.153, y=2583.143, z=79.634, color=4}, -- Rebel Radio
    {name="A VENDRE", id=374, x=-1336.715, y=59.051, z=55.246 , color=4}, -- Golf
    -- Propperty
    {name="A VENDRE", id=374, x=925.329, y=46.152, z=80.908 , color=4}, -- Casino
    {name="A VENDRE", id=374, x=134.476, y=-1307.887, z=28.983, color=4}, -- Stripbar
    {name="A VENDRE", id=374, x=-1171.42, y=-1572.72, z=3.6636, color=4}, --Smoke on the Water
    {name="A VENDRE", id=374, x=293.089, y=180.466, z=104.301, color=4}, -- Theater
	{name="A VENDRE", id=374, x=-106.320, y=-8.258, z=70.522, color=4}, -- Theater
	    -- Gas stations
    {name="Station service", id=361, x=49.4187,   y=2778.793,  z=58.043, color=17},
    {name="Station service", id=361, x=263.894,   y=2606.463,  z=44.983, color=17},
    {name="Station service", id=361, x=1039.958,  y=2671.134,  z=39.550, color=17},
    {name="Station service", id=361, x=1207.260,  y=2660.175,  z=37.899, color=17},
    {name="Station service", id=361, x=2539.685,  y=2594.192,  z=37.944, color=17},
    {name="Station service", id=361, x=2679.858,  y=3263.946,  z=55.240, color=17},
    {name="Station service", id=361, x=2005.055,  y=3773.887,  z=32.403, color=17},
    {name="Station service", id=361, x=1687.156,  y=4929.392,  z=42.078, color=17},
    {name="Station service", id=361, x=1701.314,  y=6416.028,  z=32.763, color=17},
    {name="Station service", id=361, x=179.857,   y=6602.839,  z=31.868, color=17},
    {name="Station service", id=361, x=-94.4619,  y=6419.594,  z=31.489, color=17},
    {name="Station service", id=361, x=-2554.996, y=2334.40,  z=33.078, color=17},
    {name="Station service", id=361, x=-1800.375, y=803.661,  z=138.651, color=17},
    {name="Station service", id=361, x=-1437.622, y=-276.747,  z=46.207, color=17},
    {name="Station service", id=361, x=-2096.243, y=-320.286,  z=13.168, color=17},
    {name="Station service", id=361, x=-724.619, y=-935.1631,  z=19.213, color=17},
    {name="Station service", id=361, x=-526.019, y=-1211.003,  z=18.184, color=17},
    {name="Station service", id=361, x=-70.2148, y=-1761.792,  z=29.534, color=17},
    {name="Station service", id=361, x=265.648,  y=-1261.309,  z=29.292, color=17},
    {name="Station service", id=361, x=819.653,  y=-1028.846,  z=26.403, color=17},
    {name="Station service", id=361, x=1208.951, y= -1402.567, z=35.224, color=17},
    {name="Station service", id=361, x=1181.381, y= -330.847,  z=69.316, color=17},
    {name="Station service", id=361, x=620.843,  y= 269.100,  z=103.089, color=17},
    {name="Station service", id=361, x=2581.321, y=362.039, 108.468, color=17},
}

-- Display Map Blips
Citizen.CreateThread(function()
	for _, item in pairs(blip) do
	  item.blip = AddBlipForCoord(item.x, item.y, item.z)
	  SetBlipSprite(item.blip, item.id)
	  SetBlipAsShortRange(item.blip, true)
	  SetBlipColour(item.blip, item.color)
	  BeginTextCommandSetBlipName("STRING")
	  AddTextComponentString(item.name)
	  EndTextCommandSetBlipName(item.blip)
	end
	
	for _, item in pairs(lieu) do
	  item.blip = AddBlipForCoord(item.x, item.y, item.z)
	  SetBlipSprite(item.blip, item.id)
	  SetBlipAsShortRange(item.blip, true)
	  SetBlipColour(item.blip, item.color)
	  SetBlipScale(item.blip, 0.6)
	  BeginTextCommandSetBlipName("STRING")
	  AddTextComponentString(item.name)
	  EndTextCommandSetBlipName(item.blip)
	end	
	
  --load unloaded ipl's
  LoadMpDlcMaps()
  EnableMpDlcMaps(true)
  RequestIpl("chop_props")
  RequestIpl("FIBlobby")
  RemoveIpl("FIBlobbyfake")
  RequestIpl("FBI_colPLUG")
  RequestIpl("FBI_repair")
  RequestIpl("v_tunnel_hole")
  RequestIpl("TrevorsMP")
  RequestIpl("TrevorsTrailer")
  RequestIpl("TrevorsTrailerTidy")
  RemoveIpl("farm_burnt")
  RemoveIpl("farm_burnt_lod")
  RemoveIpl("farm_burnt_props")
  RemoveIpl("farmint_cap")
  RemoveIpl("farmint_cap_lod")
  RequestIpl("farm")
  RequestIpl("farmint")
  RequestIpl("farm_lod")
  RequestIpl("farm_props")
  RequestIpl("facelobby")
  RemoveIpl("CS1_02_cf_offmission")
  RequestIpl("CS1_02_cf_onmission1")
  RequestIpl("CS1_02_cf_onmission2")
  RequestIpl("CS1_02_cf_onmission3")
  RequestIpl("CS1_02_cf_onmission4")
  RequestIpl("v_rockclub")
  RemoveIpl("hei_bi_hw1_13_door")
  RequestIpl("bkr_bi_hw1_13_int")
  RequestIpl("ufo")
  RemoveIpl("v_carshowroom")
  RemoveIpl("shutter_open")
  RemoveIpl("shutter_closed")
  RemoveIpl("shr_int")
  RemoveIpl("csr_inMission")
  RequestIpl("v_carshowroom")
  RequestIpl("shr_int")
  RequestIpl("shutter_closed")
  RequestIpl("smboat")
  RequestIpl("cargoship")
  RequestIpl("railing_start")
  RemoveIpl("sp1_10_fake_interior")
  RemoveIpl("sp1_10_fake_interior_lod")
  RequestIpl("sp1_10_real_interior")
  RequestIpl("sp1_10_real_interior_lod")
  RemoveIpl("id2_14_during_door")
  RemoveIpl("id2_14_during1")
  RemoveIpl("id2_14_during2")
  RemoveIpl("id2_14_on_fire")
  RemoveIpl("id2_14_post_no_int")
  RemoveIpl("id2_14_pre_no_int")
  RemoveIpl("id2_14_during_door")
  RequestIpl("id2_14_during1")
  RequestIpl("coronertrash")
  RequestIpl("Coroner_Int_on")
  RemoveIpl("Coroner_Int_off")
  RemoveIpl("bh1_16_refurb")
  RemoveIpl("jewel2fake")
  RemoveIpl("bh1_16_doors_shut")
  RequestIpl("refit_unload")
  RequestIpl("post_hiest_unload")
  RequestIpl("Carwash_with_spinners")
  RequestIpl("ferris_finale_Anim")
  RemoveIpl("ch1_02_closed")
  RequestIpl("ch1_02_open")
  RequestIpl("AP1_04_TriAf01")
  RequestIpl("CS2_06_TriAf02")
  RequestIpl("CS4_04_TriAf03")
  RemoveIpl("scafstartimap")
  RequestIpl("scafendimap")
  RemoveIpl("DT1_05_HC_REMOVE")
  RequestIpl("DT1_05_HC_REQ")
  RequestIpl("DT1_05_REQUEST")
  RequestIpl("FINBANK")
  RemoveIpl("DT1_03_Shutter")
  RemoveIpl("DT1_03_Gr_Closed")
  RequestIpl("ex_sm_13_office_01a")
  RequestIpl("ex_sm_13_office_01b")
  RequestIpl("ex_sm_13_office_02a")
  RequestIpl("ex_sm_13_office_02b")
end)

-- Display Markers
local markerpos = {
	{['x'] = -89.0866928100586, ['y'] = 1909.90319824219, ['z'] = 196.756561279297}, -- r cana
	{['x'] = 2487.16284179688, ['y'] = 3726.5224609375, ['z'] = 43.9216270446777},	-- t cana
	{['x'] = -1111.54541015625, ['y'] = 4936.45947265625, ['z'] = 218.38410949707}, -- v cana
}
local secretmarkerpos = {
	{['x'] = 492.511566162109, ['y'] = -2178.28369140625, ['z'] = 5.91827487945557}, -- r exta
	{['x'] = 438.4, ['y'] = 3570.67, ['z'] = 33.87},	-- t exta
	{['x'] = -1343.95764160156, ['y'] = 67.1206512451172, ['z'] = 55.2456588745117}, -- v exta
}

Citizen.CreateThread(function()
    while true do
		Citizen.Wait(1)
		local pos = GetEntityCoords(GetPlayerPed(-1), true)
		for k,v in ipairs(markerpos) do
			if(Vdist(pos.x, pos.y, pos.z, v.x, v.y, v.z) < 50)then
				DrawMarker(1, v.x, v.y, v.z - 1, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.5001, 25, 15, 15,165, 0, 0, 0,0)
				if(Vdist(pos.x, pos.y, pos.z, v.x, v.y, v.z) < 10)then
					ShowNotification(" Il se passe des choses louches ici...")
				end
			end
		end
		for k,v in ipairs(secretmarkerpos) do
			if(Vdist(pos.x, pos.y, pos.z, v.x, v.y, v.z) < 8)then
				ShowNotification(" Il se passe des choses louches ici...")
			end
		end
    end
end)

function ShowNotification(text)
  SetNotificationTextEntry("STRING")
  AddTextComponentString(text)
  DrawNotification(true, false)
end