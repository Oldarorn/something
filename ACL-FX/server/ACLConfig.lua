-- Check the ping of the client attempting to connect. Set to 0 to disable this check
ACL.maxPing = 120

-- Set to false to disable whitelisting
ACL.enableWhitelist = false

-- Set to true to enable the /kick and /playerlist commands
ACL.enablePlayerManagement = true

-- Set to false to disable the check whether identities really belong to connecting player (causes "please try again" message)
ACL.useIdentBugWorkaround = true

ACL.whitelist = {
	"steam:1100001002842b4", -- Jack Slater - OK
	"steam:1100001004655a5", -- Cronan O'Malley" - OK
	"steam:110000100965566", -- Abel O'Malley" - OK
	"steam:110000100a8852c", -- Francis Perra" - OK
	"steam:1100001016412c9", -- Ternock OMalley" - OK
	"steam:110000101f87d51", -- Stanley Dekker" - OK
	"steam:1100001021bd65b", -- Ethan O'Malley" - OK
}

-- Currently mods and admins are indistinguishable
ACL.mods = {
	"steam:1100001026a0cab", -- David
	"steam:110000107310713", -- Daminou
	"steam:110000100a8852c", -- Pacom
}

ACL.admins = {
	"ip:0.0.0.0",
	"ip:127.0.0.1",
	"ip:192.168.1.2",
	"ip:172.16.1.2",
	"ip:10.0.0.2",
	"steam:1100001002842b4", -- Jack Slater
	"steam:1100001004655a5", -- Cronan O'Malley
	"steam:110000100965566", -- Abel O'Malley
	"steam:1100001016412c9", -- Ternock OMalley
}

ACL.banlist = {
}
