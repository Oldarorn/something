--Déclaration des EventHandler
RegisterServerEvent("projectEZ:savelastpos")
RegisterServerEvent("projectEZ:SpawnPlayer")

--Intégration de la position dans MySQL
AddEventHandler("projectEZ:savelastpos", function( LastPosX , LastPosY , LastPosZ , LastPosH )
	local sourcePlayer = tonumber(source)
	local player = getPlayerID(sourcePlayer)
	local LastPos = "{" .. LastPosX .. ", " .. LastPosY .. ",  " .. LastPosZ .. ", " .. LastPosH .. "}"
	MySQL.Sync.execute("UPDATE users SET lastpos=@lastpos WHERE identifier =@player",{['@lastpos']= LastPos,['@player']= player})
end)

--Récupération de la position depuis MySQL
AddEventHandler("projectEZ:SpawnPlayer", function()
	--Récupération du SteamID.
	local sourcePlayer = tonumber(source)
	local player = getPlayerID(sourcePlayer)	
	local lastpos = MySQL.Sync.fetchScalar("SELECT lastpos FROM users WHERE identifier = @name", {['@name'] = player})
	if(lastpos ~= "") then
		local ToSpawnPos = json.decode(lastpos)
		-- On envoie la derniere position vers le client pour le spawn
		TriggerClientEvent("projectEZ:spawnlaspos", tonumber(sourcePlayer), ToSpawnPos[1], ToSpawnPos[2], ToSpawnPos[3])
	end
end)

function getPlayerID(source)
    local identifiers = GetPlayerIdentifiers(source)
    local player = getIdentifiant(identifiers)
    return player
end

function getIdentifiant(id)
    for _, v in ipairs(id) do
        return v
    end
end