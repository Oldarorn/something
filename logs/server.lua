local os_time = os.time
local os_date = os.date


local entityIdToPlayer = {}

local LogConnect = true
local LogDisconnect = true

RegisterServerEvent('logs:onPlayerKilled')
AddEventHandler('logs:onPlayerKilled', function(t,killer, kilerT) -- t : 0 = NPC, 1 = player
  file = io.open("logs/KillLogs.txt", "a")
  local local_hour = os_date("%d-%m-%Y %H:%M:%S")


  if(t == 1) then
     if(GetPlayerName(killer) ~= nil and GetPlayerName(source) ~= nil)then
        
       if(kilerT.killerinveh) then
         local model = kilerT.killervehname
         if file then
             print("["..local_hour.."] "..GetPlayerName(source).." ("..getPlayerID(source)..") has been killed by  : "..GetPlayerName(killer).." ("..getPlayerID(killer)..") | Vehicle"..model)

             file:write("["..local_hour.."] "..GetPlayerName(source).." ("..getPlayerID(source)..") has been killed by : "..GetPlayerName(killer).." ("..getPlayerID(killer)..") | Vehicle : "..model)
             file:write("\n")
         end

       else
          if file then
             print("["..local_hour.."] "..GetPlayerName(source).." ("..getPlayerID(source)..") has been killed by : "..GetPlayerName(killer).." ("..getPlayerID(killer)..")")

             file:write("["..local_hour.."] "..GetPlayerName(source).." ("..getPlayerID(source)..") has been killed by : "..GetPlayerName(killer).." ("..getPlayerID(killer)..")")
             file:write("\n")
         end
       end    
    end
  else
    print("["..local_hour.."] "..GetPlayerName(source).." ("..getPlayerID(source)..") has been killed by a NPC or vehicle")
    file:write("["..local_hour.."] "..GetPlayerName(source).." ("..getPlayerID(source)..") has been killed by a NPC or vehicle")
    file:write("\n")
  end
  file:close() 
end)


RegisterServerEvent("logs:sendPoliceLog")
AddEventHandler("logs:sendPoliceLog", function(model)
  local_hour = os_date("%c")
    print("["..local_hour.."] "..GetPlayerName(source).." ("..getPlayerID(source)..") : "..model)
    file = io.open("logs/PoliceVehicleLogs.txt", "a")
    if file then
      file:write("["..local_hour.."] "..GetPlayerName(source).." ("..getPlayerID(source)..") : "..model)
      file:write("\n")
    end
    file:close()
end)



RegisterServerEvent("logs:sendBlackListedLogs")
AddEventHandler("logs:sendBlackListedLogs", function(model)
  local_hour = os_date("%c")
  file = io.open("logs/BlackListedLogs.txt", "a")
  if file then
    print("["..local_hour.."] "..GetPlayerName(source).." ("..getPlayerID(source)..") : "..model)
     file:write("["..local_hour.."] "..GetPlayerName(source).." ("..getPlayerID(source)..") : "..model)
     file:write("\n")
  end
  file:close()
end)




RegisterServerEvent("logs:sendBlackListedWeapon")
AddEventHandler("logs:sendBlackListedWeapon", function(wea)
local_hour = os_date("%c")
  file = io.open("logs/BlackListedWeaponLogs.txt", "a")
  if file then
    print("["..local_hour.."] "..GetPlayerName(source).." ("..getPlayerID(source)..") have a blacklisted weapon : "..hashToWeapon[wea])
     file:write("["..local_hour.."] "..GetPlayerName(source).." ("..getPlayerID(source)..") have a blacklisted weapon : "..hashToWeapon[wea])
     file:write("\n")
  end
  file:close()
end)




AddEventHandler("playerDropped", function(reason)
  if(LogDisconnect) then
    local_hour = os_date("%c")
      file = io.open("logs/Connections.txt", "a")
      if file then
        print("["..local_hour.."] "..GetPlayerName(source).." ("..getPlayerID(source)..") disconnected.")
         file:write("["..local_hour.."] "..GetPlayerName(source).." ("..getPlayerID(source)..") disconnected.")
         file:write("\n")
      end
    file:close()
  end
end)


RegisterServerEvent("logs:playerConnected")
AddEventHandler("logs:playerConnected", function()
  if(LogConnect) then
    local_hour = os_date("%c")
     file = io.open("logs/Connections.txt", "a")
      if file then
        print("["..local_hour.."] "..GetPlayerName(source).." ("..getPlayerID(source)..") connected.")
        file:write("["..local_hour.."] "..GetPlayerName(source).." ("..getPlayerID(source)..") connected.")
        file:write("\n")
      end
    file:close()
  end
end)

RegisterServerEvent("logs:playerPickUpItem")
AddEventHandler("logs:playerPickUpItem", function(id,item,total)
  local _source = id
  if(LogConnect) then
    local_hour = os_date("%c")
     file = io.open("logs/GiveItems.txt", "a")
      if file then
        TriggerEvent("eden_garage:debug",GetPlayerName(_source))
        print("["..local_hour.."] "..GetPlayerName(_source).." ("..getPlayerID(_source)..") has pickup "..item.." : "..total)
        file:write("["..local_hour.."] "..GetPlayerName(_source).." ("..getPlayerID(_source)..") has pickup "..item.." : "..total)
        file:write("\n\r")
      end
    file:close()
  end
end)

RegisterServerEvent('esx:removeInventoryItem')
AddEventHandler('esx:removeInventoryItem', function(type, itemName, itemCount)
  local _source = source
  if(LogConnect) then
    local_hour = os_date("%c")
     file = io.open("logs/DropsItems.txt", "a")
      if file then
        print("["..local_hour.."] "..GetPlayerName(_source).." ("..getPlayerID(_source)..") dropped "..itemName.." : "..itemCount)
        file:write("["..local_hour.."] "..GetPlayerName(_source).." ("..getPlayerID(_source)..") dropped "..itemName.." : "..itemCount)
        file:write("\n\r")
      end
    file:close()
  end
end)

RegisterServerEvent('esx:giveInventoryItem')
AddEventHandler('esx:giveInventoryItem', function(target, type, itemName, itemCount)
  local _source      = source

  if(LogConnect) then
    local_hour = os_date("%c")
     file = io.open("logs/GiveItems.txt", "a")
      if file then
        print("["..local_hour.."] "..GetPlayerName(_source).." ("..getPlayerID(_source)..") gave "..itemName.." : "..itemCount.." to "..GetPlayerName(target).." ("..getPlayerID(target)..")")
        file:write("["..local_hour.."] "..GetPlayerName(_source).." ("..getPlayerID(_source)..") gave "..itemName.." : "..itemCount.." to "..GetPlayerName(target).." ("..getPlayerID(target)..")")
        file:write("\n\r")
      end
    file:close()
  end

end)

RegisterServerEvent('esx_weashop:buyItem')
AddEventHandler('esx_weashop:buyItem', function(itemName, price, zone)
  local _source = source

  if(LogConnect) then
    local_hour = os_date("%c")
     file = io.open("logs/BuyWeapons.txt", "a")
      if file then
        print("["..local_hour.."] "..GetPlayerName(_source).." ("..getPlayerID(_source)..") bought "..itemName.." at "..zone.." for "..price)
        file:write("["..local_hour.."] "..GetPlayerName(_source).." ("..getPlayerID(_source)..") bought "..itemName.." at "..zone.." for "..price)
        file:write("\n\r")
      end
    file:close()
  end
end)

-- get's the player id without having to use bugged essentials
function getPlayerID(source)
    local identifiers = GetPlayerIdentifiers(source)
    local player = getIdentifiant(identifiers)
    return player
end

-- gets the actual player id unique to the player,
-- independent of whether the player changes their screen name
function getIdentifiant(id)
    for _, v in ipairs(id) do
        return v
    end
end

RegisterServerEvent("logs:addEntityId")
AddEventHandler("logs:addEntityId",function(id)
  entityIdToPlayer[source] = id
end)