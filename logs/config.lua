settings = {
	LogKills = true, -- Log when a player kill an other player.
	LogPnjKills = true, -- Log when a player has been killed by a pnj
	LogEnterPoliceVehicle = true, -- Log when an player enter in a police vehicle.
	LogEnterBlackListedVehicle = true, -- Log when a player enter in a blacklisted vehicle. Blacklisted vehicles are vehicles in te "blacklistedModels" array.
	DeleteWhenEnterBlackListedVehicle = true, -- if it's on "true", when a player enter in one of blacklisted vehicles, it's going to depop.
	LogWhenHaveBlackListedGun = true, -- Log when a player gets a blacklisted gun
	DeleteBlackListedGun = true, -- if it's on true, when a player gets a blacklisted gun, it's going to be removed
}


blacklistedModels = { -- By default, it's all the military vehicles
	GetHashKey("APC"),
	GetHashKey("BARRACKS"),
	GetHashKey("BARRACKS2"),
	GetHashKey("RHINO"),
	GetHashKey("CRUSADER"),
	GetHashKey("CARGOBOB"),
	GetHashKey("SAVAGE"),
	GetHashKey("TITAN"),
	GetHashKey("LAZER"),
}


blacklistedGuns = {
	--Get models id here : https://pastebin.com/0wwDZgkF
	--0x99B507EA,
	-- (Example)
	-- 0x99B507EA, -- Knife
	-- 0x678B81B1, -- Nightstick
	-- 0x4E875F73, --Hammer
	-- 0x958A4A8F, --Bat
	-- 0x440E4788, --GolfClub
	0x84BD7BFD, --Crowbar

	0x1B06D571, --Pistol
	-- 0x5EF9FEC4, --CombatPistol
	0x22D8FE39, --APPistol
	0x99AEEB3B, --Pistol50
	-- 0x3656C8C1, --StunGun

	0x13532244, --MicroSMG
	-- 0x2BE6766B, --SMG
	-- 0xEFE7E2DF, --AssaultSMG
	0x0A3D4D34, --CombatPDW
	--0xBFEFFF6D, --AssaultRifle
	0x83BF0278, --CarbineRifles
	0xAF113F99, --AdvancedRifle
	0x9D07F764, --MG
	0x7FD62962, --CombatMG

	-- 0x1D073A89, --PumpShotgun
	-- 0x7846A318, --SawnOffShotgun
	0xE284C527, --AssaultShotgun
	0x9D61E50F, --BullpupShotgun

	0x5FC3C11, --SniperRifle
	0xC472FE2, --HeavySniper

	0xA284510B, --GrenadeLauncher
	0x4DD2DC56, --GrenadeLauncherSmoke
	0xB1CA77B1, --RPG
	0x42BF8A85, --Minigun

	0x93E220BD, --Grenade
	-- 0x2C3731D9, --StickyBomb
	-- 0xFDBC8A50, --SmokeGrenade
	-- 0xA0973D5E, --BZGas
	-- 0x24B17070, --Molotov

	-- 0x60EC506, --FireExtinguisher
	-- 0x34A67B97, --PetrolCan

	0xBFD21232, --SNSPistol
	0xC0A3098D, --SpecialCarbine
	0xD205520E, --HeavyPistol
	0x7F229F94, --BullpupRifle

	0x63AB0442, --HomingLauncher
	0xAB564B93, --ProximityMine
	--0x787F0BB, --Snowball
	--0x83839C4, --VintagePistol
	--0x92A27487, --Dagger
	-- 0x7F7497E5, --Firework
	-- 0xA89CB99E, --Musket
	-- 0xC734385A, --MarksmanRifle
	0x3AABBBAA, --HeavyShotgun
	0x61012683, --Gusenberg
	-- 0xF9DCBF2D, --Hatchet
	0x6D544C99, --Railgun
}

