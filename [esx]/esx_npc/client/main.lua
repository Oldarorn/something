local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57, 
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177, 
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70, 
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

ESX = nil

NPCs = {}

local HasAlreadyEnteredNPCZone  = false
local CurrentAction             = nil
local CurrentActionMsg          = ''
local CurrentActionData         = {}
local ClosestNPC				= nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

AddEventHandler('playerSpawned', function()
	ESX.TriggerServerCallback('esx_npc:isFirstSpawn', function(FirstSpawned)
		if FirstSpawned then
			TriggerServerEvent('esx:clientLog', '########## FirstSpawned')
			SpawnNPCs()
		else
			TriggerServerEvent('esx_npc:getNPCs')
		end
	end)
end)

RegisterNetEvent('esx_npc:getNPCs')
AddEventHandler('esx_npc:getNPCs', function(npcs)
	if npcs == nil then
		TriggerServerEvent('esx_npc:getNPCs')
	else
		NPCs = npcs
	end
end)

function SpawnNPCs()
	TriggerServerEvent('esx:clientLog', '#################### Spawning NPCs')
	for k,v in pairs(Config.NPC) do
		TriggerServerEvent('esx:clientLog', '#################### v.pedType:            ' .. v.pedType)
		TriggerServerEvent('esx:clientLog', '#################### v.hash:               ' .. v.hash)
		TriggerServerEvent('esx:clientLog', '#################### GetHashKey(v.hash):   ' .. GetHashKey(v.hash))
		TriggerServerEvent('esx:clientLog', '#################### v.x:                  ' .. v.x)
		TriggerServerEvent('esx:clientLog', '#################### v.y:                  ' .. v.y)
		TriggerServerEvent('esx:clientLog', '#################### v.z:                  ' .. v.z)
		TriggerServerEvent('esx:clientLog', '#################### v.heading:            ' .. v.heading)
		TriggerServerEvent('esx:clientLog', '#################### v.wanderRadius:       ' .. v.wanderRadius)
		TriggerServerEvent('esx:clientLog', '#################### v.minWanderDistance:  ' .. v.minWanderDistance)
		TriggerServerEvent('esx:clientLog', '#################### v.timeBetweenWanders: ' .. v.timeBetweenWanders)

		local model = GetHashKey(v.hash)

		Citizen.CreateThread(function()
			RequestModel(model)
			while not HasModelLoaded(model) do
				Wait(0)
			end
			TriggerServerEvent('esx:clientLog', '#################### HasModelLoaded')
			local npc = CreatePed(v.pedType, model, v.x, v.y, v.z, v.heading, true, true)
			table.insert(NPCs, {npc = npc, data = v})

			GiveWeaponToPed(npc, 'WEAPON_SMG', 0, false, false)
			TriggerServerEvent('esx:clientLog', '#################### npc: ' .. tostring(npc))
			if v.action == 'WanderInArea' then
				WanderInArea(npc)
				--TaskWanderInArea(npc, v.x, v.y, v.z, v.wanderRadius, v.minWanderDistance, v.timeBetweenWanders)
			elseif v.action == 'OtherAction' then
				TriggerServerEvent('esx:clientLog', '#################### OtherAction')
				TaskWanderInArea(npc, v.x, v.y, v.z, v.wanderRadius, v.minWanderTime, v.timeBetweenWanders)
			end
		end)

	end

	TriggerServerEvent('esx:clientLog', '########## loaded: ' .. json.encode(NPCs[1]))

	TriggerServerEvent('esx_npc:setNPCs', NPCs)
end

function StopAction(npc)
	TriggerServerEvent('esx:clientLog', '#################### StopAction')
end

function WanderInArea(npc)
	TriggerServerEvent('esx:clientLog', '#################### WanderInArea')
	for i = 1, #NPCs, 1 do
		if NPCs[i].npc == npc then
			TaskWanderInArea(npc, NPCs[i].data.x, NPCs[i].data.y, NPCs[i].data.z, NPCs[i].data.wanderRadius, NPCs[i].data.minWanderDistance, NPCs[i].data.timeBetweenWanders)
			break
		end
	end	
end

function AttackPlayer(npc)
	for i = 1, #NPCs, 1 do
		if NPCs[i].npc == npc then
			NPCs[i].data.isAngryAtPlayer = true
			break
		end
	end

	local playerPed = GetPlayerPed(-1)
	TaskCombatPed(npc, playerPed, 0, 16)
end

RegisterNetEvent('esx_npc:attackPlayer')
AddEventHandler('esx_npc:attackPlayer', function()
	AttackPlayer(ClosestNPC)
end)

AddEventHandler('esx_npc:hasExitedNPCZone', function(npc, data)
	TriggerServerEvent('esx:clientLog', '#################### hasExitedNPCZone')
	ESX.UI.Menu.CloseAll()
	CurrentAction = nil
end)

AddEventHandler('esx_npc:hasEnteredNPCZone', function(npc, data)
	TriggerServerEvent('esx:clientLog', '#################### hasEnteredNPCZone')

	if data.playerIsCloseType == 'SellWeed' then

		TriggerServerEvent('esx:clientLog', '#################### SellWeed')
		CurrentAction     = data.playerIsCloseType
		CurrentActionMsg  = data.hint
		CurrentActionData = {npc = npc, data = data}
	end

end)

-- Player Is Close
Citizen.CreateThread(function()
	while true do
		Wait(0)
		local playerCoords = GetEntityCoords(GetPlayerPed(-1))
		for i = 1, #NPCs, 1 do
			local npcCoords = GetEntityCoords(NPCs[i].npc)
			local distance = GetDistanceBetweenCoords(playerCoords, npcCoords.x, npcCoords.y, npcCoords.z, true)
			if distance <= 3.0 and not HasAlreadyEnteredNPCZone then
				TriggerServerEvent('esx:clientLog', '#################### CLOSE')
				HasAlreadyEnteredNPCZone = true
				ClosestNPC = k
				TriggerEvent('esx_npc:hasEnteredNPCZone', NPCs[i].npc, NPCs[i].data)
			elseif distance > 3.0 and HasAlreadyEnteredNPCZone then
				TriggerServerEvent('esx:clientLog', '#################### NOT CLOSE ANYMORE')
				HasAlreadyEnteredNPCZone = false				
				ClosestNPC = nil
				TriggerEvent('esx_npc:hasExitedNPCZone', NPCs[i].npc, NPCs[i].data)
			end
		end
	end
end)

-- TASK_STAND_STILL(Ped ped, int time) // 0x919BE13EED931959 0x6F80965D
-- Makes the specified ped stand still for (time) milliseconds

-- Key Controls
Citizen.CreateThread(function()
	while true do

		Wait(0)

		if CurrentAction ~= nil and not CurrentActionData.data.isAngryAtPlayer then

			SetTextComponentFormat('STRING')
			AddTextComponentString(CurrentActionMsg)
			DisplayHelpTextFromStringLabel(0, 0, 1, -1)

			if IsControlJustReleased(0, Keys['E']) then
				TriggerServerEvent('esx:clientLog', '#################### E released')
				CurrentActionData.data.playerIsCloseAction()
				CurrentAction = nil
			end

		end

	end
end)
