ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('esx_atm:withdraw')
AddEventHandler('esx_atm:withdraw', function(amount)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	amount = tonumber(amount)
	local accountMoney = 0
	accountMoney = xPlayer.getAccount('bank').money
	if amount == nil or amount <= 0 or amount > accountMoney then
		TriggerClientEvent('esx:showNotification', _source, _U('invalid_amount'))
	else
		xPlayer.removeAccountMoney('bank', amount)
		xPlayer.addMoney(amount)
		TriggerClientEvent('esx:showNotification', _source, _U('withdraw_money') .. amount .. '~s~.')
		TriggerClientEvent('esx_atm:closeATM', _source)
	end
end)

ESX.RegisterUsableItem('piratebox', function(source)

	local xPlayer = ESX.GetPlayerFromId(source)
	TriggerClientEvent('esx_optionalneeds:onDrink', source)

end)

RegisterServerEvent('esx_optionalneeds:start')
AddEventHandler('esx_optionalneeds:start', function(amount)
		local xPlayer = ESX.GetPlayerFromId(source)

		xPlayer.removeInventoryItem('piratebox', 1)
end)