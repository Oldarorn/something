local GUI          			  = {}
local hasAlreadyEnteredMarker = false
local isInBanquesMarker 	  = false
local menuIsShowed   		  = false

ESX = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

RegisterNetEvent('esx_banque:closeBanque')
AddEventHandler('esx_banque:closeBanque', function()
	SetNuiFocus(false)
	menuIsShowed = false
	SendNUIMessage({
		hideAll = true
	})
end)

RegisterNUICallback('escape', function(data, cb)
  	TriggerEvent('esx_banque:closeBanque')
	cb('ok')
end)

RegisterNUICallback('deposit', function(data, cb)
	TriggerServerEvent('esx_banque:deposit', data.amount)
	cb('ok')
end)

RegisterNUICallback('withdraw', function(data, cb)
	TriggerServerEvent('esx_banque:withdraw', data.amount)
	cb('ok')
end)

-- Create blips
Citizen.CreateThread(function()
    for i=1, #Config.Banques, 1 do    	
    	local blip = AddBlipForCoord(Config.Banques[i].x, Config.Banques[i].y, Config.Banques[i].z - Config.ZDiff)      
		SetBlipSprite (blip, Config.BlipSprite)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 0.9)
		SetBlipColour (blip, 2)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("Banque")
		EndTextCommandSetBlipName(blip)
    end
end)

-- Render markers
Citizen.CreateThread(function()
	while true do		
		Wait(0)		
		local coords = GetEntityCoords(GetPlayerPed(-1))		
		for i=1, #Config.Banques, 1 do
			if(GetDistanceBetweenCoords(coords, Config.Banques[i].x, Config.Banques[i].y, Config.Banques[i].z, true) < Config.DrawDistance) then
				DrawMarker(Config.MarkerType, Config.Banques[i].x, Config.Banques[i].y, Config.Banques[i].z - Config.ZDiff, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.ZoneSize.x, Config.ZoneSize.y, Config.ZoneSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
			end
		end
	end
end)

-- Activate menu when player is inside marker
Citizen.CreateThread(function()
	while true do		
		Wait(0)		
		local coords = GetEntityCoords(GetPlayerPed(-1))
		isInBanquesMarker = false
		for i=1, #Config.Banques, 1 do
			if(GetDistanceBetweenCoords(coords, Config.Banques[i].x, Config.Banques[i].y, Config.Banques[i].z, true) < Config.ZoneSize.x / 2) then
				isInBanquesMarker = true
				SetTextComponentFormat('STRING')
				AddTextComponentString(_U('press_e_Banques'))
				DisplayHelpTextFromStringLabel(0, 0, 1, -1)
			end
		end
		if isInBanquesMarker and not hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = true
		end
		if not isInBanquesMarker and hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = false
			SetNuiFocus(false)	
				menuIsShowed = false	
				SendNUIMessage({
					hideAll = true
			})
		end
	end
end)

-- Menu interactions
Citizen.CreateThread(function()
	while true do
	  	Wait(0)
	    if menuIsShowed then
			DisableControlAction(0, 1,   true) -- LookLeftRight
			DisableControlAction(0, 2,   true) -- LookUpDown
			DisableControlAction(0, 142, true) -- MeleeAttackAlternate
			DisableControlAction(0, 106, true) -- VehicleMouseControlOverride
			if IsDisabledControlJustReleased(0, 142) then -- MeleeAttackAlternate
				SendNUIMessage({
					click = true
				})
			end
	    else
		  	if IsControlJustReleased(0, 38) and isInBanquesMarker then
		  		menuIsShowed = true
				ESX.TriggerServerCallback('esx:getPlayerData', function(data)				    
				    SendNUIMessage({
						showMenu = true,
						player   = {
							money = data.money,
							accounts = data.accounts
						}
					})
				end)
				SetNuiFocus(true)
			end
	    end
	end
end)