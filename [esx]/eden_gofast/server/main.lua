ESX = nil
CopsConnected = 0

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)


AddEventHandler('esx:playerLoaded', function(source)

	local xPlayer = ESX.GetPlayerFromId(source)

	if xPlayer.job.name == 'police' then
		CopsConnected = CopsConnected + 1
	end

	if xPlayer.job.name == 'sherif' then
		CopsConnected = CopsConnected + 1
	end

end)

AddEventHandler('esx:playerDropped', function(source)

	local xPlayer = ESX.GetPlayerFromId(source)

	if xPlayer.job.name == 'police'  then
		CopsConnected = CopsConnected - 1
	end

	if xPlayer.job.name == 'sherif'  then
		CopsConnected = CopsConnected - 1
	end

end)

AddEventHandler('esx:setJob', function(source, job, lastJob)

	local xPlayer = ESX.GetPlayerFromId(source)

	if job.name == 'police' and lastJob.name ~= 'police' then
		CopsConnected = CopsConnected + 1
	end

	if job.name ~= 'police' and lastJob.name == 'police' then
		CopsConnected = CopsConnected - 1
	end

	if job.name == 'sherif' and lastJob.name ~= 'sherif' then
		CopsConnected = CopsConnected + 1
	end

	if job.name ~= 'sherif' and lastJob.name == 'sherif' then
		CopsConnected = CopsConnected - 1
	end

end)

----------------------------------------------------------------------
------------------------------- OTHER  -------------------------------
----------------------------------------------------------------------

RegisterServerEvent("eden_gofast:check")
AddEventHandler("eden_gofast:check", function(caution)
    local _source        = source
	local xPlayer        = ESX.GetPlayerFromId(_source)
	local xPlayers 		 = ESX.GetPlayers()
	local time 		     = os.date("%d/%m/%y %X")
	local pack_drugs 	 = xPlayer.getInventoryItem('pack_drugs').count
	
	if pack_drugs >= 1 then
		TriggerClientEvent('esx:showNotification', source,  _U('have_drug'))
	else
			
		if CopsConnected >= 0 then
		
				TriggerClientEvent('eden_gofast:start', _source)
				print('GostFast lance par : ' ..xPlayer.name .. ' a : ' .. time)
				xPlayer.addInventoryItem('pack_drugs', 1)
				
				for i=1, #xPlayers, 1 do
					local xPlayer2 = ESX.GetPlayerFromId(xPlayers[i])
					if xPlayer2.job.name == 'police' then
						TriggerClientEvent('esx:showNotification', xPlayers[i], _U('police_start'))
					end
					if xPlayer2.job.name == 'sherif' then
						TriggerClientEvent('esx:showNotification', xPlayers[i], _U('police_start'))
					end
				end

		else	
			TriggerClientEvent('esx:showNotification', _source, _U('need_cop'))
		end
	end
end)

RegisterServerEvent('eden_gofast:remove_caution')
AddEventHandler('eden_gofast:remove_caution', function(caution,localisation)
	local _source        = source
	local xPlayer        = ESX.GetPlayerFromId(_source)	

	xPlayer.removeMoney(caution)

end)

RegisterServerEvent('eden_gofast:itemremove')
AddEventHandler('eden_gofast:itemremove', function(reward)

	local _source		= source
	local xPlayer  		= ESX.GetPlayerFromId(source)
	local xPlayers 		= ESX.GetPlayers()
	local pack_drugs 	= xPlayer.getInventoryItem('pack_drugs').count
	local time 		     = os.date("%d/%m/%y %X")

	if pack_drugs < 1 then
        TriggerClientEvent('esx:showNotification', source, _U('dont_have_pack_drugs'))
		TriggerClientEvent('eden_gofast:ragepnj', _source)
		print('GostFast fini par : ' ..xPlayer.name .. ' a : ' .. time .. '// Fail pas de drogue avec lui' )

	else

		for i=1, #xPlayers, 1 do
			local xPlayer2 = ESX.GetPlayerFromId(xPlayers[i])
			if xPlayer2.job.name == 'police' then
				TriggerClientEvent('esx:showNotification', xPlayers[i], _U('police_finish'))
			end
			if xPlayer2.job.name == 'sherif' then
				TriggerClientEvent('esx:showNotification', xPlayers[i], _U('police_finish'))
			end
		end
		
		xPlayer.addMoney(reward)
		TriggerClientEvent('eden_gofast:recuperation', _source)
		TriggerClientEvent('esx:showNotification', source, _U('your_reward') ..reward.. '$')
		xPlayer.removeInventoryItem('pack_drugs', 1)

		print('GostFast fini par : ' ..xPlayer.name .. ' a : ' .. time .. '// Gain : '..reward..'$' )
		   
	end

end)

RegisterServerEvent('eden_gofast:fail')
AddEventHandler('eden_gofast:fail', function(reward)

	local _source		= source
	local xPlayer  		= ESX.GetPlayerFromId(source)
	local xPlayers 		= ESX.GetPlayers()
	local pack_drugs 	= xPlayer.getInventoryItem('pack_drugs').count

	if pack_drugs < 1 then
        TriggerClientEvent('eden_gofast:ragepnj', _source)
	else

		local _source        = source
		local xPlayer        = ESX.GetPlayerFromId(_source)
		local time 		     = os.date("%d/%m/%y %X")

		xPlayer.removeInventoryItem('pack_drugs', 1)
		print('GostFast fini par : ' ..xPlayer.name .. ' a : ' .. time .. '// Mission FAIL ')
		   
	end

end)

RegisterServerEvent('eden_gofast:failrun')
AddEventHandler('eden_gofast:failrun', function()

	local _source		= source
	local xPlayer  		= ESX.GetPlayerFromId(source)
	local time 		    = os.date("%d/%m/%y %X")

	TriggerClientEvent('esx_phone:onMessage', xPlayer.source, '', _U('fail'), "" , true, '~b~Nouveau message~s~', false)
	print('GostFast fini par : ' ..xPlayer.name .. ' a : ' .. time .. '// Mission FAIL ')
end)

---------------------------------------------------------
---------------------  USE pack_drugs -------------------
---------------------------------------------------------
ESX.RegisterUsableItem('pack_drugs', function(source)
	
	local _source		= source
	local xPlayer  		= ESX.GetPlayerFromId(source)
	local xPlayers 		= ESX.GetPlayers()
	local xPlayer2 		= ESX.GetPlayerFromId(xPlayers[i])
	local coke 			= math.random(500, 900)
	local meth 			= math.random(500, 900)
	local weed 			= math.random(500, 900)
	local random 		= math.random(1, 100)

	xPlayer.addInventoryItem('coke', coke)
	xPlayer.addInventoryItem('meth', meth)
	xPlayer.addInventoryItem('weed', weed)
	xPlayer.removeInventoryItem('pack_drugs', 1 )

	if random >= 1 and random <= 25 then
		xPlayer.addInventoryItem('blackberry', 1 )
	end
	if random >= 25 and random <= 50 then 
		xPlayer.addInventoryItem('kalilinux', 1 )
	end
	if random >= 50 and random <= 60 then
		xPlayer.addInventoryItem('cagoule', 1 )	
	end
	if random >= 60 and random <= 70 then
		xPlayer.addInventoryItem('silencieux', 1 )	
	end
	if random >= 70 and random <= 80 then
		xPlayer.addInventoryItem('flashlight', 1 )		
	end
	if random >= 80 and random <= 90 then
		xPlayer.addInventoryItem('grip', 1 )		
	end
	if random >= 90 and random <= 99 then
		xPlayer.addInventoryItem('bulletproof', 1 )		
	end
	if random >= 99 and random <= 100 then
		xPlayer.addInventoryItem('attacklisence', 1 )	
	end

	for i=1, #xPlayers, 1 do
		local xPlayer2 = ESX.GetPlayerFromId(xPlayers[i])
		if xPlayer2.job.name == 'police' then
			TriggerClientEvent('esx_phone:onMessage', xPlayer2.source, '', 'Une ~b~malette~s~ viens de s\'ouvrir. Acutalisation en cours', xPlayer.get('coords'), true, 'Malette', true)
			Wait(120000)
			TriggerClientEvent('esx_phone:onMessage', xPlayer2.source, '', 'Actualisation de la position', xPlayer.get('coords'), true, 'Malette', true)
		end
		if xPlayer2.job.name == 'sherif' then
			TriggerClientEvent('esx_phone:onMessage', xPlayer2.source, '', 'Une ~b~malette~s~ viens de s\'ouvrir. Acutalisation en cours', xPlayer.get('coords'), true, 'Malette', true)
			Wait(120000)
			TriggerClientEvent('esx_phone:onMessage', xPlayer2.source, '', 'Actualisation de la position', xPlayer.get('coords'), true, 'Malette', true)
		end
	end

end)

