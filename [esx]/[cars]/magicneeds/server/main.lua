ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)


--[[-- Stéroïdes
ESX.RegisterUsableItem('steroids', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('steroids', 1)
	TriggerClientEvent('esx_eden_basicneeds:onEatSteroids', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_steroid'))
end)

-- Huile pour cheveux
ESX.RegisterUsableItem('hair_oil', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('hair_oil', 1)
	TriggerClientEvent('esx_eden_basicneeds:onMoussaStuff', source, 'hair_oil')
	TriggerClientEvent('esx:showNotification', source, _U('used_hair_oil'))
end)

-- Champagne
ESX.RegisterUsableItem('champagne', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('champagne', 1)
	TriggerClientEvent('esx_eden_basicneeds:onMoussaStuff', source, 'champagne')
	TriggerClientEvent('esx:showNotification', source, _U('used_champagne'))
end)

-- Carte de Moussa
ESX.RegisterUsableItem('moussa_card', function(source)
	TriggerClientEvent('esx_eden_basicneeds:onMoussaStuff', source, 'moussa_card')
end)--]]

-- Cape
ESX.RegisterUsableItem('cape', function(source)
  local xPlayer = ESX.GetPlayerFromId(source)
  xPlayer.removeInventoryItem('cape', 1)
  TriggerClientEvent('magicneeds:onCape', source)
  TriggerClientEvent('esx:showNotification', source, ('Vous venez de mettre une cape d\'invisilité, attention elle semble de mauvaise qualitée'))
end)


