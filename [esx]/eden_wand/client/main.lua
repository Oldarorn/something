local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

ESX                             = nil

local PlayerData                = {}
local GUI                       = {}
GUI.Time                        = 0
local secondsRemaining          = 0
local IsHandcuffed              = false

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  PlayerData = xPlayer
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	PlayerData.job = job
end)

local options = {
  x = 0.900,
  y = 0.32,
  width = 0.2,
  height = 0.04,
  scale = 0.4,
  font = 2,
  menu_title = "Sorts",
  menu_subtitle = "Sorts",
  color_r = 100,
  color_g = 130,
  color_b = 130,
}

-- Cuting serflex
 Citizen.CreateThread(function()
	local isKnife = false

	while true do
		Citizen.Wait(0)

    	local ped = GetPlayerPed(-1)
		local currentWeaponHash = GetSelectedPedWeapon(ped)

		if currentWeaponHash == 1198879012 then

			SetPedInfiniteAmmo(GetPlayerPed(-1), true)
			SetPedInfiniteAmmoClip(GetPlayerPed(-1), true)
			SetPedAmmo(GetPlayerPed(-1), (GetSelectedPedWeapon(GetPlayerPed(-1))), 999)
			PedSkipNextReloading(GetPlayerPed(-1))
		end


	end
end)



function DemarchesMenu()
    ped = GetPlayerPed(-1);
	options.menu_subtitle = "Menu des sorts"
    ClearMenu()
	Menu.addButton(('Sorts disponibles'), "demHumeurs", nil)
	Menu.addButton(('Sorts indisponibles'), "demHumeurs", nil)
    Menu.addButton("~c~---------------------------", "quitter", nil)
    Menu.addButton('~o~Quitter', "quitter", nil)
end




function demHumeurs()
    options.menu_subtitle = "~b~Sorts legaux"
    ClearMenu()
							if PlayerData.job.grade_name == 'boss' then
							Menu.addButton("Désarmement", 	"humeurs1",nil)
							end
							Menu.addButton("Métamorphose",      	"humeurs2",nil)
							Menu.addButton("Lévitation", 	"humeurs4",nil)
							Menu.addButton("~r~Repousse",  		"humeurs5",nil)
							Menu.addButton("~c~Douleurs",		"humeurs6",nil)

							Menu.addButton("~c~---------------------------", "DemarchesMenu", nil)
							Menu.addButton('~o~Quitter', "reset", nil)
end



						local player, distance = ESX.Game.GetClosestPlayer()


							function humeurs1()
								local pid = PlayerPedId()
								if distance ~= -1 and distance <= 3.0 then
								TriggerServerEvent('esx_policejob:handcuff', GetPlayerServerId(player))


							end
						end
								function humeurs2()
								local pid = PlayerPedId()

								if not IsPedSittingInAnyVehicle(pid) then
								RequestAnimSet("move_m@sad@a")
								while (not HasAnimSetLoaded("move_m@sad@a")) do Citizen.Wait(0) end
								SetPedMovementClipset(GetPlayerPed(-1), "move_m@sad@a", -1)


								end
							end







							function humeurs4()
								local pid = PlayerPedId()

								if not IsPedSittingInAnyVehicle(pid) then
								RequestAnimSet("move_m@depressed@a")
								while (not HasAnimSetLoaded("move_m@depressed@a")) do Citizen.Wait(0) end
								SetPedMovementClipset(GetPlayerPed(-1), "move_m@depressed@a", -1)


								end

							end

							function humeurs5()
								local pid = PlayerPedId()

								if not IsPedSittingInAnyVehicle(pid) then
								RequestAnimSet("move_characters@michael@fire")
								while (not HasAnimSetLoaded("move_characters@michael@fire")) do Citizen.Wait(0) end
								SetPedMovementClipset(GetPlayerPed(-1), "move_characters@michael@fire", -1)


								end

							end


							function humeurs6()
								local pid = PlayerPedId()

								if not IsPedSittingInAnyVehicle(pid) then
								RequestAnimSet("move_m@quick")
								while (not HasAnimSetLoaded("move_m@quick")) do Citizen.Wait(0) end
								SetPedMovementClipset(GetPlayerPed(-1), "move_m@quick", -1)


								end

							end





							function humeurs9()
								local pid = PlayerPedId()

								if not IsPedSittingInAnyVehicle(pid) then
								RequestAnimSet("move_m@confident")
								while (not HasAnimSetLoaded("move_m@confident")) do Citizen.Wait(0) end
								SetPedMovementClipset(GetPlayerPed(-1), "move_m@confident", -1)


								end

							end


							function humeurs10()
								local pid = PlayerPedId()

								if not IsPedSittingInAnyVehicle(pid) then
								RequestAnimSet("move_m@casual@b")
								while (not HasAnimSetLoaded("move_m@casual@b")) do Citizen.Wait(0) end
								SetPedMovementClipset(GetPlayerPed(-1), "move_m@casual@b", -1)


								end

							end


							function humeurs11()
								local pid = PlayerPedId()

								if not IsPedSittingInAnyVehicle(pid) then
								RequestAnimSet("move_m@hurry@a")
								while (not HasAnimSetLoaded("move_m@hurry@a")) do Citizen.Wait(0) end
								SetPedMovementClipset(GetPlayerPed(-1), "move_m@hurry@a", -1)


								end

							end


							function humeurs12()
								local pid = PlayerPedId()

								if not IsPedSittingInAnyVehicle(pid) then
								RequestAnimSet("move_characters@jimmy@slow@")
								while (not HasAnimSetLoaded("move_characters@jimmy@slow@")) do Citizen.Wait(0) end
								SetPedMovementClipset(GetPlayerPed(-1), "move_characters@jimmy@slow@", -1)


								end

							end



function reset()
local pid = PlayerPedId()
ResetPedMovementClipset(GetPlayerPed(-1), 0)
quitter()
end

function quitter ()
Menu.hidden = true
end



-- Key Controls


Citizen.CreateThread(function()
    while true do
		local ped = GetPlayerPed(-1)
		local currentWeaponHash = GetSelectedPedWeapon(ped)
        Citizen.Wait(0)
		if IsControlJustPressed(1, Keys["BACKSPACE"]) then -- Si touche retour on cache
			Menu.hidden = true
		end
        if currentWeaponHash == 1198879012 and IsControlJustPressed(1, Keys["F9"]) then --Modifier L'ouverture du Menu Ici ! Touche F3 de base.
            DemarchesMenu()
			-- Menu to draw
            Menu.hidden = not Menu.hidden -- Hide/Show the menu
        end
		Menu.renderGUI(options) -- Draw menu on each tick if Menu.hidden = false
    end
end)




local relationshipTypes = {
"PLAYER",
"CIVMALE",
"CIVFEMALE",
"COP",
"SECURITY_GUARD",
"PRIVATE_SECURITY",
"FIREMAN",
"GANG_1",
"GANG_2",
"GANG_9",
"GANG_10",
"AMBIENT_GANG_LOST",
"AMBIENT_GANG_MEXICAN",
"AMBIENT_GANG_FAMILY",
"AMBIENT_GANG_BALLAS",
"AMBIENT_GANG_MARABUNTE",
"AMBIENT_GANG_CULT",
"AMBIENT_GANG_SALVA",
"AMBIENT_GANG_WEICHENG",
"AMBIENT_GANG_HILLBILLY",
"DEALER",
"HATES_PLAYER",
"HEN",
"WILD_ANIMAL",
"SHARK",
"COUGAR",
"NO_RELATIONSHIP",
"SPECIAL",
"MISSION2",
"MISSION3",
"MISSION4",
"MISSION5",
"MISSION6",
"MISSION7",
"MISSION8",
"ARMY",
"AGGRESSIVE_INVESTIGATE",
"MEDIC",

}

local RELATIONSHIP_HATE = 5

Citizen.CreateThread(function()
    while true do
		local ped = GetPlayerPed(-1)
		local currentWeaponHash = GetSelectedPedWeapon(ped)
		local vehicle   = GetVehiclePedIsIn(GetPlayerPed(-1),  false)
        Wait(50)

        for _, group in ipairs(relationshipTypes) do
			 if currentWeaponHash == 1198879012 then
            -- not sure about argument order, players don't have AI so only one of these should be needed
            SetRelationshipBetweenGroups(RELATIONSHIP_HATE, GetHashKey('PLAYER'), GetHashKey(group))
            SetRelationshipBetweenGroups(RELATIONSHIP_HATE, GetHashKey(group), GetHashKey('PLAYER'))
			end
			if GetEntityModel(vehicle) == GetHashKey('nimbus16') then
			  SetRelationshipBetweenGroups(RELATIONSHIP_HATE, GetHashKey('PLAYER'), GetHashKey(group))
            SetRelationshipBetweenGroups(RELATIONSHIP_HATE, GetHashKey(group), GetHashKey('PLAYER'))
			end
        end
    end
end)




