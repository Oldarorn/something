ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)


--Stock les véhicules
ESX.RegisterServerCallback('esx_eden_mexicain:checkContract',function(source,cb, label)
	local isFound = false
	local label   = label
	print(label)
	MySQL.Async.fetchAll("SELECT * FROM receleur_vehicles WHERE label=@label",{['@label'] = label}, function(data)
		if data[1] ~= nil then
			if data[1].identifier then				
				cb("Contrat déjà rempli")
			else
				cb("Contrat rempli. Contactez le Mexicain")
			end
		else
			cb("Pas de contrat sur ce véhicule")
		end
	end)
end)

RegisterServerEvent("esx_eden_mexicain:validContract")
AddEventHandler("esx_eden_mexicain:validContract", function(label, vehicleProps)	
	local _source      = source
	local xPlayer      = ESX.GetPlayerFromId(_source)
	local label        = label
	local vehicleProps = json.encode(vehicleProps)

	MySQL.Async.execute("UPDATE receleur_vehicles SET identifier = @identifier, vehicle =@vehicleProps WHERE label=@label",{['@vehicleProps'] = vehicleProps, ['@identifier'] = xPlayer.getIdentifier(), ['@label'] = label})
end)