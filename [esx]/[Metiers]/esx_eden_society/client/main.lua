local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57, 
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177, 
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70, 
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local PlayerData              = {}
local menuIsShowed			  = false
local hintIsShowed			  = false


local hasAlreadyEnteredMarker = false
local Blips                   = {}
local JobBlips                = {}

local collectedItem           = nil
local collectedQtty			  = 0
local collectedQttyUpToDate   = false
local previousCollectedItem   = nil
local previousCollectedQtty	  = 0
local isInMarker              = false
local isInPublicMarker        = false

local newTask                 = false
local hintToDisplay           = "no hint to display"
local jobDone                 = false
local onDuty				  = false
local moneyInBank			  = 0

local spawner 				  = 0
local myPlate                 = {}

local isJobVehicleDestroyed   = false

local cautionVehicleInCaseofDrop 	= 0
local maxCautionVehicleInCaseofDrop = 0
local vehicleObjInCaseofDrop		= nil
local vehicleInCaseofDrop 			= nil
local vehicleHashInCaseofDrop 		= nil
local vehicleMaxHealthInCaseofDrop	= nil
local vehicleOldHealthInCaseofDrop	= nil

local societyName   = nil
local societyConfig = {}

ESX = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
    PlayerData = xPlayer
    --TriggerServerEvent('esx_jobs:giveBackCautionInCaseOfDrop')
    refreshBlips()
end)

AddEventHandler('esx_eden_society:publicTeleports', function(position)
	SetEntityCoords(GetPlayerPed(-1), position.x, position.y, position.z)
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	PlayerData.job = job
	onDuty = false
	myPlate = {} -- loosing vehicle caution in case player changes job.
	isJobVehicleDestroyed = false
	spawner = 0
	deleteBlips()
	refreshBlips()
end)

AddEventHandler('esx_eden_society:hasExitedMarker', function(zone)
	ESX.UI.Menu.CloseAll()
	TriggerServerEvent('esx_eden_society:stopWork')
	hintToDisplay = "no hint to display"
	menuIsShowed = false
	hintIsShowed = false
	isInMarker = false
end)

AddEventHandler('esx_eden_society:spawnJobVehicle', function(spawnPoint, vehicle)
	local playerPed = GetPlayerPed(-1)
	local coords = spawnPoint.Pos
	local vehicleModel = GetHashKey(vehicle.Hash)

	RequestModel(vehicleModel)
	while not HasModelLoaded(vehicleModel) do
		Citizen.Wait(0)
	end

	local plate = math.random(100, 900)
	if not IsAnyVehicleNearPoint(coords.x, coords.y, coords.z, 5.0) then
		local veh = CreateVehicle(vehicleModel, coords.x, coords.y, coords.z, spawnPoint.Heading, true, false)

		if vehicle.Trailer ~= "none" then
			RequestModel(vehicle.Trailer)

			while not HasModelLoaded(vehicle.Trailer) do
				Citizen.Wait(0)
			end

			local trailer = CreateVehicle(vehicle.Trailer, coords.x, coords.y, coords.z, spawnPoint.Heading, true, false)
			AttachVehicleToTrailer(veh, trailer, 1.1)
		end

		SetVehicleHasBeenOwnedByPlayer(veh,  true)
		SetEntityAsMissionEntity(veh,  true,  true)
		local id = NetworkGetNetworkIdFromEntity(veh)
		SetNetworkIdCanMigrate(id, true)

		local platePrefix = "WORK"
		for k,v in pairs(Config.Plates) do
			if PlayerData.job.name == k then
				platePrefix = v
			end
		end		
		plate = platePrefix .. plate
		SetVehicleNumberPlateText(veh, plate)
		table.insert(myPlate, plate)
		plate = string.gsub(plate, " ", "")
		TriggerEvent('esx_vehiclelock:updatePlayerCars', "add", plate)
		SetVehRadioStation(veh, "OFF")
		TaskWarpPedIntoVehicle(playerPed, veh, -1)
		isJobVehicleDestroyed = false

		TriggerServerEvent('esx_eden_society:removeVehicle',vehicle.Hash, societyName)
	else
		ESX.ShowNotification("Un véhicule empêche la sortie")
	end
end)

AddEventHandler('esx_eden_society:action', function(zone)
	menuIsShowed = true
	if zone.Type == "cloakroom" then
		OpenCloackroom()
	elseif zone.Type == "work" then
		hintToDisplay = "no hint to display"
		hintIsShowed = false
		local playerPed = GetPlayerPed(-1)
		local vehicle = GetVehiclePedIsIn(playerPed, true)
		local vehmodel = GetHashKey(societyConfig.Vehicles.Truck.Hash)
		local isVehicleApropiate = IsVehicleModel(vehicle, vehmodel)		
		if(not isVehicleApropiate) then
			TriggerEvent('esx:showNotification', "Pas le bon véhicule de service")
		elseif IsPedInAnyVehicle(playerPed, 0) then
			TriggerEvent('esx:showNotification', 'Vous devez être à pied pour pouvoir travailler.')
		else
			TriggerServerEvent('esx_eden_society:startWork', zone.Item, societyConfig)
		end
	elseif zone.Type == "vehspawner" then
		OpenGarage()
	elseif zone.Type == "vehdelete" then
		local playerPed = GetPlayerPed(-1)
		local vehicle   = GetVehiclePedIsIn(playerPed,  false)

		local platePrefix = "WORK"
		for k,v in pairs(Config.Plates) do
			if PlayerData.job.name == k then
				platePrefix = v
			end
		end
		
		local plate = tostring(GetVehicleNumberPlateText(vehicle))

		if(string.find(plate,platePrefix) == nil) then
			ESX.ShowNotification("Ce n'est pas un véhicule de votre entreprise")
		elseif (GetEntityModel(vehicle) == GetHashKey(societyConfig.Vehicles.Truck.Hash)) then
			DeleteVehicle(vehicle)
			--TriggerServerEvent('esx_eden_society:addToDataStore',societyConfig.Vehicles.Truck.Hash, job)
			ESX.TriggerServerCallback('esx_eden_society:addVehicle', function() end, societyConfig.Vehicles.Truck.Hash, societyName)
		else
			ESX.ShowNotification('Vous ne pouvez ranger que les '..societyConfig.Vehicles.Truck.Hash)		
		end
	elseif zone.Type == "delivery" then
		if Blips['delivery'] ~= nil then
			RemoveBlip(Blips['delivery'])
			Blips['delivery'] = nil
		end
		hintToDisplay = "no hint to display"
		hintIsShowed = false
		TriggerServerEvent('esx_eden_society:startWork', zone.Item, societyConfig)
	elseif zone.Type == "office" and PlayerData.job.grade_name == 'boss' then
		OpenBossMenu()
	end
	--nextStep(zone.GPS)
end)

function nextStep(gps)
	if gps ~= 0 then
		if Blips['delivery'] ~= nil then
			RemoveBlip(Blips['delivery'])
			Blips['delivery'] = nil
		end
		Blips['delivery'] = AddBlipForCoord(gps.x, gps.y, gps.z)
		SetBlipRoute(Blips['delivery'], true)
		TriggerEvent('esx:showNotification', 'Rendez-vous à la prochaine étape après avoir complété celle-ci.')
	end
end

function deleteBlips()
	if JobBlips[1] ~= nil then
		for i=1, #JobBlips, 1 do
			RemoveBlip(JobBlips[i])
			JobBlips[i] = nil
		end
	end
end

function refreshBlips()
	local zones = {}
	local blipInfo = {}	
	if PlayerData.job ~= nil then
		for jobKey,jobValues in pairs(Config.Society) do
			if PlayerData.job.name == jobKey then
				for zoneKey,zoneValues in pairs(jobValues.Zones) do
					if zoneValues.Blip then
						local blip = AddBlipForCoord(zoneValues.Pos.x, zoneValues.Pos.y, zoneValues.Pos.z)
						SetBlipSprite (blip, jobValues.BlipInfos.Sprite)
						SetBlipDisplay(blip, 4)
						SetBlipScale  (blip, 1.2)
						SetBlipColour (blip, jobValues.BlipInfos.Color)
						SetBlipAsShortRange(blip, true)
						BeginTextCommandSetBlipName("STRING")
						AddTextComponentString(zoneValues.Name)
						EndTextCommandSetBlipName(blip)
						table.insert(JobBlips, blip)
					end
				end
			end
		end
	end
end


function OpenCloackroom()
	menuIsShowed = true

	local society = societyName

	local elements = {
		{label = 'Tenue civile', value = 'citizen_wear'},
		{label = 'Tenue de service', value = 'society_wear'}
	}

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'cloackroom_actions',
		{
			title    = society,
			elements = elements
		},
		function(data, menu)
			if data.current.value == 'citizen_wear' then				
				menu.close()
				onDuty = false

				ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin)
	    			TriggerEvent('skinchanger:loadSkin', skin)
				end)
			end			
			if data.current.value == 'society_wear' then				
				menu.close()
				onDuty = true

				ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
					if skin.sex == 0 then
    					TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_male)
					else
	    				TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_female)
					end
				end)
			end
		end,
		function(data, menu)
			menu.close()
		end
	)
end

function OpenRoomInventoryMenu()

  local societyName = ''
  local societyJobName = ''
  local societyBlackMoneyName = ''
  --coffre entreprise
  if PlayerData.job.name == 'defenders' then
	  societyName = 'society_defenders'
	  societyJobName = 'defenders'
	  societyBlackMoneyName = 'society_defenders_black_money'
  end

  if PlayerData.job.name == 'velvet' then
	  societyName = 'society_velvet'
	  societyJobName = 'velvet'
	  societyBlackMoneyName = 'society_velvet_black_money'
  end
  
  if PlayerData.job.name == 'cluckinbell' then
	  societyName = 'society_cluckinbell'
	  societyJobName = 'cluckinbell'
	  societyBlackMoneyName = 'society_cluckinbell_black_money'
  end
  
  local blackMoney = 0
  local items = {}
  local weapons = {}
  local elements = {}
  
  ESX.TriggerServerCallback('esx_society:getAccountMoney', function(money)
  
    blackMoney = money
	
	table.insert(elements, {label = 'Argent sale ' .. blackMoney .. '$', type = 'item_account', value = 'black_money'})
  end, societyBlackMoneyName)
	
  ESX.TriggerServerCallback('esx_society:getAccountItems', function(items)

    print(json.encode(items))

    for i=1, #items, 1 do
	  if items[i].count > 0 then
        table.insert(elements, {label = items[i].label .. ' x' .. items[i].count, type = 'item_standard', value = items[i].name})
      end
	end
	
	ESX.TriggerServerCallback('esx_jobs:getSocietyWeapons', function(weapons)

	for i=1, #weapons, 1 do
		if weapons[i].count > 0 then
			table.insert(elements, {label = ESX.GetWeaponLabel(weapons[i].name) .. ' x' .. weapons[i].count, type = 'item_weapon', value = weapons[i].name})
		end
	end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocksItems_menu',
      {
        title    = 'inventaire entreprise',
		align    = 'top-left',
        elements = elements
      },
      function(data, menu)
	  
		if data.current.type == 'item_weapon' then
		
			menu.close()

			ESX.TriggerServerCallback('esx_jobs:getWeapon', function()
				OpenRoomInventoryMenu()
			end, data.current.value, societyJobName)
			
			ESX.SetTimeout(300, function()
				OpenRoomInventoryMenu()
			end)
		
		else

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = 'quantité ?'
          },
          function(data2, menu)

            local count = tonumber(data2.value)
			
			menu.close()

            if count == nil then
              ESX.ShowNotification('montant invalide')
            else
              TriggerServerEvent('esx_jobs:getItem', data.current.type, data.current.value, count, societyJobName)
			  
              OpenRoomInventoryMenu()
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )
		
		end

      end,
      function(data, menu)
        menu.close()
      end
	 
    )
	
	end, societyJobName)

  end, societyName)

end
  --coffre entreprise
function OpenPlayerInventoryMenu()

  local societyJobName = ''
  
  if PlayerData.job.name == 'defenders' then
	societyJobName = 'defenders'
  end

  if PlayerData.job.name == 'velvet' then
	societyJobName = 'velvet'
  end
  
  if PlayerData.job.name == 'cluckinbell' then
	societyJobName = 'cluckinbell'
  end

  ESX.TriggerServerCallback('esx_jobs:getPlayerInventory', function(inventory)

    local elements = {}

    table.insert(elements, {label = 'Argent sale ' .. inventory.blackMoney .. '$', type = 'item_account', value = 'black_money'})

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end

    end

    local playerPed  = GetPlayerPed(-1)
    local weaponList = ESX.GetWeaponList()

    for i=1, #weaponList, 1 do

      local weaponHash = GetHashKey(weaponList[i].name)

      if HasPedGotWeapon(playerPed,  weaponHash,  false) and weaponList[i].name ~= 'WEAPON_UNARMED' then
        local ammo = GetAmmoInPedWeapon(playerPed, weaponHash)
        table.insert(elements, {label = weaponList[i].label, type = 'item_weapon', value = weaponList[i].name, ammo = ammo})
      end

    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'player_inventory',
      {
        title    = 'inventaire personnel',
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)

        if data.current.type == 'item_weapon' then

          menu.close()

          ESX.TriggerServerCallback('esx_jobs:putWeapon', function()
			  OpenPlayerInventoryMenu()
		  end, data.current.value, societyJobName)

          ESX.SetTimeout(300, function()
            OpenPlayerInventoryMenu()
          end)

        else

          ESX.UI.Menu.Open(
            'dialog', GetCurrentResourceName(), 'put_item_count',
            {
              title = 'quantité ?',
            },
            function(data2, menu)

              menu.close()

              TriggerServerEvent('esx_jobs:putItem', data.current.type, data.current.value, tonumber(data2.value), societyJobName)

              ESX.SetTimeout(300, function()
                OpenPlayerInventoryMenu()
              end)

            end,
            function(data2,menu)
              menu.close()
            end
          )

        end

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenGarage()
	menuIsShowed = true

	ESX.UI.Menu.CloseAll()

	ESX.TriggerServerCallback('esx_eden_society:getVehicle', function(vehicles)

		local elements = {}

		for i=1, #vehicles, 1 do
			if vehicles[i].count > 0 then
				table.insert(elements, {label = 'x' .. vehicles[i].count .. ' ' .. vehicles[i].name, value = vehicles[i].name})
			end
		end

		ESX.UI.Menu.Open(
			'default', GetCurrentResourceName(), 'spawn_vehicle',
			{
				title    = societyName,
				align    = 'top-left',
				elements = elements,
			},
			function(data, menu)

				menu.close()

				local playerPed = GetPlayerPed(-1)
				local coords    = societyConfig.Zones.VehicleSpawnPoint.Pos

				TriggerServerEvent("eden_garage:debug",coords)

				TriggerEvent('esx_eden_society:spawnJobVehicle',societyConfig.Zones.VehicleSpawnPoint,societyConfig.Vehicles.Truck)
			end,
			function(data, menu)
				menu.close()
			end
		)
		
	end, societyName)
end

function OpenBossMenu()
	menuIsShowed = true

	local societyName = societyName
	
	local elements = {
		{label = 'Achat véhicules de service', 		value = 'buy_vehicles'},
		{label = 'Retirer argent société',     		value = 'withdraw_society_money'},
  		{label = 'Déposer argent ',            		value = 'deposit_money'},
  		{label = 'Blanchir argent',            		value = 'wash_money'},
		{label = 'Recruter un employé',         	value = 'employee_recruit'},
		{label = 'Gestion des employés', 			value = 'employee_management'}
	}

	if PlayerData.job.grade_name == 'boss' then
		table.insert(elements, {label = 'Retirer du coffre entreprise', value = 'society_inventory'})
		table.insert(elements, {label = 'Deposer dans le coffre entreprise', value = 'player_inventory'})
	end	
	
	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'boss_actions',
		{
			title    = societyName,
			align    = 'top-left',
			elements = elements,
		},
		function(data, menu)
			if data.current.value == 'buy_vehicles' then
				menu.close()
				OpenBuyVehicleMenu()
			end
			
			if data.current.value == 'society_inventory' then
				OpenRoomInventoryMenu()
			end

			if data.current.value == 'player_inventory' then
				OpenPlayerInventoryMenu()
			end
			
			if data.current.value == 'withdraw_society_money' then

				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'withdraw_society_money_amount',
					{
						title = 'Montant du retrait'
					},
					function(data, menu)

						local amount = tonumber(data.value)

						if amount == nil then
							ESX.ShowNotification('Montant invalide')
						else
							menu.close()
							TriggerServerEvent('esx_society:withdrawMoney', societyName, amount)
						end

					end,
					function(data, menu)
						menu.close()
					end
				)
			end

			if data.current.value == 'deposit_money' then

				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'deposit_money_amount',
					{
						title = 'Montant du dépôt'
					},
					function(data, menu)

						local amount = tonumber(data.value)

						if amount == nil then
							ESX.ShowNotification('Montant invalide')
						else
							menu.close()
							TriggerServerEvent('esx_society:depositMoney', societyName, amount)
						end

					end,
					function(data, menu)
						menu.close()
					end
				)
			end

			if data.current.value == 'wash_money' then

				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'wash_money_amount',
					{
						title = 'Montant à blanchir'
					},
					function(data, menu)

						local amount = tonumber(data.value)

						if amount == nil then
							ESX.ShowNotification('Montant invalide')
						else
							menu.close()
							TriggerServerEvent('esx_society:washMoney', societyName, amount)
						end

					end,
					function(data, menu)
						menu.close()
					end
				)

			end

			if data.current.value == 'employee_recruit' then
				local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()
				if closestDistance == -1 or closestDistance > 3.0 then
					ESX.ShowNotification('Aucune personne n\'est près de vous !')
				else
					local recrue = GetPlayerServerId(closestPlayer)
					TriggerServerEvent('esx_eden_society:setjob', societyName, recrue)
					ESX.ShowNotification('Vous avez ~b~embauché~s~ ')
				end
			end

			if data.current.value == 'employee_management' then
				menu.close()
				TriggerEvent('esx_eden_society:OpenEmployeesManagement', societyName)
			end
			
		end,
		function(data, menu)
			menu.close()
		end
	)
end

RegisterNetEvent('esx_eden_society:OpenEmployeesManagement')
AddEventHandler('esx_eden_society:OpenEmployeesManagement', function(societyName)
	OpenEmployeesManagement(societyName)
end)

function OpenEmployeesManagement(societyName)
	menuIsShowed = true
	ESX.TriggerServerCallback('esx_eden_society:getEmployees', function(employees)
	
		local employeesList = {}

		for k,v in pairs(employees) do
			table.insert(employeesList, { label = v.name, value = v.id })
		end

		ESX.UI.Menu.Open(
			'default', GetCurrentResourceName(), 'employees_list',
			{
				title = 'Employés',
				align = 'top-left',
				elements = employeesList,
			},

			function(data, menu)		
				local employeeName = data.current.label
				local employeeId = data.current.value
				OpenEmployeeManagement(employeeName, employeeId, societyName)
			end,

			function(data, menu)
				menu.close()
			end
		)

	end, societyName)	
end

function OpenEmployeeManagement(employeeName, employeeId, societyName)

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'employee_management',
		{
			title    = employeeName,
			align    = 'top-left',
			elements = { {label = 'Changer de grade', value = 'employee_promote'}, {label = 'Renvoyer', value = 'employee_remove'} },
		},

		function(data, menu)
			if data.current.value == 'employee_promote' then
				OpenEmployeePromote(employeeName, employeeId, societyName)
			end
			
			if data.current.value == 'employee_remove' then
				TriggerServerEvent('esx_eden_society:setjob', 'unemployed', employeeId, 0)
				ESX.ShowNotification('Vous avez ~b~renvoyé~s~ ' .. employeeName)
			end
		end,
		
		function(data, menu)
			menu.close()
		end
	)

end

function OpenEmployeePromote(employeeName, employeeId, societyName)

	ESX.TriggerServerCallback('esx_eden_society:getGrades', function(grades)

		local gradesList = {}

		for k,v in pairs(grades) do
			table.insert(gradesList, { label = v.label, value = v.grade })
		end

		ESX.UI.Menu.Open(
			'default', GetCurrentResourceName(), 'employee_promote',
			{
				title    = 'Grade',
				align    = 'top-left',
				elements = gradesList,
			},

			function(data, menu)
				TriggerServerEvent('esx_eden_society:setjob', societyName, employeeId, data.current.value)
				ESX.ShowNotification(employeeName .. ' est maintenant : ~b~' .. data.current.label)
			end,
			
			function(data, menu)
				menu.close()
			end
		)

	end, societyName)

end

function OpenBuyVehicleMenu()

	menuIsShowed = true

	local price = societyConfig.Vehicles.Truck.Price

	ESX.TriggerServerCallback('esx_eden_society:getVehicle', function(vehicles)

		local elements = {}



		for k,v in pairs(vehicles) do
			
			local vehcile = v.name
			local count    = v.count

			table.insert(elements, {label = 'x' .. count .. ' ' .. vehcile .. ' $' .. price, value = vehcile, price = price })
		
		end
		
		ESX.UI.Menu.Open(
			'default', GetCurrentResourceName(), 'buy_vehicles_action',
			{
				title    = 'Achat véhicules',
				align    = 'top-left',
				elements = elements,
			},
			function(data, menu)

				ESX.TriggerServerCallback('esx_eden_society:buy', function(hasEnoughMoney)
					
					if hasEnoughMoney then
						ESX.TriggerServerCallback('esx_eden_society:addVehicle', function()
							OpenBuyVehicleMenu()
						end, data.current.value, societyName)
					else
						ESX.ShowNotification('not_enough_money')
					end
				end, data.current.price, societyName)

			end,
			function(data, menu)
				menu.close()
				OpenBossMenu()
			end
		)
	end,  societyName)
end

-- Show top left hint
Citizen.CreateThread(function()
	while true do
		Wait(0)
		if hintIsShowed == true then
			SetTextComponentFormat("STRING")
			AddTextComponentString(hintToDisplay)
			DisplayHelpTextFromStringLabel(0, 0, 1, -1)
		end
	end
end)

-- Display markers
Citizen.CreateThread(function()
	while true do
		Wait(0)		
		local zones = {}
		if PlayerData.job ~= nil then
			for k,v in pairs(Config.Society) do
				if PlayerData.job.name == k then
					zones = v.Zones
				end
			end

			local coords = GetEntityCoords(GetPlayerPed(-1))			

			for k,v in pairs(zones) do
				if(v.Type == "office" and PlayerData.job.grade_name == 'boss' and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
					DrawMarker(v.Marker, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
				end
				if (onDuty or v.Type == "cloakroom") and v.Type ~= "office" then
					if(v.Marker ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then	
						DrawMarker(v.Marker, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
					end
				end				
			end
		end
	end
end)

-- -- Activate menu when player is inside marker
Citizen.CreateThread(function()
	while true do		
		Wait(0)

		if PlayerData.job ~= nil and PlayerData.job.name ~= 'unemployed' then
			local zones = nil

			for k,v in pairs(Config.Society) do
				if PlayerData.job.name == k then
					societyName = k
					zones = v.Zones
					societyConfig = v
				end
			end

			local coords      = GetEntityCoords(GetPlayerPed(-1))
			local currentZone = nil
			local zone 		  = nil
			local lastZone    = nil

			for k,v in pairs(zones) do
				if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
					isInMarker  = true
					currentZone = k
					zone        = v
					break
				else
					isInMarker  = false
				end
			end

			if IsControlJustReleased(0, Keys["E"]) and not menuIsShowed and isInMarker then
				if onDuty or zone.Type == "cloakroom" then
					TriggerEvent('esx_eden_society:action', zone)
				end
				if(zone.Type == "office" and PlayerData.job.grade_name == 'boss') then
					TriggerEvent('esx_eden_society:action', zone)
				end
			end

			-- hide or show top left zone hints
			if isInMarker and not menuIsShowed then
				hintIsShowed = true
				if (onDuty or zone.Type == "cloakroom") then
					hintToDisplay = zone.Hint
					hintIsShowed = true
				elseif (zone.Type == "office" and PlayerData.job.grade_name == 'boss') then
					hintToDisplay = zone.Hint
					hintIsShowed = true
				else
					if not isInPublicMarker then
						hintToDisplay = "no hint to display"
						hintIsShowed = false
					end
				end
			end

			if isInMarker and not hasAlreadyEnteredMarker then
				hasAlreadyEnteredMarker = true
			end

			if not isInMarker and hasAlreadyEnteredMarker then
				hasAlreadyEnteredMarker = false
				TriggerEvent('esx_eden_society:hasExitedMarker', zone)
			end

		end
	end
end)

-- Set society icons on map
Citizen.CreateThread(function()

	for k, v in pairs(Config.SocietyBlips) do

		local blip = AddBlipForCoord(v.Pos.x, v.Pos.y, v.Pos.z)

		SetBlipSprite(blip, v.Sprite)
		SetBlipDisplay(blip, 3)
		SetBlipScale(blip, 1.2)
		SetBlipColour (blip, v.Color)
		SetBlipAsShortRange(blip, true)
		
		BeginTextCommandSetBlipName("STRING");
		AddTextComponentString(tostring(v.Name))
		EndTextCommandSetBlipName(blip)

		table.insert(JobBlips, blip)

	end

end)