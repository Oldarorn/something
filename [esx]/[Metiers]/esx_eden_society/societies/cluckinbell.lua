Config.Society.cluckinbell = {
	SocietyName = "cluckinbell",
	Ratio = 0.5,
	DrawDistance = 100.0,
	BlipInfos = {
		Sprite = 416,
		Color = 5
	},
	Vehicles = {
		Truck = {
			Spawner = 1,
			Hash = "phantom",
			Trailer = "trailers2",
			HasCaution = true,
			Price = 500
		}
	},
	Zones = {
		CloakRoom = {
			Pos   = {x = -102.7473449707, y = 6193.94921875, z = 30.021800994873},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Blip  = true,
			Name  = "Vestiaire",
			Type  = "cloakroom",
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour vous changer."
		},
		Office = {
			Pos   = { x = -68.450630187988, y = 6254.0864257813, z = 30.090166091919 },
			Size  = {x = 1.5, y = 1.5, z = 1.0},
			Color = {r = 194, g = 153, b = 255},
			Marker= 1,
			Blip  = false,
			Name  = "Bureau",
			Type  = "office",
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour gérer votre société."
		},
		AliveChicken = {
			Pos   = { x = 2553.5947265625, y = 4670.3442382813, z = 32.944484710693 },
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Blip  = true,
			Name  = "Poulailler",
			Type  = "work",
			Item  = {
				{
					name   = "Poulet vivant",
					db_name= "alive_chicken",
					time   = 3000,
					max    = 20,
					add    = 1,
					remove = 1,
					requires = "nothing",
					requires_name = "Nothing",
					drop   = 100
				}
			},
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour récupérer des poulets vivants."
		},

		SlaughterHouse = {
			Pos   = {x = -77.991, y = 6229.063, z = 30.091},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Blip  = false,
			Name  = "Abattoir",
			Type  = "work",
			Item  = {
				{
					name   = "Poulet à conditionner",
					db_name= "slaughtered_chicken",
					time   = 5000,
					max    = 20,
					add    = 1,
					remove = 1,
					requires = "alive_chicken",
					requires_name = "Poulet vivant",
					drop   = 100
				}
			},
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour dépecer les poulets."
		},

		Packaging = {
			Pos   = {x = -101.978, y = 6208.799, z = 30.025},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Blip  = false,
			Name  = "Emballage",
			Type  = "work",
			Item  = { 
				{
					name   = "Poulet en barquette",
					db_name= "packaged_chicken",
					time   = 4000,
					max    = 100,
					add    = 5,
					remove = 1,
					requires = "slaughtered_chicken",
					requires_name = "Poulet à conditionner",
					drop   = 100
				}
			},
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour conditionner le poulet en barquette."
		},

		VehicleSpawner = {
			Pos   = {z = 30.233713150024,y = 6277.591796875,x = 8.1227025985718},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Blip  = false,
			Name  = "Spawner véhicule de fonction",
			Type  = "vehspawner",
			Spawner = 1,
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour demander un véhicule.",
			Caution = 2000
		},

		VehicleSpawnPoint = {
			Pos   = { x = -4.4836220741272, y = 6266.4096679688, z = 31.408536911011 },
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Marker= -1,
			Blip  = false,
			Name  = "Véhicule de fonction",
			Type  = "vehspawnpt",
			Spawner = 1,
			Heading = 130.1
		},
		
		VehicleDeletePoint = {
			Pos   = {z = 30.259950637817,y = 6288.0361328125,x = 36.662757873535},
			Size  = {x = 5.0, y = 5.0, z = 1.0},
			Color = {r = 255, g = 0, b = 0},
			Marker= 1,
			Blip  = false,
			Name  = "Supression du véhicule",
			Type  = "vehdelete",
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour rendre le véhicule.",
			Spawner = 1,
			Caution = 2000,
			GPS = 0,
			Teleport = 0
		},

		Delivery = {
			Pos   = { x = 204.76818847656, y = -1466.6717529297, z = 28.146785736084 },
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 1.0},
			Marker= 1,
			Blip  = true,
			Name  = "Point de livraison",
			Type  = "delivery",				
			Spawner = 1,
			Item  = {
				{
					name   = "Livraison",
					time   = 500,
					remove = 1,
					max    = 100, -- if not present, probably an error at itemQtty >= item.max in esx_jobs_sv.lua
					price  = 25,
					requires = "packaged_chicken",
					requires_name = "Poulet en barquette",
					drop   = 100
				}
			},
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour livrer les barquettes de poulet."
		}
	}
}