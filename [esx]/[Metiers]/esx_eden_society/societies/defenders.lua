Config.Society.defenders = {
	SocietyName = "defenders",
	Ratio = 0.5,
	DrawDistance = 100.0,
	BlipInfos = {
		Sprite = 416,
		Color = 5
	},
	Vehicles = {
		Truck = {
			Spawner = 1,
			Hash = "mule3",
			Trailer = "none",
			HasCaution = true,
			Price = 500
		}
	},
	Zones = {
		CloakRoom = {
			Pos   = {x = -568.15576171875, y = 279.95169067383, z = 81.975860595703 },
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 124, g = 167, b = 172},
			Marker= 1,
			Blip  = true,
			Name  = "Vestiaire",
			Type  = "cloakroom",
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour vous changer."
		},
		Office = {
			Pos   = {x = -568.4814453125, y = 291.38418579102, z = 78.176612854004 },
			Size  = {x = 1.5, y = 1.5, z = 1.0},
			Color = {r = 124, g = 167, b = 172},
			Marker= 1,
			Blip  = false,
			Name  = "Bureau",
			Type  = "office",
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour gérer votre société."
		},
		Hop = {
			Pos   = {x = -98.732139587402, y = 1910.0477294922, z = 196.95967102051 },
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 124, g = 167, b = 172},
			Marker= 1,
			Blip  = true,
			Name  = "Ferme",
			Type  = "work",
			Item  = {
				{
					name   = "Malt",
					db_name= "malt",
					time   = 3000,
					max    = 20,
					add    = 1,
					remove = 1,
					requires = "nothing",
					requires_name = "Nothing",
					drop   = 100
				}
			},
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour récolter le houblon."
		},

		Distillery = {
			Pos   = {x = 839.89715576172, y = -1924.5191650391, z = 30.314670562744 },
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 124, g = 167, b = 172},
			Marker= 1,
			Blip  = true,
			Name  = "Distillerie",
			Type  = "work",
			Item  = {
				{
					name   = "Tonneau",
					db_name= "barrel",
					time   = 5000,
					max    = 20,
					add    = 1,
					remove = 1,
					requires = "malt",
					requires_name = "Malt",
					drop   = 100
				}
			},
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour mettre en tonneau."
		},

		BottlingProcess = {
			Pos   = {x = -262.95031738281, y = -2504.9677734375, z = 6.0006303787231 },
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 124, g = 167, b = 172},
			Marker= 1,
			Blip  = true,
			Name  = "Mise en bouteille",
			Type  = "work",
			Item  = { 
				{
					name   = "Bière sans alcool",
					db_name= "non_alcoholic_beer",
					time   = 4000,
					max    = 100,
					add    = 5,
					remove = 1,
					requires = "barrel",
					requires_name = "Tonneau",
					drop   = 100
				}
			},
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour mettre en bouteille."
		},

		VehicleSpawner = {
			Pos   = {x = -552.82708740234, y = 311.43823242188, z = 82.157493591309 },
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 124, g = 167, b = 172},
			Marker= 1,
			Blip  = false,
			Name  = "Spawner véhicule de fonction",
			Type  = "vehspawner",
			Spawner = 1,
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour demander le véhicule de livraison.",
			Caution = 2000
		},

		VehicleSpawnPoint = {
			Pos   = {x = -549.18249511719, y = 304.84582519531, z = 83.114372253418 },
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Marker= -1,
			Blip  = false,
			Name  = "Véhicule de fonction",
			Type  = "vehspawnpt",
			Spawner = 1,
			Heading = 130.1
		},
		
		VehicleDeletePoint = {
			Pos   = {x = -560.83978271484, y = 301.69717407227, z = 82.154739379883 },
			Size  = {x = 5.0, y = 5.0, z = 1.0},
			Color = {r = 255, g = 0, b = 0},
			Marker= 1,
			Blip  = false,
			Name  = "Supression du véhicule",
			Type  = "vehdelete",
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour rendre le véhicule.",
			Spawner = 1,
			Caution = 2000,
			GPS = 0,
			Teleport = 0
		},

		Delivery = {
			Pos   = {x = -2973.2756347656, y = 63.516620635986, z = 11.607481956482 },
			Color = {r = 124, g = 167, b = 172},
			Size  = {x = 5.0, y = 5.0, z = 1.0},
			Marker= 1,
			Blip  = true,
			Name  = "Point de livraison",
			Type  = "delivery",				
			Spawner = 1,
			Item  = {
				{
					name   = "Livraison",
					time   = 500,
					remove = 1,
					max    = 100, -- if not present, probably an error at itemQtty >= item.max in esx_jobs_sv.lua
					price  = 25,
					requires = "non_alcoholic_beer",
					requires_name = "Bière sans alcool",
					drop   = 100
				}
			},
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour livrer la bière sans alcool."
		}
	}
}