Config.Society.velvet = {
	SocietyName = "velvet",
	Ratio = 0.5,
	DrawDistance = 100.0,
	BlipInfos = {
		Sprite = 416,
		Color = 5
	},
	Vehicles = {
		Truck = {
			Spawner = 1,
			Hash = "pounder",
			Trailer = "none",
			HasCaution = true,
			Price = 500
		}
	},
	Zones = {
		CloakRoom = {
			Pos   = { x = 969.36010742188, y = -1226.6616210938, z = 26.065780639648 },	
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Blip  = true,
			Name  = "Vestiaire",
			Type  = "cloakroom",
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour vous changez."
		},
		Office = {
			Pos   = { x = -1555.2095947266, y = -574.85919189453, z = 107.53788757324 },
			Size  = {x = 1.5, y = 1.5, z = 1.0},
			Color = {r = 194, g = 153, b = 255},
			Marker= 1,
			Blip  = false,
			Name  = "Bureau",
			Type  = "office",
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour gérer votre société."
		},
		Hop = {
			Pos   = {x = 789.4083, y = -2524.977, z = 20.0888},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Blip  = true,
			Name  = "Usine",
			Type  = "work",
			Item  = {
				{
					name   = "Plantes",
					db_name= "plants",
					time   = 500,
					max    = 100,
					add    = 2,
					remove = 1,
					requires = "nothing",
					requires_name = "Nothing",
					drop   = 100
				}
			},
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour récuperer les plantes."
		},

		TripleBiffle = {
			Pos   = {x = 917.1035, y = -1261.7933, z = 24.5561},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Blip  = true,
			Name  = "Macération",
			Type  = "work",
			Item  = {
				{
					name   = "Liqueur de plante",
					db_name= "plantliquor",
					time   = 5000,
					max    = 20,
					add    = 1,
					remove = 5,
					requires = "plants",
					requires_name = "Plantes",
					drop   = 100
				}
			},
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour fabriquer la liqueur."
		},

		Packaging = {
			Pos   = {x = 843.87780761719, y = -1225.5416259766, z = 25.13592338562},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Blip  = true,
			Name  = "Mise en bouteille",
			Type  = "work",
			Item  = { 
				{
					name   = "HoleMeister",
					db_name= "holemeister",
					time   = 4000,
					max    = 100,
					add    = 5,
					remove = 1,
					requires = "plantliquor",
					requires_name = "Liqueur de plante",
					drop   = 100
				}
			},
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour la mise en bouteille."
		},

		VehicleSpawner = {
			Pos   = { x = 945.37255859375, y = -1211.6339111328, z = 24.801486968994 },
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 0},
			Marker= 1,
			Blip  = false,
			Name  = "Spawner véhicule de fonction",
			Type  = "vehspawner",
			Spawner = 1,
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour sortir le véhicule de livraison.",
			Caution = 2000
		},

		VehicleSpawnPoint = {
			Pos   = { x = 930.91436767578, y = -1221.2741699219, z = 24.651397705078 },
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Marker= -1,
			Blip  = false,
			Name  = "Véhicule de fonction",
			Type  = "vehspawnpt",
			Spawner = 1,
			Heading = 130.1
		},
		
		VehicleDeletePoint = {
			Pos   = { x = 932.1826171875, y = -1235.3262939453, z = 24.583976745605 },
			Size  = {x = 5.0, y = 5.0, z = 1.0},
			Color = {r = 255, g = 0, b = 0},
			Marker= 1,
			Blip  = false,
			Name  = "Supression du véhicule",
			Type  = "vehdelete",
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour ranger le véhicule.",
			Spawner = 1,
			Caution = 2000,
			GPS = 0,
			Teleport = 0
		},

		Delivery = {
			Pos   = { x = -1415.7598, y = -643.9201, z = 27.6733 },
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 1.0},
			Marker= 1,
			Blip  = true,
			Name  = "Point de livraison",
			Type  = "delivery",				
			Spawner = 1,
			Item  = {
				{
					name   = "Livraison",
					time   = 500,
					remove = 1,
					max    = 100, -- if not present, probably an error at itemQtty >= item.max in esx_jobs_sv.lua
					price  = 12.5,
					requires = "holemeister",
					requires_name = "HoleMeister",
					drop   = 100
				}
			},
			Hint  = "Appuyez sur ~INPUT_PICKUP~ pour vendre les bouteilles d'HoleMeister."
		}
	}
}