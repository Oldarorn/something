INSERT INTO addon_account (name, label, shared) VALUES ('society_centrale', 'Defenders', 1);
INSERT INTO addon_account_data (account_name, money, owner) VALUES ('society_centrale', 0, NULL);
INSERT INTO datastore (name, label, shared) VALUES ('society_centrale', 'Defenders', 1);
INSERT INTO datastore_data (name, owner, data) VALUES ('society_centrale', NULL, '{"vehicles":[{"name":"stockade","count":1}]}');

INSERT INTO items (name, label, `limit`, rare, can_remove) VALUES ('billet', 'Billet', -1, 0, 1);
INSERT INTO items (name, label, `limit`, rare, can_remove) VALUES ('billetMark', 'Billet marqué', -1, 0, 1);
INSERT INTO items (name, label, `limit`, rare, can_remove) VALUES ('Liassebillet', 'Liasse de billet', -1, 0, 1);