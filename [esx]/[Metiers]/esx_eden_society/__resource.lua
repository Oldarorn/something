resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

description 'ESX Cluckin Bell Job'

client_scripts {
	'config.lua',
	'societies/cluckinbell.lua',	
	'societies/velvet.lua',
	'societies/defenders.lua',
	'societies/centrale.lua',
	'client/main.lua'
}

server_scripts {
	'@mysql-async/lib/MySQL.lua',
	'config.lua',
	'server/main.lua'
}