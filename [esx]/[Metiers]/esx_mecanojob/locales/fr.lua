Locales['fr'] = {
	['remove_object'] 					 = 'Retirer du coffre entreprise',
	['deposit_object'] 					 = 'Deposer dans le coffre entreprise',
	['dirty_money'] 					 = 'Argent sale ',
	['societyInventory']				 = 'inventaire entreprise',
	['playerInventory']					 = 'inventaire personnel',
	['amount']							 = 'quantité ?',
	['amount_invalid']					 = '~r~montant invalide'
}