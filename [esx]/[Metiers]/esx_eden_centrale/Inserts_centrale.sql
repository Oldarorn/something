INSERT INTO addon_account (name, label, shared) VALUES ('society_gouv', 'Centrale', 1);
INSERT INTO addon_account_data (account_name, money, owner) VALUES ('society_gouv', 100000, NULL);
INSERT INTO datastore (name, label, shared) VALUES ('society_gouv', 'Centrale', 1);
INSERT INTO datastore_data (name, owner, data) VALUES ('society_gouv', NULL, '{"weapons":[{"name":"WEAPON_COMBATPISTOL","count":0},{"name":"WEAPON_SMG","count":0},{"name":"WEAPON_PUMPSHOTGUN","count":0},{"name":"WEAPON_ASSAULTSMG","count":1},{"name":"WEAPON_NIGHTSTICK","count":0},{"name":"WEAPON_STUNGUN","count":0},{"name":"WEAPON_PISTOL","count":0},{"name":"WEAPON_FLAREGUN","count":0},{"name":"WEAPON_FLASHLIGHT","count":0},{"name":"WEAPON_CARBINERIFLE","count":0},{"name":"WEAPON_PETROLCAN","count":0},{"name":"WEAPON_SMOKEGRENADE","count":0}]}');
INSERT INTO jobs (name, label, isPublic) VALUES ('centrale', 'Centrale', 0);



INSERT INTO job_grades (job_name, grade, name, label, salary, skin_male, skin_female) VALUES ('centrale', 0, 'interim', 'Chauffeur', 0, '{"tshirt_1":15,"torso_1":9,"torso_2":1,"arms":30,"pants_1":8,"shoes_1":12,"helmet_1":-1}', '{"tshirt_1":15,"torso_1":9,"torso_2":1,"arms":30,"pants_1":8,"shoes_1":12,"helmet_1":-1}');
INSERT INTO job_grades (job_name, grade, name, label, salary, skin_male, skin_female) VALUES ('centrale', 1, 'garde', 'Garde du corps', 0, '{"tshirt_1":15,"torso_1":9,"torso_2":1,"arms":30,"pants_1":10,"shoes_1":12,"helmet_1":-1}', '{"tshirt_1":15,"torso_1":9,"torso_2":1,"arms":30,"pants_1":10,"shoes_1":12,"helmet_1":-1}');
INSERT INTO job_grades (job_name, grade, name, label, salary, skin_male, skin_female) VALUES ('centrale', 2, 'Conseiller', 'Conseiller', 0, '{"tshirt_1":15,"torso_1":95,"arms":26,"pants_1":23,"shoes_1":12,"shoes_2":3,"helmet_1":-1}', '{"tshirt_1":15,"torso_1":95,"arms":26,"pants_1":23,"shoes_1":12,"shoes_2":3,"helmet_1":-1}');
INSERT INTO job_grades (job_name, grade, name, label, salary, skin_male, skin_female) VALUES ('centrale', 3, 'boss', 'Gouverneur', 0, '{"tshirt_1":21,"tshirt_2":4,"torso_1":28,"arms":27,"pants_1":35,"shoes_1":21,"shoes_2":9,"helmet_1":-1}', '{"tshirt_1":21,"tshirt_2":4,"torso_1":28,"arms":27,"pants_1":35,"shoes_1":21,"shoes_2":9,"helmet_1":-1}');





