local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

ESX                           = nil
local GUI                     = {}
GUI.Time                      = 0
local PlayerData              = {}
local FirstSpawn              = true
local IsDead                  = false
local HasAlreadyEnteredMarker = false
local LastZone                = nil
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local RespawnToHospitalMenu   = nil
local spawnAfterDisconnect    = false

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

function RespawnPed(ped, coords)	
	TriggerServerEvent("esx_ambulancejob:setAlive",true)
	SetEntityCoordsNoOffset(ped, coords.x, coords.y, coords.z, false, false, false, true)
	NetworkResurrectLocalPlayer(coords.x, coords.y, coords.z, coords.heading, true, false)
	SetPlayerInvincible(ped, false)
	TriggerEvent('playerSpawned', coords.x, coords.y, coords.z, coords.heading)
	ClearPedBloodDamage(ped)
	if RespawnToHospitalMenu ~= nil then
		RespawnToHospitalMenu.close()
		RespawnToHospitalMenu = nil
	end
	ESX.UI.Menu.CloseAll()
end

function StartRespawnToHospitalMenuTimer()

	ESX.SetTimeout(Config.MenuRespawnToHospitalDelay, function()

		if IsDead then

			local elements = {}

			table.insert(elements, {label = _U('yes'), value = 'yes'})

			RespawnToHospitalMenu = ESX.UI.Menu.Open(
				'default', GetCurrentResourceName(), 'menuName',
				{
					title = _U('respawn_at_hospital'),
					align = 'top-left',
					elements = elements
				},
		        function(data, menu) --Submit Cb

		        	menu.close()

		         	Citizen.CreateThread(function()

						DoScreenFadeOut(800)

						while not IsScreenFadedOut() do
							Citizen.Wait(0)
						end

						ESX.TriggerServerCallback('esx_ambulancejob:removeItemsAfterRPDeath', function()

							ESX.SetPlayerData('lastPosition', Config.Zones.HospitalInteriorInside1.Pos)
							TriggerServerEvent('esx:updateLastPosition', Config.Zones.HospitalInteriorInside1.Pos)

							RespawnPed(GetPlayerPed(-1), Config.Zones.HospitalInteriorInside1.Pos)
							StopScreenEffect('DeathFailOut')
							DoScreenFadeIn(800)
						end)
					end)
		        end,
		        function(data, menu) --Cancel Cb
		                --menu.close()
		        end,
		        function(data, menu) --Change Cb
		                --print(data.current.value)
		        end,
		        function(data, menu) --Close Cb
		            RespawnToHospitalMenu = nil
		        end
			)
		end
	end)
end

function StartRespawnTimer()

	ESX.SetTimeout(Config.RespawnDelayAfterRPDeath, function()

		if IsDead then

			Citizen.CreateThread(function()

				DoScreenFadeOut(800)

				while not IsScreenFadedOut() do
					Citizen.Wait(0)
				end
				
				ESX.TriggerServerCallback('esx_ambulancejob:removeItemsAfterRPDeath', function()

					ESX.SetPlayerData('lastPosition', Config.Zones.HospitalInteriorInside1.Pos)
					TriggerServerEvent('esx:updateLastPosition', Config.Zones.HospitalInteriorInside1.Pos)

					RespawnPed(GetPlayerPed(-1), Config.Zones.HospitalInteriorInside1.Pos)
					StopScreenEffect('DeathFailOut')
					DoScreenFadeIn(800)
				end)
			end)
		end
	end)
end

function RespawnTimer()

	local timer = Config.RespawnDelayAfterRPDeath
	local allowRespawn = Config.RespawnDelayAfterRPDeath/2
	local enoughMoney = false
	local money = 0	
	if IsDead and Config.ShowDeathTimer then
		ESX.TriggerServerCallback('esx_ambulancejob:getBankMoney', function(money)
			if money >= Config.EarlyRespawnFineAmount then
				enoughMoney = true
			else
				enoughMoney = false
			end
		end)
		Citizen.CreateThread(function()


			while timer > 0 and IsDead do
				raw_seconds = timer/1000
				raw_minutes = raw_seconds/60
				minutes = stringsplit(raw_minutes, ".")[1]
				seconds = stringsplit(raw_seconds-(minutes*60), ".")[1]

				if Config.EarlyRespawn and Config.EarlyRespawnFine and enoughMoney then
					SetTextFont(4)
					SetTextProportional(0)
					SetTextScale(0.0, 0.5)
					SetTextColour(255, 255, 255, 255)
					SetTextDropshadow(0, 0, 0, 0, 255)
					SetTextEdge(1, 0, 0, 0, 255)
					SetTextDropShadow()
					SetTextOutline()
					SetTextEntry("STRING")
					AddTextComponentString(_U('please_wait') .. minutes .. _U('minutes') .. seconds .. _U('seconds_fine') .. Config.EarlyRespawnFineAmount .. _U('press_respawn_fine'))
					SetTextCentre(true)
					DrawText(0.5, 0.8)
					timer = timer - 15
					Citizen.Wait(0)
					if IsControlPressed(0,  Keys['E']) then
						DoScreenFadeOut(800)

						while not IsScreenFadedOut() do
							Citizen.Wait(0)
						end
						
						ESX.TriggerServerCallback('esx_ambulancejob:removeItemsAfterRPDeathRemoveMoney', function()

							TriggerServerEvent('esx_ambulancejob:removeAccountMoney', source)
							ESX.SetPlayerData('lastPosition', Config.Zones.HospitalInteriorInside1.Pos)
							TriggerServerEvent('esx:updateLastPosition', Config.Zones.HospitalInteriorInside1.Pos)

							RespawnPed(GetPlayerPed(-1), Config.Zones.HospitalInteriorInside1.Pos)
							StopScreenEffect('DeathFailOut')
							DoScreenFadeIn(800)
						end)
					end
				else
					SetTextFont(4)
					SetTextProportional(0)
					SetTextScale(0.0, 0.5)
					SetTextColour(255, 255, 255, 255)
					SetTextDropshadow(0, 0, 0, 0, 255)
					SetTextEdge(1, 0, 0, 0, 255)
					SetTextDropShadow()
					SetTextOutline()
					SetTextEntry("STRING")
					AddTextComponentString(_U('please_wait') .. minutes .. _U('minutes') .. seconds .. _U('seconds'))
					SetTextCentre(true)
					DrawText(0.5, 0.8)
					timer = timer - 15
					Citizen.Wait(0)
				end
				if IsControlPressed(0,  Keys['F2']) then
					ESX.UI.Menu.Close('default','es_extended','inventory')
				end
			end

			while timer <= 0 and IsDead do
				--TriggerServerEvent("eden_garage:debug",timer)
				SetTextFont(4)
				SetTextProportional(0)
				SetTextScale(0.0, 0.5)
				SetTextColour(255, 255, 255, 255)
				SetTextDropshadow(0, 0, 0, 0, 255)
				SetTextEdge(1, 0, 0, 0, 255)
				SetTextDropShadow()
				SetTextOutline()
				SetTextEntry("STRING")
				AddTextComponentString(_U('press_respawn'))
				SetTextCentre(true)
				DrawText(0.5, 0.8)
				Citizen.Wait(0)
				if IsControlPressed(0,  Keys['E']) then
					DoScreenFadeOut(800)

					while not IsScreenFadedOut() do
						Citizen.Wait(0)
					end
					
					ESX.TriggerServerCallback('esx_ambulancejob:removeItemsAfterRPDeath', function()
						TriggerServerEvent('eden_ambulance:removeweaponquimarche')


						ESX.SetPlayerData('lastPosition', Config.Zones.HospitalInteriorInside1.Pos)
						TriggerServerEvent('esx:updateLastPosition', Config.Zones.HospitalInteriorInside1.Pos)

						RespawnPed(GetPlayerPed(-1), Config.Zones.HospitalInteriorInside1.Pos)
						StopScreenEffect('DeathFailOut')
						DoScreenFadeIn(800)
					end)
				end
				if IsControlPressed(0,  Keys['F2']) then
					ESX.UI.Menu.Close('default','es_extended','inventory')
				end
			end
		end)
	end
end

function TeleportFadeEffect(entity, coords)

	Citizen.CreateThread(function()

		DoScreenFadeOut(800)

		while not IsScreenFadedOut() do
			Citizen.Wait(0)
		end

		ESX.Game.Teleport(entity, coords, function()
			DoScreenFadeIn(800)
		end)

	end)

end

function WarpPedInClosestVehicle(ped)

	local coords = GetEntityCoords(ped)

  local vehicle, distance = ESX.Game.GetClosestVehicle({
  	x = coords.x,
  	y = coords.y,
  	z = coords.z
  })

  if distance ~= -1 and distance <= 5.0 then

  	local maxSeats = GetVehicleMaxNumberOfPassengers(vehicle)
  	local freeSeat = nil

  	for i=maxSeats - 1, 0, -1 do
  		if IsVehicleSeatFree(vehicle,  i) then
  			freeSeat = i
  			break
  		end
  	end

  	if freeSeat ~= nil then
  		TaskWarpPedIntoVehicle(ped,  vehicle,  freeSeat)
  	end

  else
  	ESX.ShowNotification(_U('no_vehicles'))
  end

end

function OpenAmbulanceActionsMenu()

	local elements = {
		{label = _U('cloakroom'), value = 'cloakroom'}
	}
	
	if PlayerData.job.grade_name ~= 'ambulance' then
		table.insert(elements, {label = _U('remove_object'), value = 'society_inventory'})
		table.insert(elements, {label = _U('deposit_object'), value = 'player_inventory'})
	end

	if Config.EnablePlayerManagement and PlayerData.job.grade_name == 'boss' then
		table.insert(elements, {label = _U('withdraw_society'), value = 'withdraw_society_money'})
		table.insert(elements, {label = _U('deposit_society'), value = 'deposit_society_money'})
		table.insert(elements, {label = _U('employee_recruit'), value = 'employee_recruit'})
		table.insert(elements, {label = _U('employee_management'), value = 'employee_management'})
	end

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'ambulance_actions',
		{
			title    = _U('ambulance'),
			align = 'top-left',
			elements = elements
		},
		function(data, menu)

			if data.current.value == 'cloakroom' then
				OpenCloakroomMenu()
			end
			
			if data.current.value == 'society_inventory' then
				OpenRoomInventoryMenu()
			end

			if data.current.value == 'player_inventory' then
				OpenPlayerInventoryMenu()
			end

			if data.current.value == 'withdraw_society_money' then

				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'withdraw_society_money_amount',
					{
						title = _U('money_withdraw')
					},
					function(data, menu)

						local amount = tonumber(data.value)

						if amount == nil then
							ESX.ShowNotification(_U('invalid_amount'))
						else
							menu.close()
							TriggerServerEvent('esx_society:withdrawMoney', 'ambulance', amount)
						end

					end,
					function(data, menu)
						menu.close()
					end
				)

			end
			
			if data.current.value == 'deposit_society_money' then

				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'deposit_money_amount',
					{
						title = _U('deposit_amount')
					},
					function(data, menu)

						local amount = tonumber(data.value)

						if amount == nil then
							ESX.ShowNotification(_U('invalid_amount'))
						else
							menu.close()
							TriggerServerEvent('esx_society:depositMoney', 'ambulance', amount)
						end

					end,
					function(data, menu)
						menu.close()
					end
				)

			end

			local societyName = 'ambulance'

			if data.current.value == 'employee_recruit' then
				local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()
				if closestDistance == -1 or closestDistance > 3.0 then
					ESX.ShowNotification('Aucune personne n\'est près de vous !')
				else
					local recrue = GetPlayerServerId(closestPlayer)
					TriggerServerEvent('esx_eden_society:setjob', societyName, recrue)
					ESX.ShowNotification('Vous avez ~b~embauché~s~ ')
				end
			end

			if data.current.value == 'employee_management' then
				menu.close()
				TriggerEvent('esx_eden_society:OpenEmployeesManagement', societyName)
			end

		end,
		function(data, menu)

			menu.close()

			CurrentAction     = 'ambulance_actions_menu'
			CurrentActionMsg  = _U('open_menu')
			CurrentActionData = {}

		end
	)

end

function OpenMobileAmbulanceActionsMenu()

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'mobile_ambulance_actions',
		{
			title    = _U('ambulance'),
			elements = {
				{label = _U('ems_menu'), value = 'citizen_interaction'},
				{label = _U('object_spawner'),  value = 'object_spawner'},
			}
		},
		function(data, menu)

			if data.current.value == 'citizen_interaction' then

				ESX.UI.Menu.Open(
					'default', GetCurrentResourceName(), 'citizen_interaction',
					{
						title    = _U('ems_menu_title'),
						elements = {
					  	{label = _U('ems_menu_revive'),             value = 'revive'},
					  	{label = _U('ems_menu_putincar'), value = 'put_in_vehicle'},
					  	
						}
					},
					function(data, menu)

						if data.current.value == 'revive' then

							menu.close()

							local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

							if closestPlayer == -1 or closestDistance > 3.0 then
								ESX.ShowNotification(_U('no_players'))
							else

								local ped    = GetPlayerPed(closestPlayer)
								local health = GetEntityHealth(ped)

								if health == 0 then

								local playerPed        = GetPlayerPed(-1)
								local closestPlayerPed = GetPlayerPed(closestPlayer)

								Citizen.CreateThread(function()

									ESX.ShowNotification(_U('revive_inprogress'))

									TaskStartScenarioInPlace(playerPed, 'CODE_HUMAN_MEDIC_TEND_TO_DEAD', 0, true)
									Citizen.Wait(10000)
									ClearPedTasks(playerPed)

									if GetEntityHealth(closestPlayerPed) == 0 then
										TriggerServerEvent('esx_ambulancejob:revive', GetPlayerServerId(closestPlayer))
										ESX.ShowNotification(_U('revive_complete') .. GetPlayerName(closestPlayer))
									else
										ESX.ShowNotification(GetPlayerName(closestPlayer) .. _U('isdead'))
									end

								end)

								else
									ESX.ShowNotification(GetPlayerName(closestPlayer) .. _U('unconscious'))
								end

							end

						end

						if data.current.value == 'put_in_vehicle' then
							menu.close()
							WarpPedInClosestVehicle(GetPlayerPed(closestPlayer))
						end
					end,
					function(data, menu)
						menu.close()
					end
				)

				elseif data.current.value == 'object_spawner' then
					

					ESX.UI.Menu.Open(
						'default', GetCurrentResourceName(), 'citizen_interaction',
						{
						title    = _U('traffic_interaction'),
						align    = 'top-left',
						elements = {
						{label = _U('cone'),     value = 'prop_roadcone02a'},
						{label = _U('barrier'), value = 'prop_barrier_work06a'},
						{label = _U('healthpack1'),    value = 'prop_stat_pack_01'},
						{label = _U('healthpack2'),   value = 'prop_ld_health_pack'},
						},
						},
						function(data2, menu2)


							local model     = data2.current.value
							local playerPed = GetPlayerPed(-1)
							local coords    = GetEntityCoords(playerPed)
							local forward   = GetEntityForwardVector(playerPed)
							local x, y, z   = table.unpack(coords + forward * 1.0)

							if model == 'prop_roadcone02a' then
								z = z - 2.0
							end

							ESX.Game.SpawnObject(model, {
								x = x,
								y = y,
								z = z
								}, function(obj)
								SetEntityHeading(obj, GetEntityHeading(playerPed))
								PlaceObjectOnGroundProperly(obj)
								end)

							end,
							function(data2, menu2)
								menu2.close()
							end
							)

					
				end

		end,
		function(data, menu)
			menu.close()
		end
	)

end

function OpenCloakroomMenu()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'cloakroom',
		{
			title    = _U('cloakroom'),
			align    = 'top-left',
			elements = {
				{label = _U('ems_clothes_civil'), value = 'citizen_wear'},
				{label = _U('ems_clothes_ems'), value = 'ambulance_wear'},
			},
		},
		function(data, menu)

			menu.close()

			if data.current.value == 'citizen_wear' then

				if Config.MaxInService ~= -1 then
					TriggerServerEvent('esx_service:disableService', 'ambulance')
				end

				ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
					TriggerEvent('skinchanger:loadSkin', skin)
				end)

			end

			if data.current.value == 'ambulance_wear' then

				ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)					

					ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)

						if canTakeService then

							if skin.sex == 0 then
								TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_male)
							else
								TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_female)
							end

						else

							ESX.ShowNotification(_U('full_service') .. inServiceCount .. '/' .. maxInService)

						end

					end, 'ambulance')

				end)

			end

			CurrentAction     = 'ambulance_actions_menu'
			CurrentActionMsg  = _U('open_menu')
			CurrentActionData = {}

		end,
		function(data, menu)
			menu.close()
		end
	)

end

function OpenRoomInventoryMenu()

  local societyName = 'society_ambulance'
  local societyJobName = 'ambulance'
  local societyBlackMoneyName = 'society_ambulance_black_money'
  
  local blackMoney = 0
  local items = {}
  local weapons = {}
  local elements = {}
  
  ESX.TriggerServerCallback('esx_society:getAccountMoney', function(money)
  
    blackMoney = money
	
	table.insert(elements, {label = _U('dirty_money') .. blackMoney .. '$', type = 'item_account', value = 'black_money'})
  end, societyBlackMoneyName)
	
  ESX.TriggerServerCallback('esx_society:getAccountItems', function(items)

    print(json.encode(items))

    for i=1, #items, 1 do
	  if items[i].count > 0 then
        table.insert(elements, {label = items[i].label .. ' x' .. items[i].count, type = 'item_standard', value = items[i].name})
      end
	end
	
	ESX.TriggerServerCallback('esx_jobs:getSocietyWeapons', function(weapons)

	for i=1, #weapons, 1 do
		if weapons[i].count > 0 then
			table.insert(elements, {label = ESX.GetWeaponLabel(weapons[i].name) .. ' x' .. weapons[i].count, type = 'item_weapon', value = weapons[i].name})
		end
	end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocksItems_menu',
      {
        title    = _U('societyInventory'),
		align    = 'top-left',
        elements = elements
      },
      function(data, menu)
	  
		if data.current.type == 'item_weapon' then
		
			menu.close()

			ESX.TriggerServerCallback('esx_jobs:getWeapon', function()
				OpenRoomInventoryMenu()
			end, data.current.value, societyJobName)
			
			ESX.SetTimeout(300, function()
				OpenRoomInventoryMenu()
			end)
		
		else

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = _U('amount')
          },
          function(data2, menu)

            local count = tonumber(data2.value)
			
			menu.close()

            if count == nil then
              ESX.ShowNotification(_U('amount_invalid'))
            else
              TriggerServerEvent('esx_jobs:getItem', data.current.type, data.current.value, count, societyJobName)
			  
              OpenRoomInventoryMenu()
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )
		
		end

      end,
      function(data, menu)
        menu.close()
      end
	 
    )
	
	end, societyJobName)

  end, societyName)

end

function OpenPlayerInventoryMenu()

  local societyJobName = 'ambulance'

  ESX.TriggerServerCallback('esx_jobs:getPlayerInventory', function(inventory)

    local elements = {}

    table.insert(elements, {label = _U('dirty_money') .. inventory.blackMoney .. '$', type = 'item_account', value = 'black_money'})

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end

    end

    local playerPed  = GetPlayerPed(-1)
    local weaponList = ESX.GetWeaponList()

    for i=1, #weaponList, 1 do

      local weaponHash = GetHashKey(weaponList[i].name)

      if HasPedGotWeapon(playerPed,  weaponHash,  false) and weaponList[i].name ~= 'WEAPON_UNARMED' then
        local ammo = GetAmmoInPedWeapon(playerPed, weaponHash)
        table.insert(elements, {label = weaponList[i].label, type = 'item_weapon', value = weaponList[i].name, ammo = ammo})
      end

    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'player_inventory',
      {
        title    = _U('playerInventory'),
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)

        if data.current.type == 'item_weapon' then

          menu.close()

          ESX.TriggerServerCallback('esx_jobs:putWeapon', function()
			  OpenPlayerInventoryMenu()
		  end, data.current.value, societyJobName)

          ESX.SetTimeout(300, function()
            OpenPlayerInventoryMenu()
          end)

        else

          ESX.UI.Menu.Open(
            'dialog', GetCurrentResourceName(), 'put_item_count',
            {
              title = _U('amount'),
            },
            function(data2, menu)

              menu.close()

              TriggerServerEvent('esx_jobs:putItem', data.current.type, data.current.value, tonumber(data2.value), societyJobName)

              ESX.SetTimeout(300, function()
                OpenPlayerInventoryMenu()
              end)

            end,
            function(data2,menu)
              menu.close()
            end
          )

        end

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenVehicleSpawnerMenu()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'cloakroom',
		{
			title    = _U('veh_menu'),
			align    = 'top-left',
			elements = {
				{label = _U('ambulance'),   value = 'ambulance'},
				{label = _U('helicopter'), value = 'polmav'},
			},
		},
		function(data, menu)

			menu.close()

			local model = data.current.value

			ESX.Game.SpawnVehicle(model, Config.Zones.VehicleSpawnPoint.Pos, 270.0, function(vehicle)

				local playerPed = GetPlayerPed(-1)

				if model == 'polmav' then
					SetVehicleModKit(vehicle, 0)
					SetVehicleLivery(vehicle, 1)
				end

				TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)

			end)

		end,
		function(data, menu)

			menu.close()

			CurrentAction     = 'vehicle_spawner_menu'
			CurrentActionMsg  = _U('veh_spawn')
			CurrentActionData = {}

		end
	)

end

AddEventHandler('playerSpawned', function()

	IsDead = false

	if FirstSpawn then
		exports.spawnmanager:setAutoSpawn(false)
		FirstSpawn = false
	end

end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)

	PlayerData = xPlayer

	ESX.TriggerServerCallback('esx_ambulancejob:getAlive', function(alive)				
		if(not alive) then
			Citizen.CreateThread(function()
			    local time = Config.TimeBeforeDeath
			    while (time ~= 0) do -- Whist we have time to wait
			        Wait( 1000 ) -- Wait a second
			        if(time == Config.TimeBeforeDeath ) then
			        	ESX.ShowNotification("Vous allez mourir dans "..time.." secondes")
			        end			        
			        time = time - 1
			        -- 1 Second should have past by now
			    end			    
			    TriggerEvent("es_admin:kill") -- Call the client event "kill"
			end)
		end
	end)

	if PlayerData.job.name == 'ambulance' then

		Config.Zones.AmbulanceActions.Type = 1
		Config.Zones.VehicleSpawner.Type   = 1
		Config.Zones.VehicleDeleter.Type   = 1

	else

		Config.Zones.AmbulanceActions.Type = -1
		Config.Zones.VehicleSpawner.Type   = -1
		Config.Zones.VehicleDeleter.Type   = -1

	end

end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)

	PlayerData.job = job

	if PlayerData.job.name == 'ambulance' then

		Config.Zones.AmbulanceActions.Type = 1
		Config.Zones.VehicleSpawner.Type   = 1
		Config.Zones.VehicleDeleter.Type   = 1

	else

		Config.Zones.AmbulanceActions.Type = -1
		Config.Zones.VehicleSpawner.Type   = -1
		Config.Zones.VehicleDeleter.Type   = -1

	end

end)


AddEventHandler('baseevents:onPlayerDied', function(killerType, coords)	
	--TriggerEvent('esx_ambulancejob:onPlayerDeath')
	OnPlayerDeath()
end)

AddEventHandler('baseevents:onPlayerKilled', function(killerId, data)	
	--TriggerEvent('esx_ambulancejob:onPlayerDeath')
	OnPlayerDeath()
end)

-- AddEventHandler('esx_ambulancejob:onPlayerDeath', function()
-- 	TriggerServerEvent("eden_garage:debug","onPlayerDeath")
-- 	IsDead = true
-- 	TriggerServerEvent("esx_ambulancejob:setAlive",false)
-- 	if Config.ShowDeathTimer == true then
-- 		RespawnTimer()
-- 	else
-- 		StartRespawnTimer()
-- 		StartRespawnToHospitalMenuTimer()
-- 	end

-- 	StartScreenEffect('DeathFailOut',  0,  false)

-- end)

function OnPlayerDeath()	
	IsDead = true
	TriggerServerEvent("esx_ambulancejob:setAlive",false)
	if Config.ShowDeathTimer == true then		
		RespawnTimer()
	else		
		StartRespawnTimer()
		StartRespawnToHospitalMenuTimer()
	end

	StartScreenEffect('DeathFailOut',  0,  false)
end

RegisterNetEvent('esx_ambulancejob:revive')
AddEventHandler('esx_ambulancejob:revive', function()

	local playerPed = GetPlayerPed(-1)
	local coords    = GetEntityCoords(playerPed)

	Citizen.CreateThread(function()

		DoScreenFadeOut(800)

		while not IsScreenFadedOut() do
			Citizen.Wait(0)
		end

		ESX.SetPlayerData('lastPosition', {
			x = coords.x,
			y = coords.y,
			z = coords.z
		})

		TriggerServerEvent('esx:updateLastPosition', {
			x = coords.x,
			y = coords.y,
			z = coords.z
		})

		RespawnPed(playerPed, {
			x = coords.x,
			y = coords.y,
			z = coords.z
		})

		StopScreenEffect('DeathFailOut')

		DoScreenFadeIn(800)

	end)

end)

AddEventHandler('esx_ambulancejob:hasEnteredMarker', function(zone)

	if zone == 'HospitalInteriorEntering1' then
		TeleportFadeEffect(GetPlayerPed(-1), Config.Zones.HospitalInteriorInside1.Pos)
	end

	if zone == 'HospitalInteriorExit1' then
		TeleportFadeEffect(GetPlayerPed(-1), Config.Zones.HospitalInteriorOutside1.Pos)
	end

	if zone == 'HospitalInteriorEntering2' then
		TeleportFadeEffect(GetPlayerPed(-1), Config.Zones.HospitalInteriorInside2.Pos)
	end

	if zone == 'HospitalInteriorExit2' then
		TeleportFadeEffect(GetPlayerPed(-1), Config.Zones.HospitalInteriorOutside2.Pos)
	end

	if zone == 'AmbulanceActions' and PlayerData.job ~= nil and PlayerData.job.name == 'ambulance' then
		CurrentAction     = 'ambulance_actions_menu'
		CurrentActionMsg  = _U('open_menu')
		CurrentActionData = {}
	end

	if zone == 'VehicleSpawner' and PlayerData.job ~= nil and PlayerData.job.name == 'ambulance' then
		CurrentAction     = 'vehicle_spawner_menu'
		CurrentActionMsg  = _U('veh_spawn')
		CurrentActionData = {}
	end

	if zone == 'VehicleDeleter' and PlayerData.job ~= nil and PlayerData.job.name == 'ambulance' then

		local playerPed = GetPlayerPed(-1)
		local coords    = GetEntityCoords(playerPed)

		if IsPedInAnyVehicle(playerPed,  false) then

			local vehicle, distance = ESX.Game.GetClosestVehicle({
				x = coords.x,
				y = coords.y,
				z = coords.z
			})
			
			--if distance ~= -1 and distance <= 3.0 then
			if distance <= 3.0 then

				CurrentAction     = 'delete_vehicle'
				CurrentActionMsg  = _U('store_veh')
				CurrentActionData = {vehicle = vehicle}

			end

		end

		end

end)

AddEventHandler('esx_ambulancejob:hasExitedMarker', function(zone)
	ESX.UI.Menu.CloseAll()
	CurrentAction = nil
end)

-- Create blips
Citizen.CreateThread(function()

	local blip = AddBlipForCoord(Config.Zones.HospitalInteriorOutside1.Pos.x, Config.Zones.HospitalInteriorOutside1.Pos.y, Config.Zones.HospitalInteriorOutside1.Pos.z)

  SetBlipSprite (blip, 61)
  SetBlipDisplay(blip, 4)
  SetBlipScale  (blip, 1.2)
  SetBlipAsShortRange(blip, true)

	BeginTextCommandSetBlipName("STRING")
  AddTextComponentString(_U('hospital'))
  EndTextCommandSetBlipName(blip)

end)

-- Display markers
Citizen.CreateThread(function()
	while true do

		Wait(0)

		local coords = GetEntityCoords(GetPlayerPed(-1))

		for k,v in pairs(Config.Zones) do

			if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
				DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
			end
		end

	end
end)

-- Activate menu when player is inside marker
Citizen.CreateThread(function()
	while true do

		Wait(0)

		local coords      = GetEntityCoords(GetPlayerPed(-1))
		local isInMarker  = false
		local currentZone = nil

		for k,v in pairs(Config.Zones) do
			if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
				isInMarker  = true
				currentZone = k
			end
		end

		if isInMarker and not hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = true
			lastZone                = currentZone
			TriggerEvent('esx_ambulancejob:hasEnteredMarker', currentZone)
		end

		if not isInMarker and hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = false
			TriggerEvent('esx_ambulancejob:hasExitedMarker', lastZone)
		end

	end
end)

-- Key Controls
Citizen.CreateThread(function()
	while true do

		Citizen.Wait(0)

		if CurrentAction ~= nil then

			SetTextComponentFormat('STRING')
			AddTextComponentString(CurrentActionMsg)
			DisplayHelpTextFromStringLabel(0, 0, 1, -1)

			if IsControlPressed(0,  Keys['E']) and PlayerData.job ~= nil and PlayerData.job.name == 'ambulance' and (GetGameTimer() - GUI.Time) > 150 then

				if CurrentAction == 'ambulance_actions_menu' then
					OpenAmbulanceActionsMenu()
				end

				if CurrentAction == 'vehicle_spawner_menu' then
					OpenVehicleSpawnerMenu()
				end

				if CurrentAction == 'delete_vehicle' then
					ESX.Game.DeleteVehicle(CurrentActionData.vehicle)
				end

				CurrentAction = nil
				GUI.Time      = GetGameTimer()

			end

		end

		if IsControlPressed(0,  Keys['F6']) and PlayerData.job ~= nil and PlayerData.job.name == 'ambulance' and (GetGameTimer() - GUI.Time) > 150 then
			OpenMobileAmbulanceActionsMenu()
			GUI.Time = GetGameTimer()
		end

	end
end)

-- Citizen.CreateThread(function()
-- 	while true do

-- 		Citizen.Wait(0)

-- 		if (IsDead and FirstSpawn) then
-- 			TriggerEvent("es_admin:kill")
-- 		end
-- 	end
-- end)

-- Load unloaded IPLs
Citizen.CreateThread(function()
  LoadMpDlcMaps()
  EnableMpDlcMaps(true)
  RequestIpl('Coroner_Int_on') -- Morgue
end)

-- String string
function stringsplit(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={} ; i=1
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end
