ESX                = nil
PlayersHarvesting  = {}
PlayersHarvesting2 = {}
PlayersHarvesting3 = {}
PlayersCrafting    = {}
PlayersCrafting2   = {}
PlayersCrafting3   = {}
PlayersCrafting4   = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

if Config.MaxInService ~= -1 then
	TriggerEvent('esx_service:activateService', 'alchimiste', Config.MaxInService)
end

-------------- Récupération bouteille de gaz -------------
---- Sqlut je teste ------
local function Harvest(source)

	SetTimeout(4000, function()

		if PlayersHarvesting[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local Orties = xPlayer.getInventoryItem('orties').count

			if Orties >= 5 then
				TriggerClientEvent('esx:showNotification', source, '~r~Vous n\'avez plus de place')
			else
                xPlayer.addInventoryItem('orties', 1)

				Harvest(source)
			end
		end
	end)
end

RegisterServerEvent('esx_alchimistejob:startHarvest')
AddEventHandler('esx_alchimistejob:startHarvest', function()
	local _source = source
	PlayersHarvesting[_source] = true
	TriggerClientEvent('esx:showNotification', _source, "Récupération d' ~b~Orties~s~...")
	Harvest(source)
end)

RegisterServerEvent('esx_alchimistejob:stopHarvest')
AddEventHandler('esx_alchimistejob:stopHarvest', function()
	local _source = source
	TriggerClientEvent('esx:showNotification', _source, 'Vous vous êtes trop éloigné de la~b~ zone de récolte (Orties)')
	PlayersHarvesting[_source] = false
end)
------------ Récupération Outils Réparation --------------
local function Harvest2(source)

	SetTimeout(4000, function()

		if PlayersHarvesting2[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local strange  = xPlayer.getInventoryItem('citrouilles').count
			if strange >= 5 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~plus de place')
			else
                xPlayer.addInventoryItem('citrouilles', 1)

				Harvest2(source)
			end
		end
	end)
end

RegisterServerEvent('esx_alchimistejob:startHarvest2')
AddEventHandler('esx_alchimistejob:startHarvest2', function()
	local _source = source
	PlayersHarvesting2[_source] = true
	TriggerClientEvent('esx:showNotification', _source, 'Récupération de ~b~Citrouilles~s~...')
	Harvest2(_source)
end)

RegisterServerEvent('esx_alchimistejob:stopHarvest2')
AddEventHandler('esx_alchimistejob:stopHarvest2', function()
	local _source = source
	TriggerClientEvent('esx:showNotification', _source, 'Vous vous êtes trop éloigné de la~b~ zone de récolte ( Citrouilles )')
	PlayersHarvesting2[_source] = false
end)
----------------- Récupération Outils Carosserie ----------------
local function Harvest3(source)

	SetTimeout(4000, function()

		if PlayersHarvesting3[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local crapaudQuantity  = xPlayer.getInventoryItem('crapaud').count
            if crapaudQuantity >= 5 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~plus de place')
			else
                xPlayer.addInventoryItem('crapaud', 1)

				Harvest3(source)
			end
		end
	end)
end

RegisterServerEvent('esx_alchimistejob:startHarvest3')
AddEventHandler('esx_alchimistejob:startHarvest3', function()
	local _source = source
	PlayersHarvesting3[_source] = true
	TriggerClientEvent('esx:showNotification', _source, 'Récupération d\'~b~oeils de crapauds~s~...')
	Harvest3(_source)
end)

RegisterServerEvent('esx_alchimistejob:stopHarvest3')
AddEventHandler('esx_alchimistejob:stopHarvest3', function()
	local _source = source
	TriggerClientEvent('esx:showNotification', _source, 'Vous vous êtes trop éloigné de la~b~ zone de récolte ( Crapaud )')
	PlayersHarvesting3[_source] = false
end)
------------ Craft Préapration -------------------
local function Craft(source)

	SetTimeout(4000, function()

		if PlayersCrafting[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local Orties = xPlayer.getInventoryItem('orties').count
			local Citrouilles = xPlayer.getInventoryItem('citrouilles').count
			local Crapaud = xPlayer.getInventoryItem('crapaud').count

			if Orties <= 0 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~pas assez~s~ d\'Orties')
			elseif Citrouilles <= 0 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~pas assez~s~ de Citrouilles')
			elseif Crapaud <= 0 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~pas assez~s~ de Crapauds')
			else
                xPlayer.removeInventoryItem('orties', 1)
                xPlayer.removeInventoryItem('citrouilles', 1)
                xPlayer.removeInventoryItem('crapaud', 1)
                xPlayer.addInventoryItem('strange', 1)

				Craft(source)
			end
		end
	end)
end

RegisterServerEvent('esx_alchimistejob:startCraft')
AddEventHandler('esx_alchimistejob:startCraft', function()
	local _source = source
	PlayersCrafting[_source] = true
	TriggerClientEvent('esx:showNotification', _source, 'Création d\'une base pour ~b~potions~s~...')
	Craft(_source)
end)

RegisterServerEvent('esx_alchimistejob:stopCraft')
AddEventHandler('esx_alchimistejob:stopCraft', function()
	local _source = source
	PlayersCrafting[_source] = false
end)
------------ Craft Potion d'énergie --------------
local function Craft2(source)

	SetTimeout(4000, function()

		if PlayersCrafting2[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local strange  = xPlayer.getInventoryItem('strange').count
			if strange < 5 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~pas assez~s~ de préparations')
			else
                xPlayer.removeInventoryItem('strange', 5)
                xPlayer.addInventoryItem('steroids', 1)

				Craft2(source)
			end
		end
	end)
end

RegisterServerEvent('esx_alchimistejob:startCraft2')
AddEventHandler('esx_alchimistejob:startCraft2', function()
	local _source = source
	PlayersCrafting2[_source] = true
	TriggerClientEvent('esx:showNotification', _source, 'Assemblage de ~b~potions d\'énergies~s~...')
	Craft2(_source)
end)

RegisterServerEvent('esx_alchimistejob:stopCraft2')
AddEventHandler('esx_alchimistejob:stopCraft2', function()
	local _source = source
	PlayersCrafting2[_source] = false
end)
----------------- Craft potion de soins ----------------
local function Craft3(source)

	SetTimeout(4000, function()

		if PlayersCrafting3[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local strange  = xPlayer.getInventoryItem('strange').count
			if strange < 5 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~pas assez~s~ de préparations')
			else
                xPlayer.removeInventoryItem('strange', 5)
                xPlayer.addInventoryItem('pvie', 1)

				Craft3(source)
			end
		end
	end)
end

RegisterServerEvent('esx_alchimistejob:startCraft3')
AddEventHandler('esx_alchimistejob:startCraft3', function()
	local _source = source
	PlayersCrafting3[_source] = true
	TriggerClientEvent('esx:showNotification', _source, 'Assemblage de ~b~potions de soins~s~...')
	Craft3(_source)
end)

RegisterServerEvent('esx_alchimistejob:stopCraft3')
AddEventHandler('esx_alchimistejob:stopCraft3', function()
	local _source = source
	PlayersCrafting3[_source] = false
end)


local function Craft4(source)

	SetTimeout(4000, function()

		if PlayersCrafting4[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local strange  = xPlayer.getInventoryItem('strange').count
			if strange < 10 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~pas assez~s~ de préparations')
			else
                xPlayer.removeInventoryItem('strange', 10)
                xPlayer.addInventoryItem('pjump', 1)

				Craft4(source)
			end
		end
	end)
end

RegisterServerEvent('esx_alchimistejob:startCraft4')
AddEventHandler('esx_alchimistejob:startCraft4', function()
	local _source = source
	PlayersCrafting4[_source] = true
	TriggerClientEvent('esx:showNotification', _source, 'Assemblage de ~b~potions gravitationelle~s~...')
	Craft4(_source)
end)

RegisterServerEvent('esx_alchimistejob:stopCraft4')
AddEventHandler('esx_alchimistejob:stopCraft4', function()
	local _source = source
	PlayersCrafting4[_source] = false
end)

local PlayersSellingCoke       = {}

local function SellCoke(source)

		if PlayersSellingCoke[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local strangeQ = xPlayer.getInventoryItem('strange').count

			if strangeQ == 0 then
				TriggerClientEvent('esx:showNotification', source, ('Pas de préparations à vendre'))
			else
					xPlayer.removeInventoryItem('strange', 1)
                    xPlayer.addAccountMoney(365)
                    TriggerClientEvent('esx:showNotification', source, ('Vous venez de vendre une préparation.'))
              end

				SellCoke(source)
			end


	end


RegisterServerEvent('esx_drugs:startSellCoke')
AddEventHandler('esx_drugs:startSellCoke', function()

	local _source = source

	PlayersSellingCoke[_source] = true

	TriggerClientEvent('esx:showNotification', _source, _U('sale_in_prog'))

	SellCoke(_source)

end)

RegisterServerEvent('esx_drugs:stopSellCoke')
AddEventHandler('esx_drugs:stopSellCoke', function()

	local _source = source

	PlayersSellingCoke[_source] = false

end)


ESX.RegisterUsableItem('pvie', function(source)
    local xPlayer = ESX.GetPlayerFromId(source)
    xPlayer.removeInventoryItem('pvie', 1)
    TriggerClientEvent('eden_alchimie:onPvie', source)
    TriggerClientEvent('esx:showNotification', source, ('Vous buvez une ~b~potion de soins~s~'))

end)

ESX.RegisterUsableItem('pjump', function(source)
    local xPlayer = ESX.GetPlayerFromId(source)
    xPlayer.removeInventoryItem('pjump', 1)
    TriggerClientEvent('eden_alchimie:onPJump', source)
    TriggerClientEvent('esx:showNotification', source, ('Vous buvez une ~b~potion de saut~s~'))

end)
