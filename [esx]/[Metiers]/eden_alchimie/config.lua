Config                        = {}
Config.DrawDistance           = 100.0
Config.MaxInService           = -1
Config.EnablePlayerManagement = true

Config.Zones = {

	alchimisteActions = {
		Pos   = {x = 2431.4340820313, y = 4963.7333984375, z = 42.345560882568},
		Size  = {x = 1.5, y = 1.5, z = 1.0},
		Color = {r = 204, g = 204, b = 0},
		Type  = 1
	},

	Orties = {
		Pos   = {x = 205.90548706055, y = -1327.7248535156, z = 29.89040184021},
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		Color = {r = 204, g = 204, b = 0},
		Type  = 1
	},
	Citrouilles = {
		Pos   = {x = 205.90548706055, y = -1350.7248535156, z = 29.89040184021},
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		Color = {r = 204, g = 204, b = 0},
		Type  = 1
	},
	Crapaud = {
		Pos   = {x = 210.90548706055, y = -1327.7248535156, z = 29.89040184021},
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		Color = {r = 204, g = 204, b = 0},
		Type  = 1
	},
	Craft = {
		Pos   = {x = 2433.9079589844, y = 4969.2338867188, z = 42.345560882568},
		Size  = {x = 1.5, y = 1.5, z = 1.0},
		Color = {r = 204, g = 204, b = 0},
		Type  = 1
	},

	VehicleSpawnPoint = {
		Pos   = {x = -157.7899017334, y = -1305.8979492188, z = 30.348382949829},
		Size  = {x = 1.5, y = 1.5, z = 1.0},
		Type  = -1
	},

	VehicleDeleter = {
		Pos   = {x = -157.7899017334, y = -1305.8979492188, z = 30.348382949829},
        Size  = {x = 3.0, y = 3.0, z = 1.0},
        Color = {r = 204, g = 204, b = 0},
        Type  = 1
	
	},
	CokeResell = {
	 	Pos   = {x = -242, y = -690.8979492188, z = 34},
        Size  = {x = 3.0, y = 3.0, z = 1.0},
        Color = {r = 204, g = 204, b = 0},
        Type  = 1 
		
		},
    
}
