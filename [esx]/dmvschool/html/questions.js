var tableauQuestion = [
	{   question : "Si vous conduisez à 110 km h, et vous approchez d'un quartier résidentiel, cela signifie que ",
        propositionA : "Vous devez accélérer",
        propositionB : "Vous pouvez accélérer tant qu'il n'y a pas d'autres voitures",
        propositionC : "Vous devez ralentir",
        propositionD : "Vous pouvez maintenir votre vitesse",
        reponse : "C"},
		
    {   question : "Vous voyez un policier au milieu de la route faisant signe de vous arrêter : ",
        propositionA : "Vous accélérez pour essayer de l'écraser",
        propositionB : "Vous continuez votre route",
        propositionC : "Vous vous arrêtez à son niveau",
        propositionD : "Vous faites demi tour en brandissant un joli doigt d'honneur par la fenêtre",
        reponse : "C"},
		
    {   question : "D'après les règles, la vitesse maximale dans une zone urbaine est de ___ km h. ",
        propositionA : "70",
        propositionB : "850",
        propositionC : "240",
        propositionD : "15",
        reponse : "A"},
	
	{   question : "Avant chaque changement de voie, vous devriez : ",
        propositionA : "Vérifier vos rétroviseurs",
        propositionB : "Vérifier vos angles morts",
        propositionC : "Vérifier votre vitesse",
        propositionD : "Tout ce qui précède",
        reponse : "D"},
		
	{   question : "Vous voyez une femme magnifique, seins nus, sur le coté ",
        propositionA : "Vous la klaxonnez",
        propositionB : "Vous descendez lui remettre ses habits et lui prendre son numéro de tél",
        propositionC : "Vous restez concentré sur la route",
        propositionD : "Vous préférez les hommes",
        reponse : "C"},
		
	{   question : "Vous devez vous arrêter jusqu'à ce que tout véhicule d'urgence avec une sirène soit passé, sauf si : ",
        propositionA : "Vous êtes pressé",
        propositionB : "Y'a votre femme qui vous attend ...",
        propositionC : "Vous êtes dans une zone scolaire",
        propositionD : "Vous êtes dans une intersection",
        reponse : "D"},
	
	{   question : "Le droit de passage doit être cédé aux véhicules d'urgence ",
        propositionA : "Tout le temps",
        propositionB : "Quand ils ont les clignotants en mode warning",
        propositionC : "Quand ils utilisent une sirène et des lumières clignotantes rouge, bleu ou blanc",
        propositionD : "Quand c'est l'heure de l'apéro",
        reponse : "C"},
		
	{   question : "Quel type de poulet préférez-vous ",
        propositionA : "Poulet au curry",
        propositionB : "Poulet basquaise",
        propositionC : "Cette question n'a rien à faire dans ce test",
        propositionD : "Poulet sauce concombre",
        reponse : "C"},
		
	{   question : "Vous conduisez sur une autoroute avec une limite de vitesse de 110 kmh. La plupart du trafic se déplace à 130 kmh, donc vous devriez conduire pas plus vite que ",
        propositionA : "80 kmh",
        propositionB : "10 kmh",
        propositionC : "25 kmh",
        propositionD : "110 kmh",
        reponse : "D"},
		
	{   question : "Lorsque vous vous faites dépasser par un autre véhicule, il NE FAUT PAS ",
        propositionA : "Tirer sur le frein à main",
        propositionB : "Vérifier vos sms",
        propositionC : "Regarder l'autre conducteur",
        propositionD : "Réponse A B et C",
        reponse : "D"},
]
