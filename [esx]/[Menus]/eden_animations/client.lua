--
-- Originally created by SuperCoolNinja.
--

Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57, 
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177, 
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70, 
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

-- Options
local options = {
    x = 0.900,
    y = 0.32,
    width = 0.2,
    height = 0.04,
    scale = 0.4,
    font = 2,
    menu_title = "Animations",
    menu_subtitle = "Categories",
    color_r = 100,
    color_g = 130,
    color_b = 130,
}
------------------------------------------------------------------------------------------------------------------------
-- Base du menu
function PersonnalMenu()
    ped = GetPlayerPed(-1);
	options.menu_subtitle = "Animations"
    ClearMenu()	
	Menu.addButton("~b~Festives", "animsFestives", nil)
	Menu.addButton("~b~Humeurs", "animsHumor", nil)
    Menu.addButton("~b~Mimiques", "animsMimiques", nil)
	Menu.addButton("~b~Mouvements", "animsMouvements", nil)
    Menu.addButton("~b~Salutations", "animsSalute", nil)    
    Menu.addButton("~b~Sports", "animsSportives", nil)
    Menu.addButton("~b~Travail", "animsWork", nil)
    Menu.addButton("~r~Adulte (18+)", "animsAdultes", nil)        
    Menu.addButton("~b~Autres", "animsOthers", nil)
    Menu.addButton("~c~---------------------------", "quitter", nil)
    Menu.addButton("Se rendre", "animOnKnee", nil)
    Menu.addButton("Faire une photo", "animsActionScenario", { anim = "WORLD_HUMAN_TOURIST_MOBILE" })
    Menu.addButton("~o~Quitter", "quitter", nil)
end
------------------------------------------------------------------------------------------------------------------------

-- Handles when a player spawns either from joining or after death
RegisterNetEvent("pm:notifs")
AddEventHandler("pm:notifs", function(msg)
    notifs(msg)
end)

function notifs(msg)
    SetNotificationTextEntry("STRING")
    AddTextComponentString( msg )
    DrawNotification(false, false)
end

------------------------------------------------------------------------------------------------------------------------

-- If a player move, stop the animation
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        if IsPedUsingAnyScenario(GetPlayerPed(-1)) then
            if IsControlJustPressed(1, Keys["A"]) or IsControlJustPressed(1, Keys["W"]) or IsControlJustPressed(1, Keys["S"]) or IsControlJustPressed(1, Keys["D"]) then
                ClearPedTasks(GetPlayerPed(-1))
            end
        end
    end
end)

--------------------------------------------------------------------------------------------------------------------------

function animsFestives()
    options.menu_subtitle = "Festives"
    ClearMenu()

    Menu.addButton("Boire une bière", "animsActionScenario", { anim = "WORLD_HUMAN_DRINKING" })
    Menu.addButton("Près du feu (H)", "animsActionScenario", { anim = "WORLD_HUMAN_STAND_FIRE" })
    Menu.addButton("Jouer de la musique (H)", "animsActionScenario", {anim = "WORLD_HUMAN_MUSICIAN" })
	Menu.addButton("Faire la fête", "animsActionScenario", {anim = "WORLD_HUMAN_PARTYING" })
	Menu.addButton("Rock and roll !", "animsAction", { lib = "mp_player_int_upperrock", anim = "mp_player_int_rock" })
	Menu.addButton("Jouer de l'air guitar", "animsAction", { lib = "anim@mp_player_intcelebrationfemale@air_guitar", anim = "air_guitar" })
    Menu.addButton("Faire le DJ", "animsAction", {lib = "anim@mp_player_intcelebrationfemale@dj", anim = "dj"})
    Menu.addButton("Être bourré sur place", "animsAction", {lib = "amb@world_human_bum_standing@drunk@idle_a", anim = "idle_a"})
    Menu.addButton("Vomir en voiture", "animsAction", {lib = "oddjobs@taxi@tie", anim = "vomit_outside"})

    Menu.addButton("~c~---------------------------", "PersonnalMenu", nil)
    Menu.addButton("~o~Retour","PersonnalMenu",nil)
end

function animsHumor()
    options.menu_subtitle = "Humeurs"
    ClearMenu()

    Menu.addButton("Féliciter", "animsActionScenario", {anim = "WORLD_HUMAN_CHEERING" })
    Menu.addButton("Damn ! ", "animsAction", { lib = "gestures@m@standing@casual", anim = "gesture_damn" })
    Menu.addButton("No way", "animsAction", { lib = "gestures@m@standing@casual", anim = "gesture_no_way" })
    Menu.addButton("Doigt d'honneur", "animsAction", { lib = "mp_player_int_upperfinger", anim = "mp_player_int_finger_01_enter" })
    Menu.addButton("Embrasser", "animsAction", { lib = "mp_ped_interaction", anim = "kisses_guy_a" })
	Menu.addButton("Super !", "animsAction", { lib = "anim@mp_player_intcelebrationmale@thumbs_up", anim = "thumbs_up" })
	Menu.addButton("Calme-toi", "animsAction", { lib = "gestures@m@standing@casual", anim = "gesture_easy_now" })
	Menu.addButton("Avoir peur", "animsAction", { lib = "amb@code_human_cower_stand@female@idle_a", anim = "idle_c" })
    Menu.addButton("Facepalm", "animsAction", {lib = "anim@mp_player_intcelebrationmale@face_palm", anim = "face_palm"})    

    Menu.addButton("~c~---------------------------", "PersonnalMenu", nil)
	Menu.addButton("~o~Retour","PersonnalMenu",nil)
end

function animsMimiques()
    options.menu_subtitle = "Mimiques"
    ClearMenu()

    Menu.addButton("Moi", "animsAction", {lib = "gestures@m@standing@casual", anim = "gesture_me"})
    Menu.addButton("Allez, viens !", "animsAction", {lib = "gestures@m@standing@casual", anim = "gesture_come_here_soft"})
    Menu.addButton("Provoquer", "animsAction", {lib = "gestures@m@standing@casual", anim = "gesture_bring_it_on"})
    Menu.addButton("Je le savais, putain !", "animsAction", {lib = "anim@am_hold_up@male", anim = "shoplift_high"})
    Menu.addButton("Oh merde ...", "animsAction", {lib = "amb@world_human_bum_standing@depressed@idle_a", anim = "idle_a"})
    Menu.addButton("Qu'est-ce que j'ai fait ?!", "animsAction", {lib = "oddjobs@assassinate@multi@", anim = "react_big_variations_a"})
    Menu.addButton("Tu veux te battre ?", "animsAction", {lib = "anim@deathmatch_intros@unarmed", anim = "intro_male_unarmed_e"})

    Menu.addButton("~c~---------------------------", "PersonnalMenu", nil)
	Menu.addButton("~o~Retour","PersonnalMenu",nil)
end

function animsMouvements()
	options.menu_subtitle = "Mouvements"
	ClearMenu()

	Menu.addButton("S'asseoir (1)", "animsActionScenario", { anim = "PROP_HUMAN_SEAT_ARMCHAIR" })
	Menu.addButton("S'asseoir (2)", "animsAction", { lib = "anim@heists@prison_heistunfinished_biztarget_idle", anim = "target_idle" })
	Menu.addButton("S'asseoir (par terre)", "animsActionScenario", { anim = "WORLD_HUMAN_PICNIC" })	
	Menu.addButton("S'adosser", "animsActionScenario", { anim = "WORLD_HUMAN_GUARD_STAND" })
	Menu.addButton("S'adosser (aléatoire)", "animsActionScenario", { anim = "WORLD_HUMAN_LEANING" })
	Menu.addButton("S'agenouiller", "animsActionScenario", { anim = "CODE_HUMAN_MEDIC_KNEEL" })
	Menu.addButton("Se gratter les couilles", "animsAction", { lib = "mp_player_int_uppergrab_crotch", anim = "mp_player_int_grab_crotch" })
	Menu.addButton("Se branler", "animsAction", { lib = "mp_player_int_upperwank", anim = "mp_player_int_wank_01" })
    Menu.addButton("Balle dans la tête", "animsAction", { lib = "mp_suicide", anim = "pistol" })        
    Menu.addButton("Ecouter à la porte", "animsAction", {lib = "mini@safe_cracking", anim = "idle_base"})

    Menu.addButton("~c~---------------------------", "PersonnalMenu", nil)
	Menu.addButton("~o~Retour","PersonnalMenu",nil)
end

function animsSalute()
    options.menu_subtitle = "Salutations"
    ClearMenu()

	Menu.addButton("Lever les bras", "animsAction", { lib = "ped", anim = "handsup" })
    Menu.addButton("Serrer la main", "animsAction", { lib = "mp_common", anim = "givetake1_a" })
    Menu.addButton("Dire bonjour", "animsAction", { lib = "gestures@m@standing@casual", anim = "gesture_hello" })
    Menu.addButton("Tapes m'en 5", "animsAction", { lib = "mp_ped_interaction", anim = "highfive_guy_a" })
    Menu.addButton("Salut militaire", "animsAction", { lib = "mp_player_int_uppersalute", anim = "mp_player_int_salute" })
    Menu.addButton("Tchek !", "animsAction", {lib = "mp_ped_interaction", anim = "handshake_guy_a"})
    Menu.addButton("Salut de gang", "animsAction", {lib = "mp_ped_interaction", anim = "hugs_guy_a"})

    Menu.addButton("~c~---------------------------", "PersonnalMenu", nil)
    Menu.addButton("~o~Retour","PersonnalMenu",nil)
end

function animsSportives()
    options.menu_subtitle = "Sports"
    ClearMenu()

    Menu.addButton("Faire du yoga", "animsActionScenario", { anim = "WORLD_HUMAN_YOGA" })
    Menu.addButton("Jogging", "animsActionScenario", { anim = "WORLD_HUMAN_JOG_STANDING" })
    Menu.addButton("Faire des pompes (H)", "animsActionScenario", { anim = "WORLD_HUMAN_PUSH_UPS" })
	Menu.addButton("Faire des abdos (H)", "animsActionScenario", { anim = "WORLD_HUMAN_SIT_UPS" })
	Menu.addButton("Montrer ses muscles", "animsActionScenario", { anim = "WORLD_HUMAN_MUSCLE_FLEX" })
    Menu.addButton("Être épuisé", "animsAction", {lib = "amb@world_human_jog_standing@male@idle_b", anim = "idle_d"})

    Menu.addButton("~c~---------------------------", "PersonnalMenu", nil)
    Menu.addButton("~o~Retour","PersonnalMenu",nil)
end

function animsWork()
    options.menu_subtitle = "Travail"
    ClearMenu()

    Menu.addButton("Prendre des notes (H)", "animsActionScenario", { anim = "WORLD_HUMAN_CLIPBOARD" })
	Menu.addButton("Pêcher", "animsActionScenario", { anim = "WORLD_HUMAN_STAND_FISHING" })	
	Menu.addButton("Faire l'enquête", "animsAction", {lib = "amb@code_human_police_investigate@idle_b", anim = "idle_f"})   
    Menu.addButton("Faire la circulation (H)", "animsActionScenario", { anim = "WORLD_HUMAN_CAR_PARK_ATTENDANT"}) 
    Menu.addButton("Parler au client (V)", "animsAction", {lib = "oddjobs@taxi@driver", anim = "leanover_idle"})
    Menu.addButton("Donner la facture (V)", "animsAction", {lib = "oddjobs@taxi@cyi", anim = "std_hand_off_ps_passenger"})
    Menu.addButton("Donner la commande", "animsAction", {lib = "mp_am_hold_up", anim = "purchase_beerbox_shopkeeper"})
    Menu.addButton("Servir un shot", "animsAction", {lib = "mini@drinking", anim = "shots_barman_b"})    
	Menu.addButton("Nettoyer les traces", "animsActionScenario", { anim = "WORLD_HUMAN_MAID_CLEAN" })
    Menu.addButton("Tenir le balai", "animsActionScenario", { anim = "WORLD_HUMAN_JANITOR" })    
    Menu.addButton("Bouche à bouche", "animsAction", {lib = "mini@cpr@char_a@cpr_str", anim = "cpr_kol"})

    Menu.addButton("~c~---------------------------", "PersonnalMenu", nil)
    Menu.addButton("~o~Retour","PersonnalMenu",nil)
end

function animsAdultes()
    options.menu_subtitle = "Adultes"
    ClearMenu()

    Menu.addButton("Se faire s*ç*r (V)(H)", "animsAction", {lib = "oddjobs@towing", anim = "m_blow_job_loop"})
    Menu.addButton("Fellation en voiture (V)(F)", "animsAction", {lib = "oddjobs@towing", anim = "f_blow_job_loop"})
    Menu.addButton("Homme b**se (V)(H)", "animsAction", {lib = "mini@prostitutes@sexlow_veh", anim = "low_car_sex_loop_player"})
    Menu.addButton("Femme b**se (V)(F)", "animsAction", {lib = "mini@prostitutes@sexlow_veh", anim = "low_car_sex_loop_female"})
    Menu.addButton("Faire du charme", "animsAction", {lib = "mini@strip_club@idles@stripper", anim = "stripper_idle_02"})
    Menu.addButton("Pose de s****e (1)", "animsActionScenario", { anim = "WORLD_HUMAN_PROSTITUTE_LOW_CLASS"})
    Menu.addButton("Pose de s****e (2)", "animsActionScenario", { anim = "WORLD_HUMAN_PROSTITUTE_HIGH_CLASS"})
    Menu.addButton("Montrer ses seins", "animsAction", {lib = "mini@strip_club@backroom@", anim = "stripper_b_backroom_idle_b"})
    Menu.addButton("Strip tease (1)", "animsAction", {lib = "mini@strip_club@lap_dance@ld_girl_a_song_a_p1", anim = "ld_girl_a_song_a_p1_f"})
    Menu.addButton("Strip tease (2)", "animsAction", {lib = "mini@strip_club@private_dance@part2", anim = "priv_dance_p2"})
    Menu.addButton("Strip tease au sol", "animsAction", {lib = "mini@strip_club@private_dance@part3", anim = "priv_dance_p3"})

    Menu.addButton("~c~---------------------------", "PersonnalMenu", nil)
    Menu.addButton("~o~Retour","PersonnalMenu",nil)
end

function animsOthers()
    options.menu_subtitle = "Autres"
    ClearMenu()	

    Menu.addButton("Bronzer (sur le dos)", "animsActionScenario", { anim = "WORLD_HUMAN_SUNBATHE_BACK" })
	Menu.addButton("Bronzer (sur le ventre)", "animsActionScenario", { anim = "WORLD_HUMAN_SUNBATHE" })
    Menu.addButton("Filmer", "animsActionScenario", { anim = "WORLD_HUMAN_MOBILE_FILM_SHOCKING" })
	Menu.addButton("Faire le paparazzi (H)", "animsActionScenario", { anim = "WORLD_HUMAN_PAPARAZZI" })
	Menu.addButton("Taper un texto", "animsActionScenario", { anim = "WORLD_HUMAN_STAND_MOBILE" })
	Menu.addButton("Apprécier le spectacle (H)", "animsActionScenario", { anim = "WORLD_HUMAN_STRIP_WATCH_STAND" })
    --Menu.addButton("Fumer une clope", "animsActionScenario", { anim = "WORLD_HUMAN_SMOKING" })
    Menu.addButton("Faire la manche (H)", "animsActionScenario", { anim = "WORLD_HUMAN_BUM_FREEWAY"})
    Menu.addButton("Faire la statue", "animsActionScenario", { anim = "WORLD_HUMAN_HUMAN_STATUE"})
    Menu.addButton("Faire à manger (H)", "animsActionScenario", { anim = "PROP_HUMAN_BBQ"})

    Menu.addButton("~c~---------------------------", "PersonnalMenu", nil)
    Menu.addButton("~o~Retour","PersonnalMenu", nil)	
end

----------------------------------------------------------------------------------------------------------------------------

function animsAction(animObj)
    RequestAnimDict( animObj.lib )
    while not HasAnimDictLoaded( animObj.lib ) do
        Citizen.Wait(0)
    end
    if HasAnimDictLoaded( animObj.lib ) then
        TaskPlayAnim( GetPlayerPed(-1), animObj.lib , animObj.anim ,8.0, -8.0, -1, 0, 0, false, false, false )
    end
end

function animsActionScenario(animObj)
    local ped = GetPlayerPed(-1);

    if ped then
        local pos = GetEntityCoords(ped);
        local head = GetEntityHeading(ped);
        TaskStartScenarioInPlace(ped, animObj.anim, 0, false)    
	end
end

function animOnKnee()
    local player = GetPlayerPed(-1)
    if (DoesEntityExist(player) and not IsEntityDead(player)) then

        RequestAnimDict("random@arrests")
        RequestAnimDict("random@arrests@busted")

        while not HasAnimDictLoaded("random@arrests") do Citizen.Wait(0) end
        while not HasAnimDictLoaded("random@arrests@busted") do Citizen.Wait(0) end

        if (IsEntityPlayingAnim(player, "random@arrests@busted", "idle_a", 3)) then
            TaskPlayAnim(player, "random@arrests@busted", "exit", 8.0, 1.0, -1, 2, 0, 0, 0, 0)
			Citizen.Wait(2000)
            TaskPlayAnim(player, "random@arrests", "kneeling_arrest_get_up", 8.0, 1.0, -1, 128, 0, 0, 0, 0)
        else
            TaskPlayAnim(player, "random@arrests", "idle_2_hands_up", 8.0, 1.0, -1, 2, 0, 0, 0, 0)
			Citizen.Wait(4000)
            TaskPlayAnim(player, "random@arrests", "kneeling_arrest_idle", 8.0, 1.0, -1, 2, 0, 0, 0, 0)
			Citizen.Wait(500)
			TaskPlayAnim(player, "random@arrests@busted", "enter", 8.0, 1.0, -1, 2, 0, 0, 0, 0)
			Citizen.Wait(1000)
			TaskPlayAnim(player, "random@arrests@busted", "idle_a", 8.0, 1.0, -1, 9, 0, 0, 0, 0)
        end     

        quitter()

    end
end

Citizen.CreateThread(function()
    while true do
        
        Citizen.Wait(0)

        if IsEntityPlayingAnim(GetPlayerPed(PlayerId()), "random@arrests@busted", "idle_a", 3) then
            DisableControlAction(1, 140, true)
            DisableControlAction(1, 141, true)
            DisableControlAction(1, 142, true)
            DisableControlAction(0,21,true)
        end

        if menuIsOpen ~= 0 then
            if IsControlJustPressed(1, KeyToucheClose) then
                closeGui()
            elseif menuIsOpen == 2 then
                local ply = GetPlayerPed(-1)
                DisableControlAction(0, 1, true)
                DisableControlAction(0, 2, true)
                DisablePlayerFiring(ply, true)
                DisableControlAction(0, 142, true)
                DisableControlAction(0, 106, true)
                if IsDisabledControlJustReleased(0, 142) then
                    SendNUIMessage({method = "clickGui"})
                end
            end
        end
    end
end)

function getPlayerID(source)
    local identifiers = GetPlayerIdentifiers(source)
    local player = getIdentifiant(identifiers)
    return player
end

function getIdentifiant(id)
    for _, v in ipairs(id) do
        return v
    end
end

--====================================================================================
--  User Event
--====================================================================================

function Notify(text)
    SetNotificationTextEntry('STRING')
    AddTextComponentString(text)
    DrawNotification(false, true)
end

function quitter ()
Menu.hidden = true
end


------------------------------------------------------------------------------------------------------------------------

function getPlayers()
    local playerList = {}
    for i = 0, 32 do
        local player = GetPlayerFromServerId(i)
        if NetworkIsPlayerActive(player) then
            table.insert(playerList, player)
        end
    end
    return playerList
end

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
		if IsControlJustPressed(1, Keys["BACKSPACE"]) then -- Si touche retour on cache
			Menu.hidden = true
		end
        if IsControlJustPressed(1, Keys["F3"]) then --Modifier L'ouverture du Menu Ici ! Touche F3 de base.
            PersonnalMenu()
			-- Menu to draw
            Menu.hidden = not Menu.hidden -- Hide/Show the menu
        end
		Menu.renderGUI(options) -- Draw menu on each tick if Menu.hidden = false
    end
end)