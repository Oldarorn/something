Locales['en'] = {


	--Title and subtitles 
	
		['demarches'] = 'Gait',
		['categories'] = 'Categories',
		['shumeurs'] = 'Moods',
		['sattitudes'] = 'Attitudes',
		['sautres'] = 'Other',
		['sfemmes'] = 'Special Women',
		['reset'] = '~r~Reset',
		['quitter'] = "~o~Quit",
		
	--Main Menu
	
		['humeurs'] = '~b~Moods',
		['attitudes'] = '~b~Attitudes',
		['autres'] = '~b~Other',
		['femmes'] = '~b~Special Women',
	
	-- Moods
	
		['determine'] = 'Determiné',
		['triste'] = 'Triste',
		['depression'] = 'Depression',
		['enerve'] = 'Enervé',
		['presse'] = 'Pressé',
		['timide'] = 'Timide',
		['lunatique'] = 'Lunatique',
		['stresse'] = 'Stressé',
		['flemme'] = 'Flemme',
		
	-- Attitudes
	
		['hautain'] = 'Hautain',
		['badboy'] = 'Bad Boy',
		['gangster'] = 'Gangster',
		['fraquasse'] = 'Fraquassé',
		['coquille'] = 'Coquille Vide',
		['coince'] = 'Coincé',
		['perdu'] = 'Perdu',
		['intimidant'] = 'Intimidant',
		['richissime'] = 'Richissime',
		['hargneux'] = 'Hargneux',
		['imposant'] = 'Imposant',
		['frimeur'] = 'Frimeur',
		
	-- Others
	
		['randonneur'] = 'Randonneur',		
		['blesse'] = 'Blessé',
		['obese'] = 'Obèse',			
		['fesses'] = 'Mal aux fesses',
		['detente'] = 'Detente',
		
	-- Special Women
	
		['arrogante'] = 'Arrogante',
		['classique'] = 'Classique',
		['fragile'] = 'Fragile',
		['enerve'] = 'Enervée',
		['fatale'] = 'Femme fatale',
		['fuite'] = 'Fuite',
		['tristesse'] = 'Tristesse',
		['rebelle'] = 'Rebelle',
		['serieuse'] = 'Serieuse',
		['ffesses'] = 'Roule des fesses',
		['hautaines'] = 'Hautaine',
}




