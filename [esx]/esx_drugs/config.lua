Config              = {}
Config.MarkerType   = 9
Config.DrawDistance = 3.0
Config.ZoneSize     = {x = 6.0, y = 6.0, z = 3.0}
Config.MarkerColor  = {r = 128, g = 128, b = 128}
Config.RequiredCopsCoke = 0
Config.RequiredCopsMeth = 0
Config.RequiredCopsWeed = 0
Config.RequiredCopsOpium = 0
Config.Locale = 'fr'

Config.Zones = {
	CokeFarm = 		{ x = -2020.5458984375, y = 659.57281494141, z = 126.03147125244 },
	CokeTreatment = { x = 1151.1339111328,y = -438.08801269531,z = 67.003875732422 },
	CokeResell = 	{ x = 1232.5384521484, y = -3034.8950195313,z = 9.3650169372559 },
	MethFarm = 		{x=2, y=2,  z=700},
	MethTreatment = {x=2, y=2,  z=700},
	MethResell = 	{x=2, y=2, z=700},
	WeedFarm = 		{x=2229.7268066406,  y=5576.9487304688,  z=53.915142059326},
	WeedTreatment = {x=-524.0732421875,  y=573.10925292969,  z=121.20484161377},
	WeedResell = 	{x=1435.8843994141, y=3640.0561523438, z=34.945152282715},
	OpiumFarm = 	 {x=3432.65, y=5514.91,  z=21.07},
	OpiumTreatment = {x=1743.26, y=-1623.17,  z=112.41},
	OpiumResell = 	 {x=-946.58, y=-1051.64, z=2.17}
}