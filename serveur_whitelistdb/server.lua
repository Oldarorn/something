-- Script made by Havanna (http://steamcommunity.com/id/HavannaPC/)
AddEventHandler( "playerConnecting", function(name, setReason )
	local _source  = source
	local identifier = GetPlayerIdentifiers(_source)[1]
	--print(identifier)
	if not isWhiteListed(identifier) then
		setReason("Vous n'etes pas WL sur le serveur. Votre id : " .. identifier)
		print("(" .. identifier .. ") " .. name .. " has been kicked because he is not WhiteListed")
		CancelEvent()
	elseif isBanned(identifier) then
		setReason("Vous etes ban sur le serveur. Votre id : " .. identifier)
		print("(" .. identifier .. ") " .. name .. " has been kicked because he is banned")
		CancelEvent()
    end
end)

-- Chat Command to add someone in WhiteList
TriggerEvent('es:addGroupCommand', 'wladd', "admin", function(source, args, user)
	if #args == 2 then
		if isWhiteListed(args[2]) then
			TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, args[2] .. " is already whitelisted!")
		else
			addWhiteList(args[2])
			TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, args[2] .. " has been whitelisted!")
		end
	else
		TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Incorrect identifier!")
	end
end, function(source, args, user)
	TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Insufficienct permissions!")
end)

-- Chat Command to remove someone in WhiteList
TriggerEvent('es:addGroupCommand', 'wlremove', "admin", function(source, args, user)
	if #args == 2 then
		if isWhiteListed(args[2]) then
			removeWhiteList(args[2])
			TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, args[2] .. " is no longer whitelisted!")
		else
			TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, args[2] .. " is not whitelisted from!")
		end
	else
		TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Incorrect identifier!")
	end
end, function(source, args, user)
	TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Insufficienct permissions!")
end)

-- Chat Command to add someone in ban
TriggerEvent('es:addGroupCommand', 'wladdban', "admin", function(source, args, user)
	if #args == 2 then
		if not isWhiteListed(args[2]) then
			TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, args[2] .. " doesn't exist!")
		else
			addBanWhiteList(args[2])
			TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, args[2] .. " has been ban!")
		end
	else
		TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Incorrect identifier!")
	end
end, function(source, args, user)
	TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Insufficienct permissions!")
end)

-- Chat Command to remove someone in ban
TriggerEvent('es:addGroupCommand', 'wlremoveban', "admin", function(source, args, user)
	if #args == 2 then
		if not isWhiteListed(args[2]) then
			TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, args[2] .. " doesn't exist!")
		else
			removeBanWhiteList(args[2])
			TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, args[2] .. " has been unban!")
		end
	else
		TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Incorrect identifier!")
	end
end, function(source, args, user)
	TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Insufficienct permissions!")
end)

function addWhiteList(identifier)
	MySQL.Sync.execute("INSERT INTO user_whitelist (`identifier`, `whitelisted`) VALUES (@identifier, @whitelisted)",{['@identifier'] = identifier, ['@whitelisted'] = 1})
end

function removeWhiteList(identifier)
	MySQL.Sync.execute("DELETE FROM user_whitelist WHERE identifier = @identifier", {['@identifier'] = identifier})
end

function addBanWhiteList(identifier)
	MySQL.Sync.execute("UPDATE user_whitelist SET ban = 1 WHERE identifier = @identifier", {['@identifier'] = identifier})
end

function removeBanWhiteList(identifier)
	MySQL.Sync.execute("UPDATE user_whitelist SET ban = 0 WHERE identifier = @identifier", {['@identifier'] = identifier})
end

function isWhiteListed(identifier)
	local identifier = identifier
	local result = MySQL.Sync.fetchScalar("SELECT * FROM user_whitelist WHERE identifier = @username AND whitelisted = 1", {['@username'] = identifier})
	-- MySQL.ASync.fetchAll("SELECT identifier FROM user_whitelist WHERE identifier = '@username' AND whitelisted = 1",{['@username'] = identifier},function(result)		
	if (result) then
		return true
	end
	return false
end

function isBanned(identifier)
	local identifier = identifier
	local result = MySQL.Sync.fetchScalar("SELECT * FROM user_whitelist WHERE identifier = @username AND ban = 1", {['@username'] = identifier})
	-- MySQL.ASync.fetchAll("SELECT identifier FROM user_whitelist WHERE identifier = '@username' AND whitelisted = 1",{['@username'] = identifier},function(result)		
	if (result) then
		return true
	end
	return false
end

-- Rcon command to add/remove someone in WhiteList
AddEventHandler('rconCommand', function(commandName, args)
	if commandName == 'wladd' then
		if #args ~= 1 then
			RconPrint("Usage: whitelistadd [identifier]\n")
			CancelEvent()
			return
		end
		if isWhiteListed(args[1]) then
			RconPrint(args[1] .. " is already whitelisted!\n")
			CancelEvent()
		else
			addWhiteList(args[1])
			RconPrint(args[1] .. " has been whitelisted!\n")
			CancelEvent()
		end
	elseif commandName == 'wlremove' then
		if #args ~= 1 then
			RconPrint("Usage: whitelistremove [identifier]\n")
			CancelEvent()
			return
		end
		if isWhiteListed(args[1]) then
			removeWhiteList(args[1])
			RconPrint(args[1] .. " is no longer whitelisted!\n")
			CancelEvent()
		else
			RconPrint(args[1] .. " is not whitelisted!\n")
			CancelEvent()
		end
	end
end)

