--[[Register]]--

RegisterNetEvent("ply_docks:getBoat")
RegisterNetEvent('ply_docks:SpawnBoat')
RegisterNetEvent('ply_docks:StoreBoat')
RegisterNetEvent('ply_docks:SelBoat')



--[[Local/Global]]--

boats = {}

local vente_location = {-774.553, -1425.26, 1.000} -- a definir

local docks = {
	{name="Dock", colour=3, id=356, x=-794.352, y=-1501.18, z=1.000, axe = 120.000},
	{name="Plaisance", colour=3, id=356, x=1427.57800292969, y=3755.90869140625, z=30.9241371154785, axe = 120.000},
	{name="Dock", colour=3, id=356, x=-286.68896484375, y=6622.47412109375, z=1.000, axe = 120.000},
}

dockSelected = { {x=nil, y=nil, z=nil}, }

--[[Functions]]--

--[[Menu Dock]]--

function MenuDock()
    ped = GetPlayerPed(-1);
    MenuTitle = "Dock"
    ClearMenu()
    Menu.addButton("Rentrer le bateau","RentrerBateau",nil)
    Menu.addButton("Sortir un bateau","ListeBateau",nil)
    Menu.addButton("Fermer","CloseMenu",nil) 
end

function RentrerBateau()
	TriggerServerEvent('ply_docks:CheckForBoat',source)
	CloseMenu()
end

function ListeBateau()
    ped = GetPlayerPed(-1);
    MenuTitle = "Bateaux"
    ClearMenu()
    for ind, value in pairs(BOATS) do
            Menu.addButton(tostring(value.boat_name) .. " : " .. tostring(value.boat_state), "OptionBateau", value.id)
    end    
    Menu.addButton("Retour","MenuDock",nil)
end

function OptionBateau(boatID)
	local boatID = boatID
    MenuTitle = "Options"
    ClearMenu()
    Menu.addButton("Sortir", "SortirBateau", boatID)
    Menu.addButton("Retour", "ListeVBateau", nil)
end

function SortirBateau(boatID)
	local boatID = boatID
	TriggerServerEvent('ply_docks:CheckForSpawnBoat', boatID)
	CloseMenu()
end


---Generic Fonction

function drawNotification(text)
	SetNotificationTextEntry("STRING")
	AddTextComponentString(text)
	DrawNotification(false, false)
end

function CloseMenu()
    Menu.hidden = true
end

function LocalPed()
	return GetPlayerPed(-1)
end

function Chat(debugg)
    TriggerEvent("chatMessage", '', { 0, 0x99, 255 }, tostring(debugg))
end

function drawTxt(text,font,centre,x,y,scale,r,g,b,a)
	SetTextFont(font)
	SetTextProportional(0)
	SetTextScale(scale, scale)
	SetTextColour(r, g, b, a)
	SetTextDropShadow(0, 0, 0, 0,255)
	SetTextEdge(1, 0, 0, 0, 255)
	SetTextDropShadow()
	SetTextOutline()
	SetTextCentre(centre)
	SetTextEntry("STRING")
	AddTextComponentString(text)
	DrawText(x , y)
end

--[[Citizen]]--

--dock
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		for _, dock in pairs(docks) do
			DrawMarker(1, dock.x, dock.y, dock.z, 0, 0, 0, 0, 0, 0, 5.001, 5.0001, 0.5001, 0, 155, 255, 200, 0, 0, 0, 0)
			if GetDistanceBetweenCoords(dock.x, dock.y, dock.z, GetEntityCoords(LocalPed())) < 10 and IsPedInAnyVehicle(LocalPed(), true) == false then
				drawTxt('~g~E~s~ pour ouvrir le menu',0,1,0.5,0.8,0.6,255,255,255,255)
				if IsControlJustPressed(1, 86) then
					dockSelected.x = dock.x
					dockSelected.y = dock.y
					dockSelected.z = dock.z
					dockSelected.axe = dock.axe
					MenuDock()
					Menu.hidden = not Menu.hidden 
				end
				Menu.renderGUI() 
			end
		end
	end
end)

--closmenurange
Citizen.CreateThread(function()
	while true do
		local near = false
		Citizen.Wait(0)
		for _, dock in pairs(docks) do		
			if (GetDistanceBetweenCoords(dock.x, dock.y, dock.z, GetEntityCoords(LocalPed())) < 10 and near ~= true) then 
				near = true							
			end
		end
		if near == false then 
			Menu.hidden = true;
		end
	end
end)

--blips
Citizen.CreateThread(function()
    for _, item in pairs(docks) do
		item.blip = AddBlipForCoord(item.x, item.y, item.z)
		SetBlipSprite(item.blip, item.id)
		SetBlipAsShortRange(item.blip, true)
		SetBlipColour(item.blip, item.colour)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString(item.name)
		EndTextCommandSetBlipName(item.blip)
    end
end)


--vente
Citizen.CreateThread(function()
	local loc = vente_location
	local pos = vente_location
	local blip = AddBlipForCoord(pos[1],pos[2],pos[3])
	SetBlipSprite(blip,207)
	SetBlipColour(blip, 3)
	BeginTextCommandSetBlipName("STRING")
	AddTextComponentString('Revente')
	EndTextCommandSetBlipName(blip)
	SetBlipAsShortRange(blip,true)
	SetBlipAsMissionCreatorBlip(blip,true)
	checkgarage = 0
	while true do
		Wait(0)
		DrawMarker(1,vente_location[1],vente_location[2],vente_location[3],0,0,0,0,0,0,5.001,5.0001,0.5001,0,155,255,200,0,0,0,0)
		if GetDistanceBetweenCoords(vente_location[1],vente_location[2],vente_location[3],GetEntityCoords(LocalPed())) < 5 and IsPedInAnyVehicle(LocalPed(), true) == false then
			drawTxt('~g~E~s~ pour vendre le bateau à 50% du prix d\'achat',0,1,0.5,0.8,0.6,255,255,255,255)
			if IsControlJustPressed(1, 86) then
				TriggerServerEvent('ply_docks:CheckForSelBoat',source)
			end
		end
	end
end)



--[[Events]]--

AddEventHandler("ply_docks:getBoat", function(THEBOATS)
    BOATS = {}
    BOATS = THEBOATS
end)

AddEventHandler("playerSpawned", function()
    TriggerServerEvent("ply_docks:CheckDockForBoat")
end)

AddEventHandler('ply_docks:SpawnBoat', function(boat, plate, state, primarycolor, secondarycolor, pearlescentcolor, wheelcolor)
	local boat = boat
	local car = GetHashKey(boat)
	local plate = plate
	local state = state
	local primarycolor = tonumber(primarycolor)
	local secondarycolor = tonumber(secondarycolor)
	local pearlescentcolor = tonumber(pearlescentcolor)
	local wheelcolor = tonumber(wheelcolor)
	Citizen.CreateThread(function()
		Citizen.Wait(3000)
		local caisseo = GetClosestVehicle(dockSelected.x, dockSelected.y, dockSelected.z, 3.000, 0, 12294)
		if DoesEntityExist(caisseo) then
			drawNotification("La zone est encombrée") 
		else
			if state == "Sortit" then
				drawNotification("Ce bateau n'est pas au dock")
			else
				local mods = {}
				for i = 0,24 do
					mods[i] = GetVehicleMod(veh,i)
				end
				RequestModel(car)
				while not HasModelLoaded(car) do
				Citizen.Wait(0)
				end
				veh = CreateVehicle(car, dockSelected.x, dockSelected.y, dockSelected.z, dockSelected.axe, true, false)
				for i,mod in pairs(mods) do
					SetVehicleModKit(personalvehicle,0)
					SetVehicleMod(personalvehicle,i,mod)
				end
				SetVehicleNumberPlateText(veh, plate)
				SetVehicleOnGroundProperly(veh)
				SetVehicleHasBeenOwnedByPlayer(veh,true)
				local id = NetworkGetNetworkIdFromEntity(veh)
				SetNetworkIdCanMigrate(id, true)
				SetVehicleColours(veh, primarycolor, secondarycolor)
				SetVehicleExtraColours(veh, pearlescentcolor, wheelcolor)
				SetEntityInvincible(veh, false) 
				drawNotification("Bateau sorti")				
				TriggerServerEvent('ply_docks:SetBoatOut', boat, plate)
   				TriggerServerEvent("ply_docks:CheckDockForBoat")
			end
		end
	end)
end)

AddEventHandler('ply_docks:StoreBoat', function(boat, plate)
	local boat = GetHashKey(boat)	
	local plate = plate
	Citizen.CreateThread(function()
		Citizen.Wait(3000)
		local caissei = GetClosestVehicle(dockSelected.x, dockSelected.y, dockSelected.z, 5.000, 0, 12294)
		SetEntityAsMissionEntity(caissei, true, true)		
		local platecaissei = GetVehicleNumberPlateText(caissei)
		if DoesEntityExist(caissei) then	
			if plate ~= platecaissei then					
				drawNotification("Ce n'est pas ton bateau")
			else
				Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(caissei))
				drawNotification("Bateau rentré")
				TriggerServerEvent('ply_docks:SetBoatIn', plate)
				TriggerServerEvent("ply_docks:CheckDockForBoat")
			end
		else
			drawNotification("Aucun bateau présent")
		end   
	end)
end)

AddEventHandler('ply_docks:SelBoat', function(boat, plate)
	local boat = GetHashKey(boat)	
	local plate = plate
	Citizen.CreateThread(function()		
		Citizen.Wait(0)
		local caissei = GetClosestVehicle(vente_location[1],vente_location[2],vente_location[3], 3.000, 0, 12294)
		SetEntityAsMissionEntity(caissei, true, true)
		local platecaissei = GetVehicleNumberPlateText(caissei)
		if DoesEntityExist(caissei) then
			if plate ~= platecaissei then
				drawNotification("Ce n'est pas ton bateau")
			else
				Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(caissei))
				TriggerServerEvent('ply_docks:SelBoat', plate)
				TriggerServerEvent("ply_docks:CheckDockForBoat")
			end
		else
			drawNotification("Aucun bateau présent")
		end
	end)
end)

local firstspawn = 0
AddEventHandler('playerSpawned', function(spawn)
	if firstspawn == 0 then
		RemoveIpl('v_carshowroom')
		RemoveIpl('shutter_open')
		RemoveIpl('shutter_closed')
		RemoveIpl('shr_int')
		RemoveIpl('csr_inMission')
		RequestIpl('v_carshowroom')
		RequestIpl('shr_int')
		RequestIpl('shutter_closed')
		firstspawn = 1
	end
end)