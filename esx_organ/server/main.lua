ESX 						   = nil
local CopsConnected       	   = 0
local PlayersHarvestingCerveau = {}
local PlayersHarvestingCoeur   = {}
local PlayersHarvestingMoelle  = {}
local PlayersHarvestingIntestin = {}
local PlayersHarvestingOs   	= {}
local PlayersTransformingOrgan 	= {}
local PlayersSellingBody      	= {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

function CountCops()

	local xPlayers = ESX.GetPlayers()

	CopsConnected = 0

	for i=1, #xPlayers, 1 do
		local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
		if xPlayer.job.name == 'police' then
			CopsConnected = CopsConnected + 1
		end
	end

	SetTimeout(5000, CountCops)

end

CountCops()

--os
local function HarvestOs(source)

	if CopsConnected < Config.RequiredCopsBody then
		TriggerClientEvent('esx:showNotification', source, _U('act_imp_police') .. CopsConnected .. '/' .. Config.RequiredCopsBody)
		return
	end

	SetTimeout(5000, function()

		if PlayersHarvestingOs[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)

			local os = xPlayer.getInventoryItem('os')

			if os.limit ~= -1 and os.count >= os.limit then
				TriggerClientEvent('esx:showNotification', source, _U('inv_full_os'))
			else
				xPlayer.addInventoryItem('os', 1)
				HarvestOs(source)
			end

		end
	end)
end

RegisterServerEvent('esx_organ:startHarvestOs')
AddEventHandler('esx_organ:startHarvestOs', function()

	local _source = source

	PlayersHarvestingOs[_source] = true

	TriggerClientEvent('esx:showNotification', _source, _U('pickup_in_prog'))

	HarvestOs(_source)

end)

RegisterServerEvent('esx_organ:stopHarvestOs')
AddEventHandler('esx_organ:stopHarvestOs', function()

	local _source = source

	PlayersHarvestingOs[_source] = false

end)

local function TransformOrgan(source)

    if CopsConnected < Config.RequiredCopsBody then
        TriggerClientEvent('esx:showNotification', source, _U('act_imp_police') .. CopsConnected .. '/' .. Config.RequiredCopsBody)
        return
    end

    SetTimeout(10000, function()

        if PlayersTransformingOrgan[source] == true then

            local xPlayer  = ESX.GetPlayerFromId(source)

            local osQuantity = xPlayer.getInventoryItem('os').count
            local poochQuantity = xPlayer.getInventoryItem('organ_pooch').count
            local cerveauQuantity = xPlayer.getInventoryItem('cerveau').count
            local coeurQuantity = xPlayer.getInventoryItem('coeur').count
            local moelleQuantity = xPlayer.getInventoryItem('moelle').count
            local intestinQuantity = xPlayer.getInventoryItem('intestin').count

            if poochQuantity > 35 then
                TriggerClientEvent('esx:showNotification', source, _U('too_many_pouches'))
            elseif osQuantity >= 1  then
                xPlayer.removeInventoryItem('os', 1)
      

                xPlayer.addInventoryItem('organ_pooch', 1)
            
                TransformOrgan(source)
            else
                TriggerClientEvent('esx:showNotification', source, _U('not_enough_organ'))
            end
        end
    end)
end

RegisterServerEvent('esx_organ:startTransformOrgan')
AddEventHandler('esx_organ:startTransformOrgan', function()

	local _source = source

	PlayersTransformingOrgan[_source] = true

	TriggerClientEvent('esx:showNotification', _source, _U('packing_in_prog'))

	TransformOrgan(_source)

end)

RegisterServerEvent('esx_organ:stopTransformOrgan')
AddEventHandler('esx_organ:stopTransformOrgan', function()

	local _source = source

	PlayersTransformingOrgan[_source] = false

end)

local function SellBody(source)

	if CopsConnected < Config.RequiredCopsBody then
		TriggerClientEvent('esx:showNotification', source, _U('act_imp_police') .. CopsConnected .. '/' .. Config.RequiredCopsBody)
		return
	end

	SetTimeout(7500, function()

		if PlayersSellingBody[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)

			local poochQuantity = xPlayer.getInventoryItem('organ_pooch').count

			if poochQuantity == 0 then
				TriggerClientEvent('esx:showNotification', source, _U('no_pouches_sale'))
			else
				xPlayer.removeInventoryItem('organ_pooch', 1)
				if CopsConnected == 0 then
                    xPlayer.addAccountMoney('black_money', 30800)
                    TriggerClientEvent('esx:showNotification', source, _U('sold_one_body'))
                elseif CopsConnected == 1 then
                    xPlayer.addAccountMoney('black_money', 40000)
                    TriggerClientEvent('esx:showNotification', source, _U('sold_one_body'))
                elseif CopsConnected == 2 then
                    xPlayer.addAccountMoney('black_money', 52000)
                    TriggerClientEvent('esx:showNotification', source, _U('sold_one_body'))
                elseif CopsConnected == 3 then
                    xPlayer.addAccountMoney('black_money', 64000)
                    TriggerClientEvent('esx:showNotification', source, _U('sold_one_body'))
                elseif CopsConnected == 4 then
                    xPlayer.addAccountMoney('black_money', 76000)
                    TriggerClientEvent('esx:showNotification', source, _U('sold_one_body'))
                elseif CopsConnected >= 5 then
                    xPlayer.addAccountMoney('black_money', 99000)  
                    TriggerClientEvent('esx:showNotification', source, _U('sold_one_body'))
                end
				
				SellBody(source)
			end

		end
	end)
end

RegisterServerEvent('esx_organ:startSellBody')
AddEventHandler('esx_organ:startSellBody', function()

	local _source = source

	PlayersSellingBody[_source] = true

	TriggerClientEvent('esx:showNotification', _source, _U('sale_in_prog'))

	SellBody(_source)

end)

RegisterServerEvent('esx_organ:stopSellBody')
AddEventHandler('esx_organ:stopSellBody', function()

	local _source = source

	PlayersSellingBody[_source] = false

end)


-- RETURN INVENTORY TO CLIENT
RegisterServerEvent('esx_organ:GetUserInventory')
AddEventHandler('esx_organ:GetUserInventory', function(currentZone)
	local _source = source
    local xPlayer  = ESX.GetPlayerFromId(_source)
    TriggerClientEvent('esx_organ:ReturnInventory', 
    	_source, 
    	xPlayer.getInventoryItem('cerveau').count, 
		xPlayer.getInventoryItem('coeur').count,  
		xPlayer.getInventoryItem('moelle').count,  
		xPlayer.getInventoryItem('intestin').count,
		xPlayer.getInventoryItem('os').count, 
		xPlayer.getInventoryItem('organ_pooch').count,
		xPlayer.job.name, 
		currentZone
    )
end)
