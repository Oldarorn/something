Citizen.CreateThread(function()
	local handsup = false
	while true do
		Citizen.Wait(0)
		local lPed = GetPlayerPed(-1)
		RequestAnimDict("move_crawlprone2crawlfront")
		if IsControlPressed(1, 323) then
			if DoesEntityExist(lPed) then
				Citizen.CreateThread(function()
					RequestAnimDict("move_crawlprone2crawlfront")
					while not HasAnimDictLoaded("move_crawlprone2crawlfront") do
						Citizen.Wait(100)
					end

					if not handsup then
						handsup = true
						TaskPlayAnim(lPed, "move_crawlprone2crawlfront", "front", 8.0, -8, -1, 49, 0, 0, 0, 0)
						TriggerEvent("pm:isCuff", true)
					end   
				end)
			end
		end

		if IsControlReleased(1, 323) then
			if DoesEntityExist(lPed) then
				Citizen.CreateThread(function()
					RequestAnimDict("move_crawlprone2crawlfront")
					while not HasAnimDictLoaded("move_crawlprone2crawlfront") do
						Citizen.Wait(100)
					end

					if handsup then
						handsup = false
						ClearPedSecondaryTask(lPed)
						TriggerEvent("pm:isCuff", false)
					end
				end)
			end
		end
	end
end)